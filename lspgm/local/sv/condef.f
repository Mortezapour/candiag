      PROGRAM CONDEF
C     PROGRAM CONDEF (DECK,       DFNAM,       OUT,         INPUT,              G2
C    1                                                      OUTPUT,     )       G2
C    2         TAPE11=DECK, TAPE12=DFNAM, TAPE13=OUT, TAPE5=INPUT,
C    3                                                TAPE6=OUTPUT) 
C     -------------------------------------------------------------             G2
C                                                                               G2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       G2
C     AUG 12/94 - F. MAJAESS (USE INTEGER*4 FOR C-ROUTINE ARG)                  
C     FEB 10/92 - E.CHAN    (CONVERT HOLLERITH TO ASCII, REPLACE CALL TO        
C                            JCLPNT WITH CALLS TO GETARG AND OPEN              
C                            STATEMENTS, AND REMOVE XIT(___,0))               
C     FEB 19/91 - F.MAJAESS (MODIFIED TO HANDLE 82 CHARACTERS/LINE)
C     NOV 22/83 - R.LAPRISE.
C                                                                               G2
CCONDEF  - SIMULATES CONDITIONNAL ASSEMBLY OF JCL STREAM                2  1    G1
C                                                                               G3
CAUTHOR  - R.LAPRISE.                                                           G3
C                                                                               G3
CPURPOSE - PRE-PROCESSOR TO PROCESS JCL BEFORE PARMSUB.                         G3
C          LINES BRACKETTED BY STATEMENTS  =IF,+NAME  OR  =IF,-NAME             G3
C          AND  =END,NAME  ON "DECK" ARE CONDITIONALLY COPIED TO "OUT"          G3
C          DEPENDING ON WHETHER "DFNAM" CONTAINS A  =DF NAME  STATEMENT.        G3
C          NOTE - NONE OF THE FILES ARE REWOUND BEFORE OR AFTER PROCESSING.     G3
C                                                                               G3
CINPUT FILES...                                                                 G3
C                                                                               G3
C      DECK  = INPUT FILE CONTAINING LINES BRACKETTED BY STATEMENTS             G3
C              =IF,+NAME  OR  =IF,-NAME  AND  =END,NAME.                        G3
C              THE =IF, AND =END STATEMENTS MUST EXIST IN PAIRS ON              G3
C              FILE DECK.                                                       G3
C      DFNAM = INPUT FILE CONTAINING STATEMENTS OF THE FORM =DF NAME.           G3
C              A MAXIMUM OF 100 NAMES ARE ALLOWED ON FILE DFNAM.                G3
C              EACH NAME CAN BE UP TO 8 CHARACTERS LONG.                        G3
C              THE LEADING CHARACTER (=) IS DEFINED BY THE CHARACTER            G3
C              PRECEEDING  (DF NAME)  ON FILE DFNAM.                            G3
C                                                                               G3
COUTPUT FILE...                                                                 G3
C                                                                               G3
C      OUT   = CONDITIONALLY COPIED LINES FROM FILE DECK.                       G3
C------------------------------------------------------------------------ 
C 
C          NDF   = NUMBER OF DEFINED PARAMETERS ON FILE DFNAM.
C          NIN   = NUMBER OF 82 COLUMNS CARDS READ IN FROM FILE DECK. 
C          NCONT = NUMBER OF CONTROL STATEMENTS ENCOUNTERED ON FILE DECK. 
C          NOUT  = NUMBER OF CARD IMAGES WRITTEN OUT TO FILE OUT. 
C          NCOND = COUNT OF NUMBER OF SIMULTANEOUS CONDITIONS RESULTING 
C                  IN SKIP. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER*512 FILNAM
      INTEGER*4 II
C
      LOGICAL LIFP,LIFM,LEND,OK,LSKIP,LSKIPA
C 
      CHARACTER*1 LC,IFP(4),IFM(4),END(4),CARD(82),DF(8,100),COND(8,100)
C 
      DATA IFP/'I','F',',','+'/
      DATA IFM/'I','F',',','-'/
      DATA END/'E','N','D',','/
C 
      DATA MAXDF/100/, LNT/4/, LNTDF/8/ 
C-----------------------------------------------------------------------
      DO 50 I=1,3
        II=I
        CALL GETARG(II,FILNAM)
        OPEN(I+10,FILE=FILNAM)
   50 CONTINUE
C 
C     * READ FILE DFNAM TO DECODE THE NAMES.
C     * DEFAULT LEADING CHARACTER IS =. 
C 
      LC='='
      NDF=0 
      DO 100 N=1,MAXDF
      READ(12,1200,END=130)LC,(DF(I,N),I=1,LNTDF) 
      NDF=NDF+1 
  100 CONTINUE
C 
C     * ALL NAMES HAVE BEEN READ IN.  PRINT THEM. 
C 
  130 WRITE(6,6000)NDF
      IF(NDF.GT.0)WRITE(6,6001)LC,((DF(I,N),I=1,LNTDF),N=1,NDF) 
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C     * PROCESS FILE DECK.
C 
      NIN=0 
      NCONT=0 
      NOUT=0
C     WRITE(6,6004) 
      LSKIP=.FALSE. 
      NCOND=0 
  150 READ(11,1000,END=901)CARD 
      NIN=NIN+1 
      IF(CARD(1).NE.LC) GO TO 300 
C 
C     * THIS CARD IS A CONTROL STATEMENT.  DECODE IT. 
C 
      NCONT=NCONT+1 
      CALL COMPARS(LIFP,CARD(2),IFP,LNT) 
      CALL COMPARS(LIFM,CARD(2),IFM,LNT) 
      CALL COMPARS(LEND,CARD(2),END,LNT) 
      IF(.NOT.(LIFP.OR.LIFM.OR.LEND)) CALL         XIT('CONDEF',-1) 
      IF(LEND) GO TO 250
C-------------------------------------------------------------------------
C     * THIS IS A   =IF,+ (OR -) CONTROL STATEMENT. 
C     * FIND OUT IF NAME IS ONE OF THE DF NAMES,  AND SET OK=.TRUE. IF SO. 
C 
      LSKIPA=LSKIP
      OK=.FALSE.
      IF(NDF.EQ.0) GO TO 205
      DO 200 N=1,NDF
      CALL COMPARS(OK,CARD(LNT+2),DF(1,N),LNTDF) 
      IF(OK) GO TO 205
  200 CONTINUE
C 
C     * SET SWITCH TO SKIP TILL  =END,NAME  IF... 
C     * NAME IS NOT ONE OF THE =DF NAME  AND  THIS IS A  =IF,+  OR
C     * NAME IS ONE OF THE =DF NAME  AND  THIS IS A  =IF,-  STATEMENT.
C 
  205 LSKIP=(OK.AND.LIFM) .OR. ((.NOT.OK).AND.LIFP) 
C 
C     * REMEMBER THE NAME THAT CAUSES THE SKIP AND ACCUMULATE IN ARRAY COND.
C 
      IF(.NOT.LSKIP) GO TO 220
      NCOND=NCOND+1 
      DO 210 N=1,LNTDF
  210 COND(N,NCOND)=CARD(LNT+1+N) 
C 
  220 LSKIP=LSKIP .OR. LSKIPA 
      GO TO 300 
C-------------------------------------------------------------------------
C     * THIS IS A  =END,NAME  STATEMENT.
C     * IF WE ARE IN SKIPPING MODE,  CHECK IF THIS IS THE END OF IT.
C 
  250 OK=.FALSE.
      IF(LSKIP)CALL COMPARS(OK,CARD(LNT+2),COND(1,NCOND),LNTDF)
      IF(OK) NCOND=NCOND-1
      LSKIP=.FALSE. 
      IF(NCOND.GT.0) LSKIP=.TRUE. 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * WRITE THIS LINE TO OUT UNLESS SKIPPING OR CONTROL CARD. 
C 
  300 CONTINUE
      IF(LSKIP .OR. CARD(1).EQ.LC) GO TO 150
      WRITE(13,1000)CARD
C     WRITE(6,6003)CARD 
      NOUT=NOUT+1 
      GO TO 150 
C 
C     * E.O.F. ON FILE DECK.
C 
  901 IF(NIN.EQ.0) CALL                            XIT('CONDEF',-2) 

C     * NORMAL TERMINATION.

      WRITE(6,6002)NIN,NCONT,NOUT 
C-----------------------------------------------------------------------
 1000 FORMAT(82A1)
 1200 FORMAT(A1,3X, 8A1)
 6000 FORMAT(/,' NUMBER OF DEFINED PARAMETERS, NDF=',I5)
 6001 FORMAT(/,' CONTROL CHARACTER IS ',A1,/,
     1         ' DEFINED NAMES ARE...',//,(10X,8A1))
 6002 FORMAT(/,' NUMBER OF CARDS IN=',I5,',CONTROL=',I5,',OUT=',I5)
C6003 FORMAT(10X,82A1)
C6004 FORMAT(//,' LIST OF OUTPUT FILE...',/)
      END 
