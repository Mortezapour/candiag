      PROGRAM RGOPR 
C     PROGRAM RGOPR (GGIN,       GGOUT,       OUTPUT,                   )       C2
C    1         TAPE1=GGIN, TAPE2=GGOUT, TAPE6=OUTPUT) 
C     -----------------------------------------------                           C2
C                                                                               C2
C     MAR 04/10 - S.KHARIN,F.MAJAESS (CHANGE "RGAS" FROM 287. TO 287.04)        C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     JAN 14/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 13/83 - R.LAPRISE.                                                    
C     JUL 01/80 - J.D.HENDERSON 
C                                                                               C2
CRGOPR   - MULTIPLIES PRESSURE GRID SETS BY RGAS/PRESSURE               1  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - MULTIPLIES A MULTI-LEVEL PRESSURE FILE BY (RGAS/PRESSURE).           C3
C          IF THE FILE CONTAINS TEMPERATURE THE RESULT WILL BE                  C3
C          SPECIFIC VOLUME (RT/P).                                              C3
C          THE RESULT IS STORED ON FILE GGOUT.                                  C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      GGIN  = FILE OF PRESSURE LEVEL SETS (REAL OR COMPLEX).                   C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      GGOUT = COPY OF FILE GGIN MULTIPLIED BY (RGAS/PRESSURE).                 C3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/G(SIZES_LONP1xLAT) 
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEXT FIELD FROM FILE GGIN. 
C 
      NR=0
  150 CALL GETFLD2(1,G,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('RGOPR',-1)
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('RGOPR',0) 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
C     * DETERMINE SIZE OF THE FIELD.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
C 
C     * MULTIPLY BY RGAS/PRESSURE.
C 
      RGAS=287.04 
      CALL LVDCODE(PRES,IBUF(4),1)
      PRES=PRES*100.E0
      CONST=RGAS/PRES 
      DO 210 I=1,NWDS 
  210 G(I)=G(I)*CONST 
C 
C     * SAVE ON FILE GGOUT. 
C 
      CALL PUTFLD2(2,G,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C---------------------------------------------------------------------
 6010 FORMAT('0',I6,'  RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
