      PROGRAM XYLIN 
C     PROGRAM XYLIN (XIN,      YIN,      XYOUT,      INPUT,      OUTPUT,)       C2
C    1         TAPE1=XIN,TAPE2=YIN,TAPE3=XYOUT,TAPE5=INPUT,TAPE6=OUTPUT)
C     ------------------------------------------------------------------        C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS )      C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     NOV 27/80 - J.D.HENDERSON 
C                                                                               C2
CXYLIN   - LINEAR OPERATION ON TWO FILES    Z=A*X+B*Y+C                 2  1 C  C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - FILE COMPUTATION  XYOUT = A*XIN + B*YIN + C                          C3
C          NOTE - XIN AND YIN CAN BE REAL OR COMPLEX.                           C3
C                 C MUST BE 0. FOR COMPLEX FILES.                               C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      XIN   = FIRST  INPUT FILE (REAL OR COMPLEX)                              C3
C      YIN   = SECOND INPUT FILE (SAME TYPE AS XIN)                             C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XYOUT = A*XIN + B*YIN + C                                                C3
C 
CINPUT PARAMETERS...
C                                                                               C5
C      A      = FACTOR FOR FIRST FILE                                           C5
C      B      = FACTOR FOR SECOND FILE                                          C5
C      C      = ADDITIVE CONSTANT                                               C5
C      NEWNAM = NEW NAME FOR OUTPUT LABEL                                       C5
C               (BLANK KEEPS OLD NAME)                                          C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*   XYLIN        2.        3.        0. NAME                                   C5
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,SPEC 
      COMMON/BLANCK/G(SIZES_BLONP1xBLAT),H(SIZES_BLONP1xBLAT) 
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C---------------------------------------------------------------------
      NFF=5 
      CALL JCLPNT(NFF,1,2,3,5,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * READ A,B,C AND NEWNAM FROM CARD.
C 
      READ(5,5010,END=905) A,B,C,NEWNAM                                         C4
      WRITE(6,6007)A,B,C
C 
C     * READ THE NEXT PAIR OF FIELDS. 
C 
      NR=0
  150 CALL GETFLD2(1,G,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('XYLIN',-1)
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('XYLIN',0) 
      ENDIF 
C 
      CALL GETFLD2(2,H,-1,0,0,0,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('XYLIN',-2)
C 
C     * MAKE SURE THAT THE FIELDS ARE THE SAME KIND AND SIZE. 
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF,JBUF 
        CALL                                       XIT('XYLIN',-3)
      ENDIF 
C 
C     * DETERMINE SIZE OF THE FIELD.
C     * STOP IF FIELD IS COMPLEX AND C IS NOT 0.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC.AND.C.NE.0.E0) CALL                  XIT('XYLIN',-4)
C 
C     * SET G = A*G+B*H+C 
C 
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
      DO 210 I=1,NWDS 
  210 G(I)=A*G(I)+B*H(I)+C
C 
C     * PUT THE RESULT ON FILE XYOUT. 
C     * CHANGE THE NAME IF NEWNAM IS NON-BLANK. 
C 
      IF(NEWNAM.NE.NC4TO8("    ")) IBUF(3)=NEWNAM
      CALL PUTFLD2(3,G,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  905 CALL                                         XIT('XYLIN',-5)
C---------------------------------------------------------------------
 5010 FORMAT(10X,3E10.0,1X,A4)                                                  C4
 6007 FORMAT('0XYLIN A,B,C=',1P3E12.4)
 6010 FORMAT('0XYLIN READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
