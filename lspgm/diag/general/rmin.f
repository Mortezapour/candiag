      PROGRAM RMIN
C     PROGRAM RMIN (XIN,       MIN,       OUTPUT,                       )       C2
C    1        TAPE1=XIN, TAPE2=MIN, TAPE6=OUTPUT)                               C2
C     -------------------------------------------                               C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     MAR 05/02 - F.MAJAESS (REVISED TO HANDLE "CHAR" KIND RECORDS)             
C     JUL 05/99 - F.MAJAESS (REVISED TO OPERATE ON REAL AND IMAGINARY PARTS OF  
C                            THE COMPLEX FIELD AS A PAIR INSTEAD OF SEPARATELY) 
C     JUL 16/98 - D.LIU
C                                                                               C2
CRMIN    - EXTRACTS MINIMUM VALUE OF EACH DATA RECORD IN A FILE         1  1    C1
C                                                                               C3
CAUTHOR  - D. LIU                                                               C3
C                                                                               C3
CPURPOSE - EXTRACTS THE MINIMUM VALUE FOR EACH DATA RECORD IN A FILE.           C3
C          THE OUTPUT HAS THE SAME STRUCTURE AS THE INPUT WITH ALL THE DATA     C3
C          POINTS IN THE RECORD SET HAVING THE VALUE OF THE MINIMUM.            C3
C          FOR COMPLEX FIELDS, THE REAL AND IMAGINARY PARTS OF THE OUTPUT       C3
C          RECORD ARE SET TO THE CORRESPONDING PARTS OF THE COMPLEX NUMBER      C3
C          IN THE INPUT RECORD THAT HAS THE SMALLEST AMPLITUDE.                 C3
C
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN = INPUT FILE. POSSIBLY CONTAINS "LABL" AND/OR "CHAR" KIND RECORDS.   C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      MIN = OUTPUT FILE OF MINIMUM VALUES. "LABL" AND/OR "CHAR" KIND RECORDS   C3
C            ARE COPIED FROM THE INPUT FILE.                                    C3
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,CPLXFLD
      REAL AMINVAL,CMINVAL(2)
C
      COMMON/BLANCK/A(SIZES_BLONP1xBLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C
C     * GET THE NEXT FIELD. LABELS ARE JUST PRINTED.
C
      NR=0
      CPLXFLD=.FALSE.
 150  CALL RECGET(1,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
         IF(NR.EQ.0) CALL                          XIT('RMIN',-1)
         WRITE(6,6010) NR,IBUF
         IF (CPLXFLD) WRITE(6,6020)
         CALL                                      XIT('RMIN',0)
      ENDIF

      KIND=IBUF(1)

C     * RECORDS OF FOLLOWING TYPES ARE SIMPLY DUPLICATED.

      IF(KIND.EQ.NC4TO8("LABL").OR.KIND.EQ.NC4TO8("DATA").OR.
     +                             KIND.EQ.NC4TO8("CHAR")) THEN
         CALL RECPUT(2,IBUF)
         GO TO 150
      ENDIF

C     * FOR OTHER TYPES, GO ON UNPACK.

      NR=NR+1
      CALL RECUP2(A,IBUF)
      LA=IBUF(5)*IBUF(6)
C
C     * DETERMINE THE KIND OF FIELD.
C
      IF(KIND.EQ.NC4TO8("GRID").OR.KIND.EQ.NC4TO8("ZONL").OR.
     1     KIND.EQ.NC4TO8("SUBA").OR.KIND.EQ.NC4TO8("TIME")) THEN
         INC=1
      ELSE IF (KIND.EQ.NC4TO8("FOUR").OR.KIND.EQ.NC4TO8("SPEC")) THEN
         LA=2*LA
         INC=2
         CPLXFLD=.TRUE.
      ELSE
         WRITE(6,6010) NR,IBUF
         CALL                                      XIT('RMIN',-2)
      ENDIF
      IF (NR.EQ.1) WRITE(6,6010) NR, IBUF
C
C     * COMPUTE THE MIN(S).
C

C     * PROCESS THE FIELD VALUES.

      IF ( INC.EQ.2) THEN

C       * COMPLEX FIELDS.

        CMINVAL(1)=A(1)
        CMINVAL(2)=A(2)
        AMINVAL=ABS(CMPLX(A(1),A(2)))
        DO I=3,LA,2
           IF(ABS(CMPLX(A(I),A(I+1))).LT.AMINVAL) THEN
             AMINVAL=ABS(CMPLX(A(I),A(I+1)))
             CMINVAL(1)=A(I)
             CMINVAL(2)=A(I+1)
           ENDIF
        ENDDO

        DO I=1,LA,2
           A(I)  =CMINVAL(1)
           A(I+1)=CMINVAL(2)
        ENDDO

      ELSE

C       * REAL FIELDS.

        AMINVAL=A(1)
        DO I=2,LA
           IF(A(I).LT.AMINVAL) AMINVAL=A(I)
        ENDDO

        DO I=1,LA
           A(I)=AMINVAL
        ENDDO

      ENDIF
      CALL PUTFLD2(2,A,IBUF,MAXX)

      GO TO 150

C---------------------------------------------------------------------
 6010 FORMAT(' ',I5,2X,A4,I10,2X,A4,I10,2I8,I9,I3,5X,1P4E14.6)
 6020 FORMAT(/,' FOR COMPLEX FIELDS, THE REAL AND IMAGINARY ',
     1     'PARTS OF THE OUTPUT RECORD',/,' ARE SET TO THE ',
     2     'CORRESPONDING PARTS OF THE COMPLEX NUMBER IN THE ',
     3     'INPUT',/,' RECORD THAT HAS THE SMALLEST AMPLITUDE.')
      END
