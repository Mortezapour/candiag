program epflux4gaschem

!     program epflux (upvp, upwp, vptp, uz, tz, vz, wz,  & 
!                     epfy, epfz, epfd, vres, wres, output )
!
! Fortran unit numbers for in- and output files of this program:
! Unit (in)     Field                 Unit (out)    Field
! --------      -----                 ----------    -----
!  11           UPVP                   21           EPFY
!  12           UPWP                   22           EPFZ
!  13           VPTP                   23           EPFD
!  14           UZ                     24           VRES
!  15           TZ                     25           WRES
!  16           VZ                      6          stdout
!  17           WZ
!
! --------------------------------------------------------------
! Author : C. McLandress (December 20 2006)
!
! Purpose: Compute EP flux diagnostics and residual circulation in log-pressure
!          coordinates using the formulation given in "Middle Atmosphere
!          Dynamics" (1987, p.128) by Andrews, Holton & Leovy.
!          The input fluxes can either be time-varying (ie instantaneous)
!          or stationary fluxes computed from the time-mean wave fields.
!
! Input fields:
!   UPVP - meridional flux of zonal momentum for zonal deviations (m^2/s^2)
!   UPWP - vertical (ie pressure velocity) flux of zonal momentum for zonal
!          deviations (Pa m/s^2)
!   VPTP - meridional heat flux for zonal deviations (K m/s)
!   UZ   - zonal-mean zonal wind (m/s)
!   TZ   - zonal-mean temperature (K)
!   VZ   - zonal-mean meridional wind (m/s)
!   WZ   - zonal-mean vertical wind (pressure velocity in Pa/s)
!
! Output fields:
!   EPFY - meridional component of the EP flux vector (kg/s^2)
!   EPFZ - vertical component of the EP flux vector (kg/s^2)
!   EPFD - EP flux divergence (force per unit mass, ie divided by 
!                               rho*a*cos(phi)) (m/s^2)
!   VRES - residual meridional velocity (m/s) 
!   WRES - residual vertical velocity (m/s) 
!----------------------------------------------------------------------------

  use diag_sizes, only : SIZES_LONP1xLAT,         &
                         SIZES_LONP1xLATxNWORDIO, &
                         SIZES_LAT,               &
                         SIZES_MAXLEV,            &
                         SIZES_MAXLEVxBLONP1xBLAT
  implicit none

  integer :: ilath, j, l, n, nc4to8, nf, nlat, nlatm, nlev, nlevm, nr, nsets
  integer, dimension(SIZES_MAXLEV) :: lev
  integer, parameter :: maxlat=SIZES_LAT, maxlev=SIZES_MAXLEV

  logical :: ok, ok1, ok2, ok3, ok4, ok5, ok6, ok7

  real :: acosdy, afct, acs, coriol, dy, dz, radnp, radsp, om2
  real, dimension(SIZES_MAXLEV) :: dens, pr, tfac, wfac, zlogp
  real, dimension(SIZES_LAT,SIZES_MAXLEV) :: upvp, upwp, vptp, tz, uz, vz, wz
  real, dimension(SIZES_LAT,SIZES_MAXLEV) :: tzdz, uzdz, epfd, epfy, epfz
  real, dimension(SIZES_LAT,SIZES_MAXLEV) :: fhat, vres, wres, workv, workw

  real*8, dimension(SIZES_LAT) :: siny, cosy, rad, wl, wossl

  integer, dimension(8)     :: ibuf
  integer, dimension(20008) :: idat
  common /icom/ ibuf, idat

  ! Parameters used in this routine. For consistency, they are identical to
  ! what is defined in CanAM/glue/phys_consts.F90
  real, parameter :: ww=7.292e-5       ! formerly OMEGA
  real, parameter :: rayon=6.37122e06  ! formerly RADIUS=6.371E6
  real, parameter :: rgas=287.04       ! formerly RGAS=287.
  real, parameter :: cp=1004.5         ! formerly CPGAS=1004.E0
  real, parameter :: rkappa=rgas/cp    ! formerly RKAPPA=0.286
  real, parameter :: grav=9.80616      ! formerly GRAVITY=9.81
  real, parameter :: pr0=1013.25e2
  real, parameter :: hscale=7.0e3
  real, parameter :: dens0=pr0/(grav*hscale) ! formerly DENS0=1.47
  real, parameter :: pi=4.0*atan(1.0)  ! formerly PI=2.*ASIN(1.E0)
!---------------------------------------------------------------------

  nf = 12 
  call jclpnt (nf, 11, 12, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 6)
!
! Rewind input files.
  do n = 11, 17
    rewind n
  enddo  ! loop 100

! Determine the pressure levels of first input file. It is assumed that
! the other files are the same.
  call filev (lev, nlev, ibuf, 11)

  nlat  = ibuf(5)
  nlatm = nlat - 1
  nlevm = nlev - 1
  radsp = -pi / 2.0
  radnp =  pi / 2.0
  om2   = 2.0 * ww

! Abort if array sizes or type are incorrect.
  if ( (nlev < 3) .or. (nlev > maxlev) ) call xit('epflux4gaschem', -1) 
  if ( nlat > maxlat )                   call xit('epflux4gaschem', -2) 
  if ( ibuf(1) /=  nc4to8("ZONL") )      call xit('epflux4gaschem', -3)

! Latitude trigonometric arrays
  ilath = nlat / 2
  call gaussg (ilath, siny, wl, cosy, rad, wossl)
  call  trigl (ilath, siny, wl, cosy, rad, wossl)

! Compute pressure in pascals, log-pressure height and scale factors for
! converting to potential temperature and vertical velocity.
  call lvdcode (pr, lev, nlev)

  do l = 1, nlev
    pr(l) = pr(l) * 100.e0 
    zlogp(l) = -hscale * alog(pr(l)/pr0)                
    tfac(l)  = (pr0/pr(l))**rkappa
    wfac(l)  = -hscale / pr(l)
    dens(l)  = dens0 * exp(-zlogp(l)/hscale)
  enddo

! ----------------------------------------------------------------
! For debugging:
!      write(6,*) 'nlev=',nlev
!      do l=1,nlev
!        write(6,6100) l,zlogp(l)/1.e3,pr(l),
!     &                dens(l),wfac(l),tfac(l)
!      end do
! 6100 format('l,z,p,dens,wfac,tfac=',i2,f6.1,4(e12.5,1x))
!      write(6,*) 
!      write(6,*) 'nlat=',nlat
!      write(6,*) pi
!      do j=1,nlat
!        ydeg=rad(j)*180./pi
!        write(6,6110) j,ydeg,rad(j),cosy(j),siny(j)
!      end do
! 6110 format('j,y,rad,cos,sin=',i2,f8.2,3(1x,f6.3))
!      write(6,*) 
! ----------------------------------------------------------------

  nr = 0
  nsets = 0

! Read the next sets of fields.
 1000 CONTINUE 

! Loop through levels and put data into latitude vs height arrays;
! convert temperature to potential temperature, and ww ("omega") to
! log-pressure vertical velocity.

  do l = 1, nlev
    call getfld2 (11, upvp(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok1)
    call getfld2 (12, upwp(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok2)
    call getfld2 (13, vptp(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok3)
    call getfld2 (14,   uz(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok4)
    call getfld2 (15,   tz(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok5)
    call getfld2 (16,   vz(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok6)
    call getfld2 (17,   wz(1,l), nc4to8("ZONL"),-1,-1,-1, ibuf, maxlat, ok7)

    ok = .false.
    if (ok1.and.ok2.and.ok3.and.ok4.and.ok5.and.ok6.and.ok7) ok = .true.
    if (.not. ok) then
      write(6,6010) nsets
      if (nr == 0)  call xit('epflux4gaschem', -4)  
      call xit('epflux4gaschem',0)  
    endif

    do j = 1, nlat
      upwp(j,l) = upwp(j,l)*wfac(l)
      vptp(j,l) = vptp(j,l)*tfac(l)
      tz(j,l) = tz(j,l)*tfac(l)
      wz(j,l) = wz(j,l)*wfac(l)
    enddo

    nr = nr + 1
  enddo

! ----------------------------------------------------------------
!      do l=1,nlev
!        write(6,6120) l,zlogp(l)*1.e-3,uz(10,l),vz(10,l),wz(1,l)
!      enddo
!6120  format(' l,z,u,v,w=',i3,f6.1,1x,f7.2,2(1x,e14.7))
!      write(6,*) 
! ----------------------------------------------------------------

! Compute vertical derivatives. Ends use one-sided difference;
! interior uses centered difference.

  do j = 1, nlat
    dz = zlogp(1) - zlogp(2)
    uzdz(j,1) = (uz(j,1) - uz(j,2)) / dz
    tzdz(j,1) = (tz(j,1) - tz(j,2)) / dz

    do l = 2, nlevm
      dz = zlogp(l-1) - zlogp(l+1)
      uzdz(j,l) = (uz(j,l-1) - uz(j,l+1)) / dz
      tzdz(j,l) = (tz(j,l-1) - tz(j,l+1)) / dz
    end do

    dz = zlogp(nlevm) - zlogp(nlev)
    uzdz(j,nlev) = (uz(j,nlevm) - uz(j,nlev)) / dz
    tzdz(j,nlev) = (tz(j,nlevm) - tz(j,nlev)) / dz
  enddo

! Work arrays for residual circulation
  do j = 1, nlat
    do l = 1, nlev
      workv(j,l) = dens(l)*vptp(j,l) / tzdz(j,l)
      workw(j,l) = cosy(j)*vptp(j,l) / tzdz(j,l)
    enddo
  enddo

  do j = 1, nlat
    dz = zlogp(1) - zlogp(2)
    vres(j,1) = (workv(j,1) - workv(j,2)) / dz

    do l = 2, nlevm
      dz = zlogp(l-1) - zlogp(l+1)
      vres(j,l) = (workv(j,l-1)-workv(j,l+1)) / dz
    enddo

    dz = zlogp(nlevm) - zlogp(nlev)
    vres(j,nlev) = (workv(j,nlevm)-workv(j,nlev)) / dz
  enddo

! ----------------------------------------------------------------
!      do j=1,nlat
!        do l=1,nlev
!          uz(j,l)=ww*rayon*cosy(j)
!        enddo
!      enddo
! ----------------------------------------------------------------

! Compute latitudinal derivatives. Use centered differences at all points
! (the function goes to zero at the poles).

  do l = 1, nlev
    dy = rad(2) - radsp
    fhat(1,l) = uz(2,l) * cosy(2) / dy
    wres(1,l) = workw(2,l) / dy

    do j = 2, nlatm
      dy = rad(j+1) - rad(j-1)
      fhat(j,l) = (uz(j+1,l)*cosy(j+1) - uz(j-1,l)*cosy(j-1)) / dy
      wres(j,l) = (workw(j+1,l) - workw(j-1,l)) / dy
    enddo

    dy = radnp - rad(nlatm)
    fhat(nlat,l) = -uz(nlatm,l) * cosy(nlatm) / dy
    wres(nlat,l) = -workw(nlatm,l) / dy
  enddo

  do j = 1, nlat
    acs = rayon * cosy(j)
    coriol = om2 * siny(j)

    do l = 1, nlev
      fhat(j,l) = coriol - fhat(j,l) / acs
      vres(j,l) = vz(j,l) - vres(j,l) / dens(l)
      wres(j,l) = wz(j,l) + wres(j,l) / acs
    enddo
  enddo

! ----------------------------------------------------------------
!      do l=1,nlev
!         write(6,6211) l,zlogp(l)*1.e-3,(fhat(j,l),j=1,nlat)
!      enddo
! 6211 format(' l,z,fhat=',i3,f6.1,1x,32(e10.3,1x))     
! ----------------------------------------------------------------

! Compute components of the EP flux vector
  do j = 1, nlat
    acs = rayon * cosy(j)

    do l = 1, nlev
      afct = dens(l) * acs
      epfy(j,l) = afct*(uzdz(j,l) * vptp(j,l) / tzdz(j,l) - upvp(j,l))
      epfz(j,l) = afct*(fhat(j,l) * vptp(j,l) / tzdz(j,l) - upwp(j,l)) 
    enddo
  enddo

! EP flux divergence
  do j = 1, nlat
    dz = zlogp(1) - zlogp(2)
    epfd(j,1) = (epfz(j,1) - epfz(j,2)) / dz

    do l = 2, nlevm 
      dz = zlogp(l-1) - zlogp(l+1)
      epfd(j,l) = (epfz(j,l-1) - epfz(j,l+1)) / dz
    enddo

    dz = zlogp(nlevm) - zlogp(nlev)
    epfd(j,nlev) = (epfz(j,nlevm) - epfz(j,nlev)) / dz
  enddo

  do l = 1, nlev
    acosdy = rayon * cosy(1) * (rad(2)-radsp)
    epfd(1,l) = epfd(1,l) + epfy(2,l)*cosy(2)/acosdy

    do j = 2, nlatm
      acosdy = rayon * cosy(j) * (rad(j+1)-rad(j-1))
      epfd(j,l) = epfd(j,l) &
                + ( epfy(j+1,l)*cosy(j+1) - epfy(j-1,l)*cosy(j-1) ) / acosdy
    enddo

    acosdy = rayon * cosy(nlat) * (radnp-rad(nlatm))
    epfd(nlat,l) = epfd(nlat,l) - epfy(nlatm,l)*cosy(nlatm)/acosdy
  enddo

! Convert EP flux divergence to zonal wind tendency
  do j = 1, nlat
    acs = rayon * cosy(j)
    do l = 1, nlev
      epfd(j,l) = epfd(j,l) / (dens(l)*acs)
    enddo
  enddo

! ----------------------------------------------------------------
!      do l=1,nlev
!        write(6,6150) l,zlogp(l)*1.e-3,vres(10,l),wres(1,l),
!     &                epfd(10,l)*86400.
!      enddo
!6150  format(' l,z,vr,wr,epfd=',i3,f6.1,1x,3(e14.7,1x))
!      write(6,*) 
! ----------------------------------------------------------------

! Put the results into output files.
  do l = 1, nlev
    call setlab (ibuf,-1,-1, nc4to8("EPFY"), lev(l),-1,-1,-1,-1) 
    call putfld2(21, epfy(1,l), ibuf, maxlat)

    call setlab (ibuf,-1,-1, nc4to8("EPFZ"), lev(l),-1,-1,-1,-1) 
    call putfld2(22, epfz(1,l), ibuf, maxlat)

    call setlab (ibuf,-1,-1, nc4to8("EPFD"), lev(l),-1,-1,-1,-1)
    call putfld2(23, epfd(1,l), ibuf, maxlat)

    call setlab (ibuf,-1,-1, nc4to8("VRES"), lev(l),-1,-1,-1,-1)
    call putfld2(24, vres(1,l), ibuf, maxlat)

    call setlab (ibuf,-1,-1, nc4to8("WRES"), lev(l),-1,-1,-1,-1)  
    call putfld2(25, wres(1,l), ibuf, maxlat)
  enddo

! Do next set
  nsets = nsets + 1
  go to 1000

!---------------------------------------------------------------------
 6010 FORMAT(' ',I6,' SETS OF MULTILEVEL RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)

end program epflux4gaschem
