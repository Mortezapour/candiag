      PROGRAM PULL_SONDES_V2
C     PROGRAM PULL_SONDES_V2 (FILE1, FILE2, FILE3, STLIST, INPUT, OUTPUT)
C
C     ------------------------------------------------------
C
C     READ IN A LIST OF SONDE STATIONS, GIVING THE LATITUDE AND LONGITUDE 
C     OF THE STATION, AND INTERPOLATE THE MODEL FIELDS ON TO THE STATION
C     LOCATION TO CREATE A VERTICAL PROFILE.  THE TIME-SERIES AT THE 
C     STATIONS IS OUTPUT TO AN ASCII FILE
C
C  Modifications
C
C     April 10, 2018
C     --- added IGFLAG to specify whether input grid is Gaussian (IGFLAG=1)
C         or lat-lon (IGFLAG=2)
C     March 1, 2017
C     --- increased the hardcoded JPSIZ to 200 to deal with the T63 grid
C         and added an abort if the grid is too large
C     August 23, 2016
C     --- increased the hardcoded maximum time samples (NTMAX) to 400 to
C         deal with six-hourly outputs and added an abort if the number
C         of time samples exceeds NTMAX
C
C INPUT CARD NEEDED (UNIT 5)
C    IGFLAG, ICOORD, PTOIT, DELT
C
C  INPUTS: 
C    FILE1 - 3D field of data for which profiles are to be extracted at
C            the station locations
C    FILE2 - 3D field of temperature matching FILE1 and also to be
C            extracted at the station locations
C    FILE3 - surface pressure in Pa used to calculate pressure on the 
C            model levels
C
COUTPUT FILE...                                                        
C
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_MAXLEV,
     &                       SIZES_LAT,
     &                       SIZES_LON,
     &                       SIZES_LONP1xLATxNWORDIO
C
      implicit none
C
C  -----DOUBLE PRECISION ARRAYS PASSED TO GAUSSG
      REAL*8, DIMENSION(SIZES_LAT)  :: RADL, WL, SL, CL, WOSSL
C
      REAL, DIMENSION(SIZES_LAT) :: GRLAT
      REAL, DIMENSION(SIZES_LON) :: GRLON
C
      LOGICAL OK
      INTEGER lev(SIZES_MAXLEV)
      REAL, DIMENSION(SIZES_MAXLEV) :: eta, acf, bcf
C
      REAL, ALLOCATABLE, DIMENSION(:) ::  gg, temp, fg
C
      INTEGER ibuf, idat1
      COMMON/ICOM/IBUF(8),IDAT1(SIZES_LONP1xLATxNWORDIO)
C
      INTEGER maxx, maxlev
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
      DATA MAXLEV/SIZES_MAXLEV/
C
C  ----- PARAMETERS READ FROM INPUT CARD
      INTEGER igflag, icoord
      REAL delt, ptoit
C
C  ----- ARRAYS TO HOLD STATION DATA
C     ---- possible to process a maximum of 200 stations (NSTMAX) with
C          a maximum of 400 timesteps of data
      INTEGER ntmax, nstmax
      PARAMETER (NTMAX=400, NSTMAX= 200)
C
      CHARACTER*40 XPNAME(NSTMAX)
      REAL, DIMENSION(NSTMAX) :: XPLAT, XPLON
      REAL, DIMENSION(SIZES_MAXLEV,NTMAX,NSTMAX) :: XPDATA
      REAL, DIMENSION(SIZES_MAXLEV,NTMAX,NSTMAX) :: XPTEMP
      REAL, DIMENSION(SIZES_MAXLEV,NTMAX,NSTMAX) :: XPPRES
      REAL, DIMENSION(NTMAX) :: XPTIME
C
      INTEGER, DIMENSION(NSTMAX) :: IPT, JPT
      REAL, DIMENSION(NSTMAX) ::  XPXFR, XPYFR
C
      INTEGER nf, ilev, ilon, ilat, nwds, ilath, nstats
      INTEGER nr, nsl, itim, i1
      REAL fac, dlat, dlon
      REAL xp1, xp2, xp3, xp4, prx1, prx2, sfpr
C
      INTEGER i,j,l,n
C
      INTEGER nc4to8
      EXTERNAL nc4to8
C
C---------------------------------------------------------------------
      NF=7
      CALL JCLPNT(NF,1,2,3,-11,-12,5,6)
C
      REWIND 1
      REWIND 2
      REWIND 3
C
C READ INPUT CARD(1)
      READ(5,5010,END=900)igflag, icoord, ptoit, delt
C
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0)
      ELSE
        PTOIT=MAX(PTOIT, 0.100E-09)
      ENDIF
C
      WRITE(6,6010)igflag, icoord, ptoit, delt
C
      IF(IGFLAG.NE.1 .AND. IGFLAG.NE.2) CALL     XIT('PULLST',-1)
C
C     * READ FIRST RECORD TO GET GRID INFORMATION
C
      CALL FILEV (LEV,ILEV,IBUF,1)
      IF(ILEV.LT.1 .OR. ILEV.GT.MAXLEV)  CALL    XIT('PULLST',-2)
      WRITE(6,6007) ILEV,(LEV(L),L=1,ILEV)
C
      ILON = IBUF(5)
      ILAT = IBUF(6)
      NWDS = IBUF(5)*IBUF(6)
C
      IF(NWDS .GT. MAXX) CALL                    XIT('PULLST',-3)
C
      CALL LVDCODE(ETA,LEV,ILEV)
C
      DO L=1,ILEV
        ETA(L) = 0.001*ETA(L)
      ENDDO
      CALL COORDAB(ACF,BCF, ILEV, ETA, ICOORD, PTOIT)
c
      DO L=1,ILEV
         write(6,*)l,eta(l),acf(l),bcf(l)
      ENDDO
C
C  ----- allocate local arrays
C
      ALLOCATE(GG(NWDS*ILEV))
      ALLOCATE(TEMP(NWDS*ILEV))
      ALLOCATE(FG(NWDS))
C
      IF(igflag .EQ. 1) THEN
        ILATH = ILAT / 2
        CALL GAUSSG (ILATH,SL,WL,CL,RADL,WOSSL)
        CALL TRIGL (ILATH,SL,WL,CL,RADL,WOSSL)
C
        FAC=180.0/ACOS(-1.0)
        DO i = 1,ilat
           grlat(i)=REAL(radl(i)*fac)
        ENDDO
      ELSEIF(igflag .EQ. 2) THEN
        dlat=180.0/FLOAT(ilat)
        grlat(1) = -90.0 + 0.5*dlat
        DO i=2,ilat
          grlat(i) = grlat(i-1) + dlat
        ENDDO
      ENDIF
C
      dlon=360.0/FLOAT(ilon-1)
      DO i=1,ilon
        grlon(i) = FLOAT(i-1)*dlon
      ENDDO
C
      WRITE(6,6020)ILON,ILAT
      write(6,*)' MODEL GRID LATITUDES '
      write(6,'(6F13.8)')(GRLAT(I),I=1,ILAT)
      write(6,*)' MODEL GRID LONGITUDES '
      write(6,'(6F13.8)')(GRLON(I),I=1,ILON)
C
C --- READ IN THE LIST OF STATIONS TO PROCESS
C  ---- REMOVE ANY NEGATIVE LONGITUDES (WEST) TO MAKE THE
C       LONGITUDES RUN FROM 0 TO 360
      READ(11,*) NSTATS
      DO N=1,NSTATS
        READ(11,'(A)') XPNAME(N)
        READ(11,*)XPLAT(N),XPLON(N)
        IF(XPLON(N).LT.0.0) XPLON(N) = XPLON(N)+360.0
      ENDDO
C
C --- FIND THE LOCATION OF THE STATIONS IN THE GCM GRID
      DO N=1,NSTATS
        IPT(N)=0
        DO I=1,ILON
          IF(GRLON(I).LT.XPLON(N)) IPT(N)=I
        ENDDO
C
        JPT(N)=0
        DO J=1,ILAT
          IF(GRLAT(J).LT.XPLAT(N)) JPT(N)=J
        ENDDO
C
C --- FOR STATIONS BETWEEN THE POLE AND THE FIRST GAUSSIAN LATITUDE 
C     TAKE THE GRID VALUE CLOSEST TO THE STATION
        IF(JPT(N).EQ.0) THEN
          XPXFR(N)=0.0
          JPT(N)=1
        ELSEIF(JPT(N).EQ.ILAT) THEN
          XPXFR(N)=1.0
          JPT(N)=ILAT-1
        ELSE
          XPXFR(N) = (XPLON(N)-GRLON(IPT(N)))/
     1               (GRLON(IPT(N)+1)-GRLON(IPT(N)))
        ENDIF
        XPYFR(N) = ABS( (XPLAT(N)-GRLAT(JPT(N)))/
     1                (GRLAT(JPT(N)+1)-GRLAT(JPT(N))) )
C
        WRITE(6,'(A20,2F8.2,I5)')xpname(n),xplat(n),xplon(n)
        WRITE(6,'(2I5,2F8.2)')ipt(n),jpt(n),xpxfr(n),xpyfr(n)
c
      ENDDO
C
      NR=0
C
 100  CALL GETSET2(1,GG,LEV,NSL,IBUF,MAXX,OK)
      IF(NR.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK) GOTO 999
C
      ITIM=IBUF(2)
C
C --- GET THE 3-D TEMPERATURE
      DO l=1,ilev
        CALL GETFLD2(2,TEMP((l-1)*ilon*ilat+1),NC4TO8("GRID"),
     1               ITIM,NC4TO8("TEMP"),LEV(l),IBUF,MAXX,OK)
        IF(NR.EQ.0) WRITE(6,6035) IBUF
        IF(.NOT.OK) CALL                    XIT('PULLST',-20-L)
      ENDDO
C
C --- GET SURFACE PRESSURE FOR THIS STEP
      CALL GETFLD2(3,FG,NC4TO8("GRID"),ITIM,NC4TO8("SFPR"),
     1                1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                         XIT('PULLST',-5)
      IF(NR.EQ.0) WRITE(6,6035) IBUF
C
      XPTIME(NR+1) = FLOAT(IBUF(2))*DELT/(365.0*86400.0)
      DO N=1,NSTATS
C
C ---- INTERPOLATE THE SURFACE PRESSURE TO THE STATION LOCATION
        XP1 = FG((JPT(N)-1)*ILON +IPT(N))
        XP2 = FG((JPT(N)-1)*ILON +IPT(N)+1)
        XP3 = FG((JPT(N)  )*ILON +IPT(N))
        XP4 = FG((JPT(N)  )*ILON +IPT(N)+1)
        PRX1 = (1.0 - XPXFR(N))*XP1 + XPXFR(N)*XP2
        PRX2 = (1.0 - XPXFR(N))*XP3 + XPXFR(N)*XP4
        SFPR = (1.0 - XPYFR(N))*PRX1 + XPYFR(N)*PRX2
C
        DO L=1,ILEV
          XPPRES(L,NR+1,N) = ACF(L) + SFPR*BCF(L)
          I1 = (L-1)*ILON*ILAT
C
          XP1 = GG(I1+ (JPT(N)-1)*ILON +IPT(N))
          XP2 = GG(I1+ (JPT(N)-1)*ILON +IPT(N)+1)
          XP3 = GG(I1+ (JPT(N)  )*ILON +IPT(N))
          XP4 = GG(I1+ (JPT(N)  )*ILON +IPT(N)+1)
          PRX1 = (1.0 - XPXFR(N))*XP1 + XPXFR(N)*XP2
          PRX2 = (1.0 - XPXFR(N))*XP3 + XPXFR(N)*XP4
          XPDATA(L,NR+1,N) = (1.0 - XPYFR(N))*PRX1 + XPYFR(N)*PRX2
C
          XP1 = TEMP(I1+ (JPT(N)-1)*ILON +IPT(N))
          XP2 = TEMP(I1+ (JPT(N)-1)*ILON +IPT(N)+1)
          XP3 = TEMP(I1+ (JPT(N)  )*ILON +IPT(N))
          XP4 = TEMP(I1+ (JPT(N)  )*ILON +IPT(N)+1)
          PRX1 = (1.0 - XPXFR(N))*XP1 + XPXFR(N)*XP2
          PRX2 = (1.0 - XPXFR(N))*XP3 + XPXFR(N)*XP4
          XPTEMP(L,NR+1,N) = (1.0 - XPYFR(N))*PRX1 + XPYFR(N)*PRX2
        ENDDO
      ENDDO
C
      NR=NR+1
C
      GOTO 100
C 
 999  CONTINUE
C
      WRITE(6,6030) NR
      IF(NR.GT.NTMAX) CALL                    XIT('PULLST',-5)
      IF(NR.EQ.0)THEN
          CALL                                XIT('PULLST',-6)
      ELSE
        WRITE(12,'(A20,I6)')' NUMBER OF TIMES ',NR
        DO J=1,NSTATS
          WRITE(12,'(A45,2I4)')'STATION: '//XPNAME(J),
     1                IPT(J),JPT(J)
          DO I=1,NR
            WRITE(12,'(F11.6)')XPTIME(I)
            DO L=1,ILEV
              WRITE(12,'(1P3E13.6)') XPPRES(L,I,J),
     1            XPTEMP(L,I,J),XPDATA(L,I,J)
            ENDDO
          ENDDO
        ENDDO
        CALL                                  XIT('PULLST', 0)
      ENDIF
C
 900  CALL                                    XIT('PULLST',-7)
C
C---------------------------------------------------------------------
 5010 FORMAT(10X,I2,1X,A4,2E10.0)
 6007 FORMAT(' ILEV,ETA =',I5,2X,20I5/(18X,20I5))
 6010 FORMAT(' GAUSSIAN (1) OR LAT/LON (2) ',I2,
     1        ' COORD= ',A4,' PTOIT= ',E10.4,' DELT = ',F7.1)
 6020 FORMAT(' FIELD SIZE= ', 2I6)
 6030 FORMAT('0 PULLST PROCESSED ',I5,' SETS') 
 6035 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END 
