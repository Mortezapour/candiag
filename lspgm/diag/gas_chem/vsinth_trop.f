      PROGRAM VSINTH_TROP
C     PROGRAM VSINTH (XIN,     LNSP,      XOUT,      INPUT,      OUTPUT,)
C    1         TAPE1=XIN,TAPE2=LNSP,TAPE3=XOUT,TAPE5=INPUT,TAPE6=OUTPUT)
C     ------------------------------------------------------------------
C
C     Jan  7/13 - D. Plummer - ADDED OUTPUT OF THE TROPOPAUSE PRESSURE
C                   CALCULATED HERE
C     Aug 11/11 - D. PLUMMER (INCORPORATED SOME MISSED UPDATES FOR
C                   COMPATABILITY WITH F90 AND LINUX PLATFORMS)
C     Dec 05/05 - D. PLUMMER (STARTED WITH VSINTH, MODIFIED TO INTEGRATE
C                   ONLY UP TO THE LOCALLY DIAGNOSED TROPOPAUSE - DEFINED
C                   'SIMILARLY' TO WMO 2K/KM)
C     DEC 06/93 - D. LIU   (CHANGED SIGMA_TAU)
C     JAN 14/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     FEB 16/88 - R.LAPRISE.
C
CVSINTH  - VERTICAL SIGMA INTEGRAL FOR DATA ON ETA (SIGMA/HYBRID)
C          COORDINATES
C
CAUTHOR  - R. LAPRISE
C
CPURPOSE - COMPUTES THE VERTICAL SIGMA INTEGRAL FOR DATA ON ETA                 J3
C          (SIGMA/HYBRID) COORDINATES.                                          J3
C          UPWARD COMPATIBLE WITH FORMER VERSION (VSINT), EXCEPT FOR            J3
C          AN EXTRA INPUT FILE LNSP WHICH IS NOW REQUIRED, AND THE              J3
C          FACT THAT SPECTRAL FIELDS CAN NOT BE HANDLED IN ETA COORD.           J3
C                                                                               J3
CINPUT FILE...                                                                  J3
C                                                                               J3
C      XIN  = INPUT SERIES OF ETA LEVEL GRID FIELDS .                           J3
C      LNSP = INPUT SERIES OF LN(SURFACE PRESSURE) IN MB.                       J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      XOUT = OUTPUT SERIES OF VERTICAL SIGMA INTEGRALS OF XIN.                 J3
C
CINPUT PARAMETERS...
C                                                                               J5
C      LEVTYP = FULL FOR MOMENTUM VARIABLE, AND                                 J5
C               HALF FOR THERMODYNAMIC ONE.                                     J5
C      CONST  = SCALES THE OUTPUT FIELD.                                        J5
C      LAY    = DEFINES THE POSITION OF LAYER INTERFACES IN RELATION            J5
C               TO LAYER CENTRES (SEE BASCAL).                                  J5
C               (ZERO DEFAULTS TO THE FORMER STAGGERING CONVENTION).            J5
C      ICOORD = 4H ETA/4H SIG FOR ETA/SIGMA LEVELS OF INPUT FIELDS.             J5
C      SIGTOP = VALUE OF SIGMA AT TOP OF DOMAIN FOR VERTICAL INTEGRAL.          J5
C               IF .LT.0., THEN INTERNALLY DEFINED BASED ON LEVTYP              J5
C               FOR UPWARD COMPATIBILITY.                                       J5
C      PTOIT  = PRESSURE (PA) OF THE RIGID LID OF THE MODEL.                    J5
C                                                                               J5
C      NOTE - LAY AND LEVTYP DEFINE THE TYPE OF LEVELLING FOR THE VARIABLE.     J5
C                                                                               J5
CEXAMPLE OF INPUT CARD...                                                       J5
C                                                                               J5
C*VSINTH.  HALF        1.    0  SIG  -1.        0.                              J5
C------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_LAT,
     &                       SIZES_MAXLEV 
      
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,SPEC

      INTEGER LEV(SIZES_MAXLEV)

      REAL ETA(SIZES_MAXLEV), ETAB(SIZES_MAXLEV)
      REAL ABM(SIZES_MAXLEV), BBM(SIZES_MAXLEV)
      REAL ABB(SIZES_MAXLEV), BBB(SIZES_MAXLEV)
C
      COMMON /LEVELS/ SIGB(SIZES_LONP1xLAT,2), SIGM(SIZES_LONP1xLAT,2)
      COMMON /BLANCK/ PSMBLN(SIZES_LONP1xLAT), ACC(SIZES_LONP1xLAT), 
     1                GG(SIZES_LONP1xLAT), TEMP(SIZES_LONP1xLAT,2)
      REAL            PRESS(SIZES_LONP1xLAT)
      EQUIVALENCE     (PRESS,PSMBLN)


      REAL, ALLOCATABLE, DIMENSION(:,:) :: DTDZ, PRLEVS, TALEVS
      INTEGER, ALLOCATABLE, DIMENSION(:) :: LFLAG
      
      COMMON /ICOM  /IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)

      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXLEV/SIZES_MAXLEV/
C---------------------------------------------------------------------
      NFIL = 8
      CALL JCLPNT(NFIL,1,2,3,11,12,13,5,6)
      REWIND 1
      REWIND 2
      REWIND 3
      REWIND 11
      REWIND 12

C     * READ CONTROL DIRECTIVES.

      READ(5,5010,END=991) LEVTYP,CONST,LAY,ICOORD,SIGTOP,PTOIT
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00)
      ELSE
        PTOIT=MAX(PTOIT, 0.100E-09)
      ENDIF
      WRITE(6,6005) LEVTYP,CONST,LAY,ICOORD,SIGTOP,PTOIT

C     * GET ETA VALUES OF LAYER CENTRES FROM XIN.

      CALL FILEV (LEV,ILEV,IBUF,1)
      IF(ILEV.LT.1 .OR. ILEV.GT.MAXLEV)  CALL      XIT('VSINTH',-1)
      WRITE(6,6007) ILEV,(LEV(L),L=1,ILEV)

      CALL LVDCODE(ETA,LEV,ILEV)
      DO 150 L=1,ILEV
         ETA(L)=ETA(L)*.001
  150 CONTINUE

C     * TROPOPAUSE SEARCH STARTS AROUND 0.5 SIGMA TO AVOID SURFACE
C     * INVERSIONS

      IST=0
      DO L=ILEV,1,-1
        IF(ETA(L).LE.0.5.AND.IST.EQ.0) IST=L
      ENDDO
      write(6,*)'::::: IST = ',ist

C     * DETERMINE THE FIELD SIZE.

      KIND=IBUF(1)
      NAME=IBUF(3)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) THEN
         IF(ICOORD.NE.NC4TO8(" SIG")) CALL         XIT('VSINTH',-2)
         NWDS=2*NWDS
      ENDIF
C
C     ALLOCATE LOCAL ARRAYS
C
      ALLOCATE(DTDZ(NWDS,ILEV))
      ALLOCATE(PRLEVS(NWDS,ILEV))
      ALLOCATE(TALEVS(NWDS,ILEV))
      ALLOCATE(LFLAG(NWDS))
      
C     * EVALUATE LAYER INTERFACES FROM LEVTYP AND LAY.
      IF    (LEVTYP.EQ.NC4TO8("FULL")) THEN
         CALL BASCAL (ETAB,IBUF, ETA,ETA,ILEV,LAY)
      ELSEIF(LEVTYP.EQ.NC4TO8("HALF")) THEN
         CALL BASCAL (IBUF,ETAB, ETA,ETA,ILEV,LAY)
      ELSE
         CALL                                      XIT('VSINTH',-3)
      ENDIF

C---------------------------------------------------------------------
      NSETS=0
  200 CONTINUE

C     * READ NEXT LN(PS) NEEDED TO EVALUATE THE SIGMA LEVELS.

      IF(SPEC) THEN
         ITIM=-1
         DO 250 I=1,NWDS
            PRESS(I)=0.
  250    CONTINUE
      ELSE
         CALL GETFLD2 (2,PSMBLN,KIND,-1,NC4TO8("LNSP"),1,IBUF,MAXX,OK)
         IF(NSETS.EQ.0) WRITE(6,6025) IBUF
         IF(.NOT.OK) THEN
            IF(NSETS.GT.0) THEN
               WRITE(6,6025) IBUF
               WRITE(6,6020) NSETS
               CALL                                XIT('VSINTH',0)
            ELSE
               CALL                                XIT('VSINTH',-4)
            ENDIF
         ENDIF
         ITIM=IBUF(2)
         DO 300 I=1,NWDS
            PRESS(I)=100.*EXP(PSMBLN(I))
  300    CONTINUE
      ENDIF

C     * EVALUATE THE PARAMETERS A AND B OF THE VERTICAL DISCRETIZATION
C     * PERFORMED FOR BOTH THE LAYER MIDPOINTS AND LOWER FACE

      CALL COORDAB (ABM,BBM, ILEV,ETA ,ICOORD,PTOIT)
      CALL COORDAB (ABB,BBB, ILEV,ETAB,ICOORD,PTOIT)

C     * CALCULATE THE LAPSE RATE IN UNITS OF DEGREES C/KM

      CNST1 = -8.31447*0.001/(0.02896*9.80665)
      IGH = 1
      LOW = 2

      CALL NIVCAL  (SIGM(1,IGH), ABM(1),BBM(1),PRESS,1,NWDS,NWDS)
      DO I=1,NWDS
        PRLEVS(I,1) = SIGM(1,IGH)*PRESS(I)
      ENDDO

      CALL GETFLD2 (3,TEMP(1,IGH),KIND,ITIM,NC4TO8("TEMP"),LEV(1),
     1                 IBUF,MAXX,OK)
      DO I=1,NWDS
        TALEVS(I,1) = TEMP(I,IGH)
      ENDDO

      DO 700 L=2,ILEV
         CALL GETFLD2 (3,TEMP(1,LOW),KIND,ITIM,NC4TO8("TEMP"),LEV(L),
     1                 IBUF,MAXX,OK)
         DO I=1,NWDS
           TALEVS(I,L) = TEMP(I,LOW)
         ENDDO
C
         CALL NIVCAL  (SIGM(1,LOW), ABM(L),BBM(L),PRESS,1,NWDS,NWDS)
         DO I=1,NWDS
           PRLEVS(I,L) = SIGM(I,LOW)*PRESS(I)
         ENDDO
C
         DO I=1,NWDS
            DTDZ(I,L) = (TEMP(I,IGH)-TEMP(I,LOW))/(CNST1*
     1                  0.5*(TEMP(I,LOW)+TEMP(I,IGH))*
     2                  LOG(SIGM(I,IGH)/SIGM(I,LOW)))
         ENDDO
C
         KEEP= LOW
         LOW = IGH
         IGH = KEEP
C
 700  CONTINUE

C     * FIND THE MODEL TROPOPAUSE

      DO I=1,NWDS
         LFLAG(I)=0
      ENDDO

      DO 740 L=IST,4,-1
        DO 750 I=1,NWDS
cxx          IF(DTDZ(I,L).GE.-2.0.AND.DTDZ(I,L-1).GE.-2.0
cxx     1            .AND.DTDZ(I,L-2).GE.-2.0
cxx     2            .AND.LFLAG(I).EQ.0) LFLAG(I)=L
          IF(DTDZ(I,L).GE.-2.0.AND.
     1         0.5*(DTDZ(I,L-1)+DTDZ(I,L-2)).GE.-2.0
     2         .AND.LFLAG(I).EQ.0) LFLAG(I)=L

 750    CONTINUE
 740  CONTINUE

      DO I=1,NWDS
       IF(LFLAG(I).LT.1)
     1      WRITE(6,*)'WARNING - NO TROPOPAUSE FOUND'
       IF(LFLAG(I).EQ.IST) THEN
          WRITE(6,*)'TROPOPAUSE VERY LOW'
cxx          DO L=1,ILEV
cxx            WRITE(6,'(I4,1P2E12.4)')L,PRLEVS(I,L),DTDZ(I,L)
cxx          ENDDO
       ENDIF
      ENDDO

C     * INTEGRATE COLUMN UP TO DIAGNOSED TROPOPAUSE

      IGH = 1
      LOW = 2
      DO 800 I=1,NWDS
         ACC (I)     = 0.
  800 CONTINUE

      IF(SIGTOP.LT.0.)THEN
         IF(LEVTYP.EQ.NC4TO8("FULL"))THEN
            DO 840 I=1,NWDS
               SIGB(I,IGH)=0.0
  840       CONTINUE
         ELSE
            DO 850 I=1,NWDS
               SIGB(I,IGH)=PTOIT/PRESS(I)
  850       CONTINUE
         ENDIF
      ELSE
         DO 860 I=1,NWDS
            SIGB(I,IGH)=SIGTOP
  860    CONTINUE
      ENDIF

C     * PROCESS ALL LEVELS AND ACCUMULATE SIGMA VERTICAL INTEGRAL.

      DO 900 L=1,ILEV

         CALL GETFLD2 (1,GG,KIND,ITIM,NAME,LEV(L),IBUF,MAXX,OK)
         IF(NSETS.EQ.0) WRITE(6,6025) IBUF
         IF(.NOT.OK)  CALL                         XIT('VSINTH',-10-L)

         CALL NIVCAL  (SIGB(1,LOW), ABB(L),BBB(L),PRESS,1,NWDS,NWDS)

         DO 910 I=1,NWDS
            IF(L.GE.LFLAG(I)) THEN
              ACC(I) = ACC(I) + GG(I)*CONST*(SIGB(I,LOW)-SIGB(I,IGH))
            ENDIF
  910    CONTINUE

         KEEP= LOW
         LOW = IGH
         IGH = KEEP

  900 CONTINUE

      DO I=1,NWDS
        IF(ACC(I)*PRESS(I).GT.100.0) THEN
          WRITE(6,*)'LARGE TROPOSPHERIC COLUMN ', acc(i)*press(i)
          write(6,*)lflag(i)
cxx          DO L=1,ILEV
cxx            write(6,'(I4,1P2E12.4)') l,prlevs(i,l),dtdz(i,l)
cxx          ENDDO
        ENDIF
      ENDDO

C     * PUT THE RESULT ONTO FILE XOUT.

      IBUF(4)=1
      CALL PUTFLD2 (11,ACC,IBUF,MAXX)

C     * WRITE OUT THE TROPOPAUSE PRESSURE

      DO I=1,NWDS
        ACC(I) = 0.5*(PRLEVS(I,LFLAG(I)) + PRLEVS(I,LFLAG(I)-1))
      ENDDO
      CALL PUTFLD2 (12,ACC,IBUF,MAXX)

C     * WRITE OUT THE TROPOPAUSE TEMPERATURE

      DO I=1,NWDS
        ACC(I) = 0.5*(TALEVS(I,LFLAG(I)) + TALEVS(I,LFLAG(I)-1))
      ENDDO
      CALL PUTFLD2 (13,ACC,IBUF,MAXX)

      NSETS=NSETS+1

      GO TO 200

C     * E.O.F. ON INPUT.

  991 CALL                                         XIT('VSINTH',-5)
C---------------------------------------------------------------------
 5010 FORMAT(11X,A4,E10.0,I5,1X,A4,E5.0,E10.0,E10.0)                           J4
 6005 FORMAT('  LEVTYP = ',A4,', CONST = ',1P,E12.4,
     1       ', LAY = ',0P,I5,', COORD=',1X,A4,', SIGTOP =',F15.10,
     2       ', P.LID (PA)=',E10.3)
 6007 FORMAT(' ILEV,ETA =',I5,2X,20I5/(18X,20I5))
 6020 FORMAT('0',I6,' SETS WERE PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,5I10)
      END
