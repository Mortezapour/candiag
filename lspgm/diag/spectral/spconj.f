      PROGRAM SPCONJ
C     PROGRAM SPCONJ (X,       Y,       OUTPUT,                         )       E2
C    1          TAPE1=X, TAPE2=Y, TAPE6=OUTPUT) 
C     -----------------------------------------                                 E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 13/83 - R.LAPRISE.                                                    
C     FEB 14/80 - J.D.HENDERSON 
C                                                                               E2
CSPCONJ  - COMPLEX CONJUGATE OF A COMPLEX FILE                          1  1    E1
C                                                                               E3
CAUTHOR  - J.D.HENDERSON                                                        E3
C                                                                               E3
CPURPOSE - FILE ARITHMETIC PROGRAM THAT COMPUTES THE COMPLEX CONJUGATE (Y)      E3
C          OF A COMPLEX FILE (X). (I.E. Y = CONJG(X)).                          E3
C          NOTE - X MUST BE COMPLEX.                                            E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      X = COMPLEX FILE                                                         E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      Y = COMPLEX CONJUGATE OF X                                               E3
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     Load diag size parameters
      integer, parameter :: 
     & MAXC=MAX((SIZES_LMTP1+1)*SIZES_LAT,(SIZES_LA+SIZES_LMTP1))
      integer, parameter ::
     & MAXX = 2*MAXC*SIZES_NWORDIO ! defined as a max buffer size for I/O 

      COMPLEX A 
      COMMON/BLANCK/A(MAXC) 
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(MAXX) 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEXT FIELD.
C 
      NR=0
  140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('SPCONJ',-1) 
        WRITE(6,6020) NR
        CALL                                       XIT('SPCONJ',0)
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6030) IBUF
C 
C     * MAKE SURE THAT THE FIELD IS COMPLEX.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(.NOT.SPEC) CALL                           XIT('SPCONJ',-2) 
C 
C     * COMPUTE THE CONJUGATE.
C 
      NWDS=IBUF(5)*IBUF(6)
      DO 200 I=1,NWDS 
  200 A(I)=CONJG(A(I))
C 
C     * SAVE THE RESULT.
C 
      CALL PUTFLD2(2,A,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6030) IBUF
      NR=NR+1 
      GO TO 140 
C-----------------------------------------------------------------------
 6020 FORMAT('0SPCONJ READ',I6,' RECORDS')
 6030 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
