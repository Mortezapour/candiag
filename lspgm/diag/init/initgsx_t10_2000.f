      program initgsx

!     program initgsx (ictl,      gpinit,     gpspec,   gsinit,  output)
!                  tape1=control, tape2=gp, tape3=gp_x, tape4=species_adv
!     ----------------------------------------------------------------
!
! Purpose: Convert a pressure-level gaussian-grid dataset of tracer model
!          variables to a sigma/hybrid-level grid. Based on subroutine
!          initgs8, but for species only.
!
! History:
! D. Plummer, October 28, 2016
!      - remove seasonal background aerosol fields (BGA1 and BGA2) from
!        initialization since these are no longer used.
!
! Input files:
!       ictl   = Initialization control dataset,
!                called "control" in inatmo12.cdk.
!       gpinit = Pressure level gaussian grids for model variables. Used only
!                to extract surface pressure for vertical interpolation.
!                Called "gp" in inatmo12.cdk.
!       gpspec = Pressure level gaussian grids for species,
!                called "gp_x" in inatmo12.cdk.
!
!
!       NAME  VARIABLE          UNITS
!       ps    surface pressure  pa
!       x     species           vmr
!
! Output file:
!       gsinit ("species_adv") = sigma/hybrid level gaussian grids of...
!
!       NAME  VARIABLE          UNITS    LEVELS
!       x     species           vmr      ilev
!----------------------------------------------------------------------------

      use diag_sizes, only : sizes_lonp1Xlat,
     &                       sizes_lonp1XlatXnwordio,
     &                       sizes_maxlev,
     &                       sizes_maxlevXlonp1Xlat

      implicit real (a-h,o-z),
     +integer (i-n)

      integer, parameter :: jpmxlat=96, jpmxa=2210, jpmxb=2145

      common /lat1/ sl(jpmxlat),cl(jpmxlat),wl(jpmxlat), wossl(jpmxlat)
      common /lat2/ rad(jpmxlat), alp(jpmxa), epsi(jpmxa)
      common /lat3/ ilg, ilat, la, lm, lrlmt, lsr(2,66)

      complex :: sstrac
      common /specsig/ sstrac(jpmxb)

      integer :: ibuf(8), idat(sizes_lonp1XlatXnwordio)
      common/icom/ ibuf, idat
      real, allocatable, dimension(:) :: fa, xa, ya, za, sp 

      logical :: ok

      integer :: kbuf(8)
      integer :: lp(sizes_maxlev), lg(sizes_maxlev), lh(sizes_maxlev)

      real :: ah(sizes_maxlev), bh(sizes_maxlev), dfdlnp(sizes_maxlev+1)
      real :: sh(sizes_maxlev), pr(sizes_maxlev), prlog(sizes_maxlev)
      real :: sg(sizes_maxlev), fpcol(sizes_maxlev), dlnp(sizes_maxlev)

      data ij/sizes_lonp1XlatXnwordio/,
     &     ijk/sizes_maxlevXlonp1Xlat/, maxlev/sizes_maxlev/

!     Initialisation of long-lived species
      integer, parameter :: jpntrci=34    !> number of long-lived species
      character (len=4) :: cspl(jpntrci)  !> names of long-lived species

      data  cspl(1) /'  OX'/    ! Ox   : O3+O+O1D
      data  cspl(2) /'  NX'/    ! NOx  : NO+NO2+NO3+2*N2O5
      data  cspl(3) /'  C9'/    ! ClOx : Cl ClO + HOCl + OClO + ClONO2 + Cl2O2 
      data  cspl(4) /'  BX'/    ! BrOX : Br+BrO+HOBr+BrONO2+BrCl
      data  cspl(5) /'  N5'/    ! HNO3
      data  cspl(6) /'  HX'/    ! HOX
      data  cspl(7) /'  N7'/    ! HNO4
      data  cspl(8) /'  T5'/    ! N2O  
      data  cspl(9) /'  HB'/    ! HBr
      data  cspl(10)/'  MM'/    ! CH4
      data  cspl(11)/'  CO'/    ! CO
      data  cspl(12)/'  HA'/    ! CH2O
      data  cspl(13)/'  MH'/    ! CH3OOH
      data  cspl(14)/'  HD'/    ! H2
      data  cspl(15)/'  C7'/    ! HCl
      data  cspl(16)/'  C4'/    ! ClONO2
      data  cspl(17)/'  F1'/    ! F-11(CFCl3)
      data  cspl(18)/'  F2'/    ! F-12(CF2Cl2)
      data  cspl(19)/'  F3'/    ! CCl4
      data  cspl(20)/'  F4'/    ! CH3CCl3
      data  cspl(21)/'  F5'/    ! HCFC-22
      data  cspl(22)/'  F6'/    ! CH3Cl
      data  cspl(23)/'  CB'/    ! CH3Br
      data  cspl(24)/'  BF'/    ! CHBr3
      data  cspl(25)/'  DB'/    ! CH2Br2
      data  cspl(26)/'  NY'/    ! NOy = NX + N5 + N7
      data  cspl(27)/'  CY'/    ! Cl_y = C9+C7+C4
      data  cspl(28)/'  CZ'/    ! Cl_tot = C9+C7+C4 + F1+F2+F3 + F4+F5+F6
      data  cspl(29)/'  BZ'/    ! Brz = BX + HB + CB + 3BF + 2DB
      data  cspl(30)/'  TA'/    ! Tropospheric NOx (hybrid)
      data  cspl(31)/'  TB'/    ! Tropospheric HNO3 (hybrid)
      data  cspl(32)/'  X1'/    ! Diagnostic tracer - strato. O3
      data  cspl(33)/'  X2'/    ! Diagnostic tracer - e90
      data  cspl(34)/'  X3'/    ! Diagnostic tracer - age of air
! -----------------------------------------------------------------------

! Call JCLPNT to associate filenames to unit numbers. NFIL is the number of
! files to which unit numbers are to be assigned. The integers following
! NFIL are the unit numbers to use. The files themselves are the four arguments
! to the call to subroutine initgsx_t10_2000; see header notes above. 
      nfil = 5
      call jclpnt(nfil, 1, 2, 3, 11, 6)

! Get the pressure level values and the analysis grid size from the temperature
! fields in file GPINIT (tape 2 on call; unit=2 after JCLPNT).
      rewind 2
      call find(2,nc4to8("GRID"),-1,nc4to8("TEMP"),-1,ok)
      if (.not. ok) call xit('initgsx',-1)
      call filev(lp, npl, kbuf, -2)
      if ((npl < 1) .or. (npl > maxlev)) call xit('initgsx',-2)

      call lvdcode(pr, lp, npl)
      write(6,6005) npl,(pr(l),l=1,npl)
 6005 format('initgsx_t10_2000: p levels:',i5,20f5.0/('0',18x,20f5.0))

! Read model sigma values from the control file ICTL (tape 1 => unit=1).
      rewind 1
      read(1,end=903) labl,nsl,(sg(l),l=1,nsl)
     &                        ,(sh(l),l=1,nsl),lay,icoord,ptoit,moist
      read(1,end=904) labl,ilg,ilat,ilgm,ilatm,lrlmt,iday,gmt

      write(6,*) "                nsl=", nsl
      if ((nsl < 1) .or. (nsl > maxlev)) call xit('initgsx',-3)

      write(6,6015) lrlmt
 6015 format ('initgsx_t10_2000: lrlmt =', i10)
      write(6,6017) ilg, ilat, ilgm, ilatm
 6017 format ('        Analysis grid:', 2i5, 5x, 'Model grid:',2i5/)
      call writlev (sh, nsl, ' SH ')
      write(6,6016) lay, icoord, ptoit, moist
 6016 format(' LAY=',I5,', COORD=',a4,', P.LID=',f10.3,' (Pa)',
     &       ', MOIST=',A4)
      
!     Stop if there is not enough space.
      ilen = ilat*(ilg+1)
      if (    ilen > ij ) call xit('initgsx',-4)
      if (npl*ilen > ijk) call xit('initgsx',-5)
      if (nsl*ilen > ijk) call xit('initgsx',-6)

!     Allocate arrays
      allocate(fa(sizes_maxlevXlonp1Xlat), xa(sizes_maxlevXlonp1Xlat),
     &         ya(sizes_maxlevXlonp1Xlat), za(sizes_maxlevXlonp1Xlat),
     &         sp(sizes_lonp1Xlat))

      call siglab2 (lg,lh, sg,sh, nsl)
      call coordab (ah,bh, nsl,sh, icoord,ptoit)

      ilath = ilat / 2

      call dimgt  (lsr, la, lr, lm, ktr, lrlmt)
      call epscal (epsi, lsr, lm)
      call gaussg (ilath, sl, wl, cl, rad, wossl)
      call  trigl (ilath, sl, wl, cl, rad, wossl)

!     Perform the vertical interpolation of species
      call ptsix (2, 3, 11, fa, xa, ya, za, sp, ilen, npl, pr, nsl,
     &            lp, lh, ah, bh, npl+1, fpcol, dfdlnp, dlnp, sg,
     &            prlog, ibuf, ij+8, cspl, jpntrci)
      call xit('initgsx', 0)

  903 call xit('initgsx',-7)
  904 call xit('initgsx',-8)

      end program initgsx

! -----------------------------------------------------------------------------

      subroutine ptsix (nf2, nf3, nfo, fa, xa, ya, za, sp, ilen, npl,
     &                  pr, nsl, lp, lh, ah, bh, npl1, fpcol, dfdlnp,
     &                  dlnp, sig, prlog, ibuf, maxp8, cspl, jpntrci) 

!     SEPT 00 - J. DE GRANDPRE. - PREVIOUS VERSION PTSH.
!     AUG 13/90 - M.LAZARE. - PREVIOUS VERSION PTSH.
!
!     Convert NPL pressure levels of gaussian grids (ILG,ILAT)
!     on file NF3 to NSL eta levels on file NFO for hybrid model.

      integer, parameter :: jpmxlat=96, jpmxa=2210, jpmxb=2145

      common /lat1/ sl(jpmxlat), cl(jpmxlat), wl(jpmxlat),wossl(jpmxlat)
      common /lat2/ rad(jpmxlat), alp(jpmxa), epsi(jpmxa)
      common /lat3/ ilg, ilat, la, lm, lrlmt, lsr(2,66)
      real :: wrks(546,2), wrkl(728)

      complex :: sstrac
      common /specsig/ sstrac(jpmxb)

      logical :: ok
  
      real sp(ilen)
      real fa(ilen,npl)
      real xa(ilen,npl)
      real ya(ilen,npl) ! special case to create Cloy
      real za(ilen,npl) ! special case to create inorganic fraction of Cly
                                         !<                    fraction of Cly
      real :: AH     (NSL) ,BH   (NSL) 
      real :: PR    (NPL ),FPCOL (NPL ),DFDLNP (NPL1),DLNP (NPL) 
      real :: SIG   (NSL ),PRLOG (NPL )

      integer :: lp (npl), lh(nsl)
      integer :: ibuf(maxp8)
      character*4 cspl(jpntrci)

! The lower boundary condition for the six chlorine-containing
! halocarbons plus CH3Br used to create the initial conditions
      real :: haloic(7)
      data haloic / 2.6345e-10, 6.6082e-10, 9.9370e-11,
     &              5.6148e-11, 2.1337e-10, 5.5001e-10, 1.6653e-11 /

! Specify the target lower boundary conditions for the same seven halocarbons
      real :: halotg(7)
      data halotg / 2.6345e-10, 6.6082e-10, 9.9370e-11,
     &              5.6148e-11, 2.1337e-10, 5.5001e-10, 1.6653e-11 /

!-------------------------------------------------------------------- 
      write(6,*) " "
      maxx = maxp8-8

      do l = 1, npl
        prlog(l) = log(pr(l))
      enddo ! loop 110

! Calculate total initial Cl forcing
      xitlcl = 3.0*haloic(1) + 2.0*haloic(2) + 4.0*haloic(3) +
     &         3.0*haloic(4) + 1.0*haloic(5) + 1.0*haloic(6)


! Calculate the target initial Cl forcing
      xttlcl = 3.0*halotg(1) + 2.0*halotg(2) + 4.0*halotg(3) +
     &         3.0*halotg(4) + 1.0*halotg(5) + 1.0*halotg(6)

! Read surface pressure and convert to LSNP.
      rewind nf2
      call getfld2(nf2, sp, nc4to8("GRID"), -1, nc4to8("  PS"),
     &             1, ibuf, maxx, ok)
      if (.not.ok) call xit('ptsix',-2)

      write(6,6025) (ibuf(ind),ind=1,8)
      do i=1,ilen
         sp(i)=log(sp(i))
      enddo  ! loop 210

! Initial work to calculate the [inorganic Cl]/[total Cl] fraction,
! to be used for setting up the HCl/ClOy and halogens
      rewind nf3

      call nom (nam,'  C9')
      do l = 1, npl
        call getfld2 (nf3, xa(1,l), nc4to8("GRID"), -1,
     &                nam, lp(l), ibuf, maxx, ok)
        if(.not.ok) call xit('ptsix',-32)
        write(6,6025) (ibuf(ind),ind=1,8)
      enddo

      call nom (nam,'  C7')
      do l = 1, npl
        call getfld2 (nf3, ya(1,l), nc4to8("GRID"), -1,
     &               nam, lp(l), ibuf, maxx, ok)
        if(.not.ok) call xit('ptsix',-32)
        write(6,6025) (ibuf(ind),ind=1,8)
      enddo

      do l = 1, npl
        do il = 1, ilen
          xa(il,l)=xa(il,l)+ya(il,l)
        enddo
      enddo

      call nom(nam,'  C4')
      do l = 1, npl
        call getfld2 (nf3, ya(1,l), nc4to8("GRID"), -1,
     &                nam, lp(l), ibuf, maxx, ok)
        if (.not.ok) call xit('ptsix',-32)
        write(6,6025) (ibuf(ind),ind=1,8)
      enddo

! Initial ClOy = ClOx+HCl+ClONO2
      do l = 1, npl
        do il = 1, ilen
          xa(il,l)=xa(il,l)+ya(il,l)
        enddo
      enddo

! Calculate the total organic chlorine
!
! Read in CFC-11 and add to Clorg
      call nom (nam,'  F1')
      do l=1,npl
        call getfld2(nf3, za(1,l),nc4to8("GRID"),-1,
     1               nam,lp(l),ibuf,maxx,ok)
        if (.not.ok) CALL xit('ptsix',-32)
        write(6,6025) (ibuf(ind),ind=1,8)
      enddo

      do l=1,npl
        do il=1,ilen
          za(il,l) = 3.0*za(il,l)
        enddo
      enddo

! Read in CFC-12 and add to Clorg
      call nom(nam,'  F2')
      do L=1,NPL
        call GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               nam,LP(L),IBUF,MAXX,OK)
        if (.NOT.OK) call XIT('PTSIX',-32)
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      enddo

      do l = 1, npl
        do il = 1, ilen
          za(il,l) = za(il,l) + 2.0*ya(il,l)
        enddo
      enddo

! ---- read in CCl4 and add to Clorg
      CALL NOM(NAM,'  F3')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
!
      DO L=1,npl
        DO IL=1,ilen
          ZA(IL,L) = ZA(IL,L) + 4.0*YA(IL,L)
        ENDDO
      ENDDO
!
! ---- read in CH3CCl3 and add to Clorg
      CALL NOM(NAM,'  F4')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO

      DO L=1,npl
        DO IL=1,ilen
          ZA(IL,L) = ZA(IL,L) + 3.0*YA(IL,L)
        ENDDO
      ENDDO

C ---- read in HCFC-22 and add to Clorg
      CALL NOM(NAM,'  F5')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,ilen
          ZA(IL,L) = ZA(IL,L) + YA(IL,L)
        ENDDO
      ENDDO
C
C ---- read in CH3Cl and add to Clorg
      CALL NOM(NAM,'  F6')
      DO L=1,NPL
        CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1               NAM,LP(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL XIT('PTSIX',-32) 
        WRITE(6,6025) (IBUF(IND),IND=1,8)
      ENDDO
C
      DO L=1,npl
        DO IL=1,ilen
          ZA(IL,L) = ZA(IL,L) + YA(IL,L)
        ENDDO
      ENDDO
C
C ---- calculate the inorganic/total fraction
      DO L=1,npl
        DO IL=1,ilen
          FA(IL,L) = XA(IL,L)/(XA(IL,L)+ZA(IL,L))
        ENDDO
      ENDDO
C
C ---- calculate the ratio of the initial total chlorine
C      to the idealized total chlorine
      DO L=1,npl
        DO IL=1,ilen
          ZA(IL,L) = XITLCL/(XA(IL,L)+ZA(IL,L))
        ENDDO
      ENDDO

!     Read species.
      do n = 1, jpntrci
        rewind nf3

! This would be shorter if it tested for the ones that ARE included, out
! of the total 34: OX  NX  N5  HX  N7  T5 MM  CO  HA  MH  HD  BF  DB
! It's on reading OX that it capsizes.

        if (cspl(n) /= '  C9' .and. cspl(n) /= '  C7' .and.
     1      cspl(n) /= '  C4' .and. cspl(n) /= '  F1' .and.
     2      cspl(n) /= '  F2' .and. cspl(n) /= '  F3' .and.
     3      cspl(n) /= '  F4' .and. cspl(n) /= '  F5' .and.
     4      cspl(n) /= '  F6' .and.
     5      cspl(n) /= '  BX' .and. cspl(n) /= '  HB' .and.
     6      cspl(n) /= '  CB' .and.
     7      cspl(n) /= '  NY' .and. cspl(n) /= '  CY' .and.
     8      cspl(n) /= '  CZ' .and. cspl(n) /= '  BZ' .and.
     9      cspl(n) /= '  TA' .and. cspl(n) /= '  TB' .and.
     a      cspl(n) /= '  X1' .and. cspl(n) /= '  X2' .and.
     b      cspl(n) /= '  X3') then

!        if (cspl(n) == '  OX' .or. cspl(n) == '  NX' .or.
!     1      cspl(n) == '  N5' .or. cspl(n) == '  HX' .or.
!     2      cspl(n) == '  N7' .or. cspl(n) == '  T5' .or.
!     3      cspl(n) == '  MM' .or. cspl(n) == '  CO' .or.
!     4      cspl(n) == '  HA' .or. cspl(n) == '  MH' .or.
!     5      cspl(n) == '  HD' .or. cspl(n) == '  BF' .or.
!     6      cspl(n) == '  DB') then

          call nom (nam, cspl(n))
          do l = 1, npl
            call getfld2(nf3, xa(1,l), nc4to8("GRID"), -1,
     &                   nam, lp(l), ibuf, maxx, ok)
            if (.not.ok) call xit('ptsix',-3)
            write(6,6025) (ibuf(ind),ind=1,8)
          enddo

        else

! For inorganic Cl, use the fraction of inorganic-to-total,
! scaled to the new target, and put into HCl.

          IF (cspl(N).EQ.'  C9') THEN
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = 1.0E-16
              ENDDO
            ENDDO
          ENDIF
c
          IF (cspl(N).EQ.'  C7') THEN
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = FA(IL,L)*XTTLCL
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  C4') THEN
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = 1.0E-16
              ENDDO
            ENDDO
          ENDIF
C
C ---- individual halocarbons are scaled to the target
C      amount and have a correction factor on any error
C      in total chlorine from the initial conditions
C
          IF (cspl(N).EQ.'  F1') THEN
            CALL NOM(NAM,'  F1')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(1)/HALOIC(1)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  F2') THEN
            CALL NOM(NAM,'  F2')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(2)/HALOIC(2)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  F3') THEN
            CALL NOM(NAM,'  F3')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(3)/HALOIC(3)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  F4') THEN
            CALL NOM(NAM,'  F4')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(4)/HALOIC(4)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  F5') THEN
            CALL NOM(NAM,'  F5')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(5)/HALOIC(5)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  F6') THEN
            CALL NOM(NAM,'  F6')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(6)/HALOIC(6)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)*ZA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
C  ---- bromine is trickier because of the contribution from
C       VSLS so here a rough scaling is done taking into
C       account the contribution from CH3Br and an extra
C       5 pptv from the VSLS
C
          IF (cspl(N).EQ.'  BX') THEN
            CALL NOM(NAM,'  BX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  HB') THEN
            CALL NOM(NAM,'  HB')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  CB') THEN
            CALL NOM(NAM,'  CB')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(7)/HALOIC(7)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = PRX1*XA(IL,L)
              ENDDO
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  NY')THEN
c
c  --------  construct the NOy family
c
            CALL NOM(NAM,'  NX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
c
            CALL NOM(NAM,'  N5')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
            DO IL=1,ilen
              DO L=1,npl
                xa(il,l) = xa(il,l) + ya(il,l)
              ENDDO
            ENDDO
c
            CALL NOM(NAM,'  N7')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = xa(il,l) + ya(il,l)
              ENDDO
            ENDDO
          ENDIF
c ----
          IF (cspl(N).EQ.'  CY')THEN
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = FA(IL,L)*XTTLCL
              ENDDO
            ENDDO
          ENDIF
c ----
          IF (cspl(N).EQ.'  CZ')THEN
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = XTTLCL
              ENDDO
            ENDDO
          ENDIF
c ----
          IF (cspl(N).EQ.'  BZ')THEN
c
c  --------  construct the Brz family using the same scaling
c            used earlier for the individual Br species
c
            CALL NOM(NAM,'  BX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = prx1*xa(il,l)
              ENDDO
            ENDDO
c
            CALL NOM(NAM,'  HB')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = (HALOTG(7)+5.0E-12)/
     1              (HALOIC(7)+5.0E-12)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = xa(il,l) + prx1*ya(il,l)
              ENDDO
            ENDDO
C
            CALL NOM(NAM,'  CB')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(7)/HALOIC(7)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = xa(il,l) + prx1*ya(il,l)
              ENDDO
            ENDDO
C
            CALL NOM(NAM,'  BF')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = xa(il,l) + 3.0*ya(il,l)
              ENDDO
            ENDDO
C
            CALL NOM(NAM,'  DB')
            DO L=1,NPL
              CALL GETFLD2(NF3, YA(1,L),NC4TO8("GRID"),-1,
     1                    NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-32) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
C
            PRX1 = HALOTG(7)/HALOIC(7)
            DO L=1,npl
              DO IL=1,ilen
                xa(il,l) = xa(il,l) + 2.0*ya(il,l)
              ENDDO
            ENDDO

          ENDIF
c
          IF(cspl(N).EQ.'  TA') THEN
C
C --- initialize the hybrid tracers for troposheric NOx
C   --- constants for hybridization must agree with model values
            XREF1=1.5E-08
            XPOW1=1.0
            SMIN = XREF1/
     1             ((1. + XPOW1*LOG(XREF1/1.0E-14))**(1.0/XPOW1))
            DO L=1,NPL
              DO I=1,ILEN
                XA(I,L) = SMIN
              ENDDO
            ENDDO
          ENDIF

          IF(cspl(N).EQ.'  TB') THEN
C
C --- initialize the hybrid tracers for troposheric HNO3
C   --- constants for hybridization must agree with model values
            XREF2=5.0E-09
            XPOW2=1.0
            SMIN = XREF2/
     1             ((1. + XPOW2*LOG(XREF2/1.0E-14))**(1.0/XPOW2))
C
            DO L=1,NPL
              DO I=1,ILEN
                XA(I,L) = SMIN
              ENDDO
            ENDDO
          ENDIF
c ---
          IF (cspl(N).EQ.'  X1') THEN
c
c  --------  stratospheric ozone
c
            CALL NOM(NAM,'  OX')
            DO L=1,NPL
              CALL GETFLD2(NF3, XA(1,L),NC4TO8("GRID"),-1,
     1                     NAM,LP(L),IBUF,MAXX,OK)
              IF(.NOT.OK) CALL XIT('PTSIX',-3) 
              WRITE(6,6025) (IBUF(IND),IND=1,8)
            ENDDO
          ENDIF
C
          IF (cspl(N).EQ.'  X2') THEN
c
c  -------- e90 tracer
c
            DO L=1,npl
              DO IL=1,ilen
                XA(IL,L)= 1.00E-07
              ENDDO
            ENDDO
          ENDIF
c
          IF (cspl(N).EQ.'  X3') THEN
c
c  -------- SF6-like age of air tracer
c
            DO L=1,npl
              DO IL=1,ilen
                XA(IL,L)= 2.0E-12
              ENDDO
            ENDDO
          ENDIF
c
        ENDIF

C     * FROM DT/DP=(R*T)/(P*CP),  R/CP=2./7.,  AND T=280. 
C     * WE GET RLDN = DT/D(LN P) = 80.
C     * LTES = (0,1) TO PUT TEMP,ES ON (HALF,FULL) LEVELS.
  
        RLUP =  0.
        RLDN =  0.

        CALL PAELX  (XA,ILEN,SIG,NSL, XA,PRLOG,NPL,SP,RLUP,RLDN, 
     1              AH,BH,NPL+1,FPCOL,DFDLNP ,DLNP )
  
C     * CONVERT TO SPECTRAL AND SAVE ADV. SPECIES

        DO 280 K=1,NSL
           CALL GGAST2(SSTRAC,LSR,LM,LA,XA(1,K),ILG+1,ILAT,
     1                 1,SL,WL,ALP,EPSI,WRKS,WRKL)
           CALL NOM(NAM,cspl(N))
           CALL SETLAB(IBUF,NC4TO8("SPEC"),0,
     1                 NAM,LH(K),LA,1,LRLMT,2)

           CALL PUTFLD2(NFO,SSTRAC,IBUF,MAXX)
           WRITE(6,6026) (IBUF(IND),IND=1,8)
  280   CONTINUE

      enddo  ! end loop 300 over N=1,JPNTRCI

C-------------------------------------------------------------------- 
 6000 FORMAT(A4,'    ')
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
 6026 FORMAT(' ',60X,A4,I6,1X,A4,5I6) 
      END

! -----------------------------------------------------------------------------

      SUBROUTINE PAELX (FS, ILEN,SIG,NSL, FP,PRLOG,NPL, PSLOG,RLUP,RLDN, 
     1                 A,B,NPL1,FPC,DFDLNP,DLNP)
  
C     * FEB 02/88 - R.LAPRISE.
C     * INTERPOLATES MULTI-LEVEL SET OF GRIDS FROM PRESSURE LEVELS
C     * TO ETA LEVELS FOR HYBRID MODEL. 
C     * THE INTERPOLATION IS LINEAR IN LN(PRES).
C 
C     * ALL GRIDS HAVE THE SAME HORIZONTAL SIZE (LEN POINTS).
C     * FP    = INPUT  GRIDS ON PRESSURE LEVELS.
C     * FS    = OUTPUT GRIDS ON ETA      LEVELS.
C     *     (NOTE THAT FP AND FS MAY BE EQUIVALENCED IN CALLING PROGRAM)
C     * PSLOG = INPUT  GRID OF LN(SURFACE PRESSURE IN MB).
C 
C     * PRLOG(NPL) = VALUES OF INPUT PRESSURE LEVEL (MB). 
C     * SIG(NSL) = VALUES OF ETA LEVELS OF OUTPUT FIELD.
C     *            (BOTH MUST BE MONOTONIC AND INCREASING). 
C     * NPL1  = NPL+1.
C 
C     * RLUP  = LAPSE RATE USED TO EXTRAPOLATE ABOVE PRLOG(1).
C     * RLDN  = LAPSE RATE USED TO EXTRAPOLATE BELOW PRLOG(NSL).
C     *         UNITS OF RLUP,RLDN ARE  DF/D(LN PRES).
C 
      real :: FP   (ILEN,NPL),FS (ILEN,NSL),PSLOG (ILEN) 
      real :: PRLOG(   NPL)
  
      real :: A(NSL),B(NSL)
  
C     * WORK SPACE. 
  
      real :: FPC(NPL),DFDLNP(NPL1),DLNP(NPL),SIG(NSL) 
C---------------------------------------------------------------------

C     * PRECOMPUTE INVERSE OF DELTA LN PRES.
C 
      DO 180 L=1,NPL-1
         DLNP(L) =1. / (PRLOG(L+1)-PRLOG(L))
  180 CONTINUE
  
C     * LOOP OVER ALL HORIZONTAL POINTS.
  
      DO 500 I=1,ILEN 
  
C     * GET A COLUMN OF FP (ON PRESSURE LEVELS).
  
      DO 210 L=1,NPL
         FPC(L)=FP(I,L) 
  210 CONTINUE
  
C     * COMPUTE VERTICAL DERIVATIVE OVER ALL PRESSURE INTERVALS.
  
      DO 260 L=1,NPL-1
  260 DFDLNP(L+1  ) = (FPC(L+1)-FPC(L)) *DLNP(L)
      DFDLNP(1    ) = RLUP
      DFDLNP(NPL+1) = RLDN
  
C     * COMPUTE THE LOCAL SIGMA VALUES. 
  
      CALL NIVCAL (SIG, A,B,100.*EXP(PSLOG(I)),NSL,1,1) 

      do l=1,nsl
         SIG(L)=LOG(SIG(L))
      enddo
  
C     * LOOP OVER SIGMA LEVELS TO BE INTERPOLATED.
C     * X IS THE LN(PRES)=LN(PS)*LN(SIG) VALUE OF REQUIRED SIGMA LEVEL. 
  
      K=1 
      DO 350 N=1,NSL
      X=SIG(N)+PSLOG(I) 
  
C     * FIND WHICH PRESSURE INTERVAL WE ARE IN. 
  
      DO 310 L=K,NPL
      INTVL=L 
  310 IF(X.LT.PRLOG(L)) GO TO 320 
      INTVL=NPL+1 
  320 K=INTVL-1 
      IF(K.EQ.0) K=1
  
C     * NOW INTERPOLATE AT THIS POINT.
  
  350 FS(I,N)  = FPC(K)+DFDLNP(INTVL)*(X-PRLOG(K))
  
  500 CONTINUE
  
      RETURN
      END 

! -----------------------------------------------------------------------------
      subroutine nom (nam, cspl)

!     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.)
!     * Sous-routine qui définit le nom des champs associés aux différents
!     * traceurs en fonction du nombre de traceurs.
!     * Subroutine that defines field names corresponding to tracers, based
!     * on the number of tracers.
      character (len=8) :: nam
      character (len=4) :: cspl
!----------------------------------------------------------------------

      write (nam,1000) cspl
 1000 format(a4,'    ')

      return
      end subroutine nom
