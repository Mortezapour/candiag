      PROGRAM DELHAT
C     PROGRAM DELHAT (PSFIL,       DHFIL,       INPUT,       OUTPUT,    )       D2
C    1          TAPE1=PSFIL, TAPE2=DHFIL, TAPE5=INPUT, TAPE6=OUTPUT)
C     --------------------------------------------------------------            D2
C                                                                               D2
C     DEC 22/10 - D.PLUMMER (REVISED FOR BETTER HANDLING OF TIMESTEP AND LEVEL  D2
C                            LABELLING)                                         D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)              
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 06/83 - R.LAPRISE.                                                   
C     JUL 08/80 - J.D.HENDERSON.
C                                                                               D2
CDELHAT  - COMPUTES THE FUNCION DELTA-HAT FOR ALL PRESSURE LEVELS       1  1 C  D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES THE FUNCTION DELTA-HAT FOR A FILE OF SURFACE PRESSURE       D3
C          GRIDS, (ONE SET FOR EACH PRESSURE GRID).                             D3
C          PRESSURE VALUES ARE READ FROM A CARD.                                D3
C          DELTA-HAT IS 1. ABOVE THE SURFACE AND 0. BELOW IT.                   D3
C          MAXIMUM NUMBER OF LEVELS IS $PL$.                                    D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      PSFIL = INPUT SERIES OF SURFACE PRESSURE GRIDS (MB).                     D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      DHFIL = DELTA-HAT AT REQUESTED PRESSURE LEVELS FOR EACH                  D3
C              PRESSURE GRID IN PSFIL.                                          D3
C              LABELS CONTAIN  4HGRID, 0, 4HDELH, INT(PR(L)), ETC.              D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      NLEV = NUMBER OF PRESSURE LEVELS                                         D5
C      PR   = VALUE OF THE PRESSURE IN MILLIBARS                                D5
C                                                                               D5
CEXAMPLE OF INPUT CARDS...                                                      D5
C                                                                               D5
C*  DELHAT   10                                                                 D5
C* 50  150  250  350  450  550  650  750  850  950                              D5
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_PLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/PSBAR(SIZES_LONP1xLAT),DELTA(SIZES_LONP1xLAT) 
C 
      LOGICAL OK
      INTEGER LEV(SIZES_PLEV)
      REAL PR(SIZES_PLEV) 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_PLEV/
C-------------------------------------------------------------------- 
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE PRESSURE LEVELS FROM CARDS.
C 
      READ(5,5010,END=902) NLEV                                                 D4
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('DELHAT',-1) 
      READ(5,5012,END=903) (LEV(L),L=1,NLEV)                                    D4
      CALL LVDCODE(PR,LEV,NLEV)
C 
C     * GET THE NEXT SURFACE PRESSURE FIELD INTO PSBAR. 
C 
      NR=0
  120 CALL GETFLD2(1,PSBAR,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('DELHAT',-2) 
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('DELHAT',0)
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) THEN
        WRITE(6,6025) IBUF
        NPACK=MIN(2,IBUF(8))
      ENDIF
      NLG=IBUF(5) 
      NLAT=IBUF(6)
      NWDS=NLG*NLAT 
C 
C     * COMPUTE DELTA-HAT FOR EACH PRESSURE LEVEL AND SAVE ON FILE 2. 
C 
      DO 150 L=1,NLEV 
C 
      DO 130 I=1,NWDS 
      DELTA(I)=1.E0 
      IF(PSBAR(I).LT.PR(L)) DELTA(I)=0.E0 
  130 CONTINUE
C 
      IPR=INT(PR(L))
C     CALL SETLAB(IBUF,  NC4TO8("GRID"),0,NC4TO8("DELH"),IPR,
      CALL SETLAB(IBUF,NC4TO8("GRID"),-1,NC4TO8("DELH"),LEV(L),
     +                                      NLG,NLAT,0,NPACK)
      CALL PUTFLD2(2,DELTA,IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
  150 CONTINUE
      NR=NR+1 
      GO TO 120 
C 
C     * E.O.F. ON INPUT.
C 
  902 CALL                                         XIT('DELHAT',-3) 
  903 CALL                                         XIT('DELHAT',-4) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,I5)                                                            D4
 5012 FORMAT(16I5)                                                              D4
 6010 FORMAT( '0',I6,'  RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
