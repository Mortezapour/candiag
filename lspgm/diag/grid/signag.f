      PROGRAM SIGNAG
C     PROGRAM SIGNAG (GRID,       INPUT,       OUTPUT,                  )       D2
C    1          TAPE1=GRID, TAPE5=INPUT, TAPE6=OUTPUT)
C     ------------------------------------------------                          D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     DEC 06/83 - R.LAPRISE                                                     
C                                                                               D2
CSIGNAG  - PRODUCES A GRID OF (-1,0,1) FROM (-,0,+) INPUT CARDS         0  1 C GD1
C                                                                               D3
CAUTHOR - R.LAPRISE                                                             D3
C                                                                               D3
CPURPOSE - READ AN INPUT RECORD CONTAINING A TABLE OF "-", "0" OR "+" AND       D3
C          GENERATE A STANDART CCRN FILE WITH CORRESPONDING FLOATING POINTS     D3
C          VALUES.                                                              D3
C          NOTE - GRID SIZES ARE RESTRICTED TO 72X46 .                          D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GRID = 72X46 STANDART CCRN FILE IMAGE OF THE INPUT RECORD.               D3
C             CONTAINS -1.0, 0.0 OR 1.0 .                                       D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      IBUF = STANDART CCRN 8 WORD LABEL                                        D5
C      IG   = SIGN INFO READ AND LATER TRANSLATED BY PROGRAM                    D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* SIGNAG.   ... VERY SPECIFIC - SKIPPED ...                                    D5
C-----------------------------------------------------------------------------
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER*1 IG
      COMMON/BLANCK/ G(73,46), IG(73,46)
      COMMON /ICOM/ IBUF(8),IDAT(11000)
C 
      DATA ILG/72/, JLAT/46/, MAXX/11000/                                       
C-------------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
      ILG1=ILG+1
C 
C     READ-IN 8 WORD LABEL CARD.
C 
      READ(5,5001,END=901) IBUF                                                 D4
      WRITE(6,5001) IBUF
      IF(IBUF(5).NE.ILG1.OR.IBUF(6).NE.JLAT) CALL  XIT('SIGNAG',-1) 
C 
C     * READ-IN DATA (+,0,-). 
C 
      DO 100 J=1,JLAT                                                           D4
      READ(5,5002,END=902)(IG(I,J),I=1,ILG)                                     D4
      IG(ILG1,J)=IG(1,J)
  100 CONTINUE                                                                  D4
C 
C     * REVERSE LATITUDES AND CONVERT CODE TO FLOATING POINT. 
C 
      DO 200 J=1,JLAT 
      JJ=JLAT+1-J 
      DO 200 I=1,ILG1 
      IF(IG(I,J).EQ.'+') G(I,JJ)=+1.0E0 
      IF(IG(I,J).EQ.'0') G(I,JJ)= 0.0E0 
      IF(IG(I,J).EQ.'-') G(I,JJ)=-1.0E0 
  200 CONTINUE
C 
C     * WRITE-OUT FIELD.
C 
      CALL PUTFLD2(1,G,IBUF,MAXX)
      CALL                                         XIT('SIGNAG',0)
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('SIGNAG',-2) 
  902 CALL                                         XIT('SIGNAG',-3) 
C-------------------------------------------------------------------------
 5001 FORMAT(6X,A4,I10,6X,A4,5I10)                                              D4
 5002 FORMAT(7X,72A1)                                                           D4
      END
