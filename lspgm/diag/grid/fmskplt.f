      PROGRAM FMSKPLT
C     PROGRAM FMSKPLT (IN,      OUT,      MSK,      INPUT,      OUTPUT, )       D2
C    1           TAPE1=IN,TAPE2=OUT,TAPE3=MSK,TAPE5=INPUT,TAPE6=OUTPUT)
C     -----------------------------------------------------------------         D2
C                                                                               D2
C     Nov 17/10 - S.KHARIN (FIX A BUG IN SETTING "LOPN" VARIABLE AND REVISE     D2
C                           FORMAT STATEMENTS TO PRINT MORE SIGNIFICANT DIGITS) D2
C     FEB 02/09 - S.KHARIN (ADD OPTIONS FOR SPECIFYING SPVAL IN INPUT CARD,     
C                           KEEPING THE ORIGINAL VARIABLE NAMES, AND            
C                           AUTOMATICALLY REWINDING THE MASK AT EOF.            
C                           PROPAGATE SUPERLABELS FROM INPUT TO OUTPUT FILE.)   
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     APR 24/98 - F.MAJAESS (ADDED "MAXXR" DECLARATION AND USE)
C     FEB 18/93 - E. CHAN   (CHANGE SPVAL TO 1.E38)
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)
C     APR 17/90 - F.MAJAESS (CHANGE SPVAL VALUE TO 10.E500)
C     MAR 07/90 - F.MAJAESS
C                                                                               D2
CFMSKPLT - MAKES A MASKED FILE BY SUBSTITUTING A DEFAULT HIGH MASK              D1
C                         VALUE IN A GRID OR A SET OF GRIDS.            2  1 C  D1
C                                                                               D3
CAUTHOR  - F.MAJAESS                                                            D3
C                                                                               D3
CPURPOSE - THIS PROGRAM IS MAINLY USED  TO  SUBSTITUTE A HIGH MASK VALUE        D3
C          "SPVAL", (SEE DATA SECTION BELOW), IN FIELD(S) READ FROM FILE        D3
C          "IN". THE RESULTING DATASET WRITTEN  TO  FILE "OUT"  IS  THEN        D3
C          PASSED ON TO A PLOTTING PROGRAM WHICH  HAS THE CAPABILITY  OF        D3
C          SKIP PLOTTING POINT(S) WITH THE SAME HIGH MASK VALUE "SPVAL".        D3
C          THE SUBSTITUTION OF "SPVAL" VALUE FOR VALUE(S) READ  IN  FROM        D3
C          FILE "IN" DEPEND ON NOT PASSING A TEST WHICH USES  A  LOGICAL        D3
C          OPERATOR "LOP" AND A COMPARISON NUMBER "VALUE" WHICH ARE BOTH        D3
C          READ FROM A CARD. THE TEST ITSELF IS APPLIED ON THE DATA FROM        D3
C          FILE "MSK", IF  THE  LATER  IS  SPECIFIED,  OTHERWISE  IT  IS        D3
C          PERFORMED ON THE DATA FROM FILE "IN"; THAT IS:                       D3
C            FOR EACH NUMBER IN A FIELD READ IN FROM FILE "IN",                 D3
C              IF "MSK" FILE IS SPECIFIED THEN,                                 D3
C                   WHENEVER ("MSK" FIELD NUMBER.LOP.VALUE) IS TRUE,            D3
C                             THE "IN" FIELD VALUE IS LEFT UNCHANGED,           D3
C                   OTHERWISE THE "IN" FIELD VALUE IS CHANGED TO "SPVAL".       D3
C              OTHERWISE ("MSK" FILE IS NOT SPECIFIED)                          D3
C                   WHENEVER ("IN" FIELD VALUE.LOP.VALUE) IS TRUE,              D3
C                             THE "IN" FIELD VALUE IS LEFT UNCHANGED,           D3
C                   OTHERWISE THE "IN" FIELD VALUE IS CHANGED TO "SPVAL".       D3
C                                                                               D3
C          NOTE - THE FIELD FROM FILE "IN" CAN BE REAL OR COMPLEX;              D3
C                 COMPLEX FIELDS ARE TREATED AS A STRING OF REAL NUMBERS.       D3
C                 ALSO, THERE SHOULD BE ONE CORRESPONDING RECORD IN FILE        D3
C                 "MSK", (IF SPECIFIED), FOR EACH RECORD PROCESSED FROM         D3
C                 FILE "IN".                                                    D3
C                                                                               D3
CINPUT FILE(S)...                                                               D3
C                                                                               D3
C      IN  = FILE OF REAL OR COMPLEX FIELDS.                                    D3
C      MSK = (IF SPECIFIED), FILE ON WHOSE DATA THE TEST IS APPLIED.            D3
C            (RECORDS SHOULD BE OF THE SAME KIND AS THOSE PROCESSED FROM        D3
C             FILE "IN").                                                       D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      OUT = MASKED FIELD(S) COMPUTED FROM THE FIELD(S) IN FILE "IN".           D3
C
CINPUT PARAMETERS...
C                                                                               D5
C11-20 NT      = STEP OF FIELD TO BE SELECTED FROM "IN".                        D5
C21-25 NAME    = NAME OF FIELD TO BE SELECTED FROM "IN".                        D5
C26-30 LOP     = 2 CHARACTER LOGICAL OPERATOR (EQ,NE,LT,LE,GT,GE).              D5
C31-40 VALUE   = COMPARISON VALUE FOR LOP.                                      D5
C41-45 LSPV    = 1, USE SPVAL FROM INPUT CARD.                                  D5
C                   OTHERWISE, USE SPVAL=1.E+38 (DEFAULT).                      D5
C46-55 SPVAL   = SPECIAL VALUE (IGNORED WHEN LSPV != 1)                         D5
C56-60 LNAME   = 1, KEEP THE VARIABLE NAMES.                                    D5
C                   OTHERWISE USE 4HFMSK (DEFAULT).                             D5
C61-65 LRWND   = 1, REWIND MASK WHEN EOF IS REACHED.                            D5
C                   OTHERWISE, ABORT (DEFAULT).                                 D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* FMSKPLT        36   TS   GT      273.                                        D5
C                                                                               D5
C FOR THE ABOVE EXAMPLE, (PROVIDED MSK FILE IS NOT SPECIFIED), THE VALUES       D5
C ARE LEFT UNCHANGED WHENEVER THE VALUE IN THE FIELD TS IS GREATER THAN 273,    D5
C AND SET TO "1.E38" OTHERWISE.                                                 D5
C                                                                               D5
C* FMSKPLT        -1 NEXT   GT       2.2    1        0.    1                    D5
C (USE 0. INSTEAD OF 1.E+38 AND KEEP THE ORIGINAL NAMES)                        D5
C                                                                               D5
C* FMSKPLT        -1 NEXT   NE    1.E+38    1        0.    1                    D5
C (REPLACE 1.E+38 WITH 0. AND KEEP THE ORIGINAL NAMES)                          D5
C                                                                               D5
C* FMSKPLT        -1 NEXT   GT        0.                                        D5
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/VAL(SIZES_BLONP1xBLAT),FMSK(SIZES_BLONP1xBLAT)

      LOGICAL OK, SPEC, MSKTST
      CHARACTER*2 LOP,LOPN
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/,MAXXR/SIZES_BLONP1xBLAT/
      DATA SPVAL/1.E38/
C---------------------------------------------------------------------
      NFF=5
      CALL JCLPNT(NFF,1,2,3,5,6)
C
C     * CHECK IF MSK FILE IS SPECIFIED AND SET LOGICAL SWITCH ACCORDINGLY.
C
      IF(NFF.GT.4)THEN
        MSKTST=.TRUE.
      ELSE
        MSKTST=.FALSE.
      ENDIF
C
      REWIND 1
      REWIND 2
      IF (MSKTST) REWIND 3
C
C     * READ NEEDED INFORMATION FROM CARD.
C     * NT,NAME IDENTIFYING THE FIELD TO BE READ.
C     * LOP IS A TWO CHARACTER LOGICAL OPERATOR (EQ,NE,LT,GT,LE,GE).
C     * VALUE IS THE FIELD COMPARISON NUMBER.
C
      READ(5,5010,END=900) NT,NAME,LOP,VALUE,LSPV,SPVAL,LNAME,LRWND             D4
      IF (LSPV.NE.1)SPVAL=1.E+38
      WRITE(6,6008) NT,NAME,LNAME,LRWND
      IF(LOP.EQ.'EQ')THEN
        LOPN='NE'
      ELSE IF(LOP.EQ.'NE')THEN
        LOPN='EQ'
      ELSE IF(LOP.EQ.'GT')THEN
        LOPN='LE'
      ELSE IF(LOP.EQ.'GE')THEN
        LOPN='LT'
      ELSE IF(LOP.EQ.'LT')THEN
        LOPN='GE'
      ELSE IF(LOP.EQ.'LE')THEN
        LOPN='GT'
      ENDIF
      IF(MSKTST) THEN
        WRITE(6,6010) SPVAL,LOPN,VALUE
      ELSE
        WRITE(6,6020) LOPN,VALUE,SPVAL
      ENDIF
C
C     * INITIALIZE FMSK ARRAY IF FILE "MSK" IS NOT SPECIFIED.
C
      IF (.NOT.MSKTST) THEN
        DO 50 I=1,MAXXR
          FMSK(I)=SPVAL
   50   CONTINUE
      ENDIF
C
C     * GET THE REQUESTED FIELD FROM FILE "IN".
C
      NR=0
  100 CALL GETFLD2(1,VAL,-1,NT,NAME,-1,IBUF,MAXX,OK)
      IF (.NOT.OK)  THEN
          IF (NR.EQ.0)  CALL                       XIT('FMSKPLT',-1)
          WRITE(6,6030) NR
          CALL                                     XIT('FMSKPLT',0)
      ENDIF
      IF(NR.EQ.0) CALL PRTLAB(IBUF)
C
C     * GET THE CORRESPONDING FIELD FROM FILE "MSK" (IF SPECIFIED).
C
      IF (MSKTST) THEN
 110    CALL GETFLD2(3,FMSK,-1,NT,NAME,-1,JBUF,MAXX,OK)
        IF (.NOT.OK) THEN
          IF (LRWND.NE.1.OR.(LRWND.EQ.1.AND.NR.EQ.0))
     1         CALL                                XIT('FMSKPLT',-2)
          REWIND 3
          GOTO 110
        ENDIF
        IF(NR.EQ.0) CALL PRTLAB(JBUF)
C
C       * MAKE SURE THAT THE FIELDS ARE OF THE SAME KIND AND SIZE.
C
        CALL CMPLBL(0,IBUF,0,JBUF,OK)
        IF (.NOT.OK)  THEN
          CALL PRTLAB(IBUF)
          CALL PRTLAB(JBUF)
          CALL                                     XIT('FMSKPLT',-3)
        ENDIF
      ENDIF
C
      SPEC=(IBUF(1).EQ.NC4TO8("SPEC").OR.IBUF(1).EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
C
C     * PROCESS SUPERLABELS
C
      IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR"))THEN
        CALL PUTFLD2(2,VAL,IBUF,MAXX)
        GOTO 100
      ENDIF

C     * PREPARE THE RESULTING (MASKED) FIELD ACCORDING TO VALUE.

      IF(LOP.NE.'LT') GO TO 211
      DO 210 I=1,NWDS
          VAL(I) = MERGE(MERGE(VAL(I),SPVAL,FMSK(I).LT.VALUE),
     1                   MERGE(VAL(I),SPVAL,VAL (I).LT.VALUE),
     2                   MSKTST)
  210 CONTINUE
      GO TO 240
  211 IF(LOP.NE.'GT') GO TO 213
      DO 212 I=1,NWDS
          VAL(I) = MERGE(MERGE(VAL(I),SPVAL,FMSK(I).GT.VALUE),
     1                   MERGE(VAL(I),SPVAL,VAL (I).GT.VALUE),
     2                   MSKTST)
  212 CONTINUE
      GO TO 240
  213 IF(LOP.NE.'GE') GO TO 215
      DO 214 I=1,NWDS
          VAL(I) = MERGE(MERGE(VAL(I),SPVAL,FMSK(I).GE.VALUE),
     1                   MERGE(VAL(I),SPVAL,VAL (I).GE.VALUE),
     2                   MSKTST)
  214 CONTINUE
      GO TO 240
  215 IF(LOP.NE.'LE') GO TO 217
      DO 216 I=1,NWDS
          VAL(I) = MERGE(MERGE(VAL(I),SPVAL,FMSK(I).LE.VALUE),
     1                   MERGE(VAL(I),SPVAL,VAL (I).LE.VALUE),
     2                   MSKTST)
  216 CONTINUE
      GO TO 240
  217 IF(LOP.NE.'EQ') GO TO 219
      DO 218 I=1,NWDS
          VAL(I) = MERGE(MERGE(VAL(I),SPVAL,FMSK(I).EQ.VALUE),
     1                   MERGE(VAL(I),SPVAL,VAL (I).EQ.VALUE),
     2                   MSKTST)
  218 CONTINUE
      GO TO 240
  219 IF(LOP.NE.'NE') GO TO 230
      DO 220 I=1,NWDS
          VAL(I) = MERGE(MERGE(VAL(I),SPVAL,FMSK(I).NE.VALUE),
     1                   MERGE(VAL(I),SPVAL,VAL (I).NE.VALUE),
     2                   MSKTST)
  220 CONTINUE
      GO TO 240
  230 CALL                                         XIT('FMSKPLT',-4)

C     * SAVE THE RESULTING (MASKED) FIELD ON FILE "OUT".

 240  IF(IBUF(3).NE.NC4TO8("SUBA").AND.LNAME.NE.1)
     1     IBUF(3)=NC4TO8("FMSK")
      IBUF(8)=1
      CALL PUTFLD2(2,VAL,IBUF,MAXX)
      IF(NR.EQ.0) CALL PRTLAB(IBUF)
      NR=NR+1
      GO TO 100

C     * E.O.F. ON INPUT.

  900 CALL                                         XIT('FMSKPLT',-5)
C---------------------------------------------------------------------
 5010 FORMAT(10X,I10,1X,A4,3X,A2,1E10.0,I5,1E10.0,2I5)                          D4
 6008 FORMAT('0 FIELD ',I10,2X,A4,' LNAME=',I2,' LRWND=',I2)
 6010 FORMAT('0 SET VALUE TO ',G12.5,' IF MASK .',A2,'.',G12.5/)
 6020 FORMAT('0 SET VALUE .',A2,'.',G12.5,' TO ',G12.5/)
 6030 FORMAT('0 PROGRAM READ ',I5,' FIELDS.')
      END
