      PROGRAM MURGE
C     PROGRAM MURGE (INA,       INB,       MERG,       OUTPUT,          )       B2
C    1         TAPE1=INA, TAPE2=INB, TAPE3=MERG, TAPE6=OUTPUT)
C     --------------------------------------------------------                  B2
C                                                                               B2
C     DEC 22/08 - F.MAJAESS (FIX THE CONDITIONAL MERGING LOGIC)                 B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     AUG 28/00 - F.MAJAESS (FIX A DISCOVERED BUG IN HANDLING TIMESTEP MATCH BY
C                            TAKING INTO ACCOUNT THE TIMESTEP ON THE LAST
C                            WRITTEN RECORD)
C     JUL 19/00 - F.MAJAESS (EXTEND THE FUNCTIONALITY TO INCLUDE CHECKING FIELD
C                            NAME, AS WELL AS THE TIMESTEP AND LEVEL VALUES)
C     JUN 20/00 - F.MAJAESS (ENSURE CHECKING FOR THE LEVEL VALUE WHILE MERGING)
C     MAY 05/00 - F.MAJAESS (CORRECT FOR DUPLICATE RECORDS)
C     SEP 29/97 - F.MAJAESS (CHANGE THE NAME FROM "MERGE" INTO "MURGE")
C     JAN 14/93 - E. CHAN (END READ USING FBUFFIN IF K.GE.0)
C     JAN 27/92 - E. CHAN (REPLACE BUFFERED I/O AND CONVERT HOLLERITH
C                          LITERALS TO ASCII)
C     MAR 10/81 - J.D.HENDERSON
C
CMURGE   - MERGES TWO FILES ORDERED BY TIMESTEP NUMBER, NAME AND LEVEL  2  1    B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - MERGES TWO FILES "INA" AND "INB", ORDERED BY TIMESTEP, FIELD NAME    B3
C          AND LEVEL, INTO ONE FILE "MERG".                                     B3
C          NOTE - BOTH INPUT FILES SHOULD BE ORDERED BY TIMESTEP NUMBER, HAVE   B3
C                 SAME FIELD NAME ORDER, AND WITHIN THAT ORDERED BY LEVEL       B3
C                 VALUES.                                                       B3
C                 THE STRUCTURE OF "MERG" OUTPUT FILE WILL FOLLOW THAT OF "INB" B3
C                 IF THE LATTER IS NOT EMPTY.                                   B3
C                 TIES, WHERE THE TIMESTEPS, FIELD NAMES AND LEVEL VALUES ARE   B3
C                 ALL THE SAME, THESE ARE RESOLVED IN FAVOR OF THE RECORD IN    B3
C                 THE FIRST FILE "INA". IN SUCH CASES, THE "INA" RECORD IS      B3
C                 WRITTEN INTO "MERG", AND "INB" RECORD GET SKIPPED.            B3
C                                                                               B3
CINPUT FILES...                                                                 B3
C                                                                               B3
C      INA  = FIRST  INPUT FILE, (ORDERED BY TIMESTEP NUMBER,NAME AND LEVELS)   B3
C      INB  = SECOND INPUT FILE, (ORDERED BY TIMESTEP NUMBER,NAME AND LEVELS)   B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      MERG = FILE OF INA AND INB MERGED BY TIMESTEP NUMBER.                    B3
C--------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: LA = 10+SIZES_BLONP1xBLATxNWORDIO

      LOGICAL EOFA,EOFB,READA,READB
      INTEGER PSTEP,PNAME
C
      COMMON/ICOM/IBUF(LA)
      COMMON/JCOM/JBUF(LA)
C
C--------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
      EOFA=.FALSE.
      EOFB=.FALSE.
      READA=.TRUE.
      READB=.TRUE.
      NRA=0
      NRB=0
      NRRA=0
      NRRB=0
      PNAME=NC4TO8("+XYZ")
      PSTEP=-1
C
C     * READ THE NEXT RECORD FROM FILE 1 AND/OR FILE 2.
C
  150 IF((EOFA).OR.(.NOT.READA)) GO TO 160
      CALL FBUFFIN(1,IBUF,LA,K,LENA)
      IF (K .GE. 0) THEN
        EOFA=.TRUE.
      ELSE
        EOFA=.FALSE.
        NRRA=NRRA+1
      ENDIF
      READA=.FALSE.
      IF (NRRA.EQ.1) WRITE (6,6025) (IBUF(I),I=1,8)
C
  160 IF((EOFB).OR.(.NOT.READB)) GO TO 170
      CALL FBUFFIN(2,JBUF,LA,K,LENB)
      IF (K .GE. 0) THEN
        EOFB=.TRUE.
      ELSE
        EOFB=.FALSE.
        NRRB=NRRB+1
      ENDIF
      READB=.FALSE.
      IF (NRRB.EQ.1) WRITE (6,6025) (JBUF(I),I=1,8)
C
C     * STOP HERE IF BOTH FILES ARE EMPTY.
C
  170 NRMERG=NRA+NRB
      IF(EOFA.AND.EOFB)THEN
        WRITE(6,6010) NRRA,NRRB,NRA,NRB,NRMERG
        CALL                                       XIT('MURGE',0)
      ENDIF
C
C     * THE DECISION TO COPY WHICH RECORD INTO "MERG" FILE, IS
C     * RESOLVED ACCORDING TO THE FOLLOWING RULES:
C     *
C     *  1- FROM THE OTHER INPUT FILE IF END_OF_FILE IS REACHED
C     *     ON ONE OF THE INPUT FILES.
C     *  2- THE ONE WITH LOWER TIMESTEP NUMBER,
C     *  3- THE ONE WITH THE SAME NAME AS THE PREVIOUS RECORD
C     *     WRITTEN, WITH "INA" RECORD FAVOURED, IF THE TIMESTEP
C     *     NUMBERS IN BOTH "INA" AND "INB" RECORDS ARE THE SAME,
C     *  4- THE ONE WITH THE LOWER LEVEL VALUE, IF BOTH THE
C     *     TIMESTEP NUMBERS AND FIELD NAMES ARE THE SAME, IN
C     *     BOTH "INA" AND "INB" RECORDS.
C
      IF (EOFA) THEN
       IF ((JBUF(2).GT.IBUF(2)).OR.
     1     ((JBUF(2).EQ.IBUF(2)).AND.(JBUF(3).NE.IBUF(3))).OR.
     2     ((JBUF(2).EQ.IBUF(2)).AND.(JBUF(3).EQ.IBUF(3))
     3                          .AND.(JBUF(4).GT.IBUF(4)))) THEN
        CALL FBUFOUT(3,JBUF,LENB,K)
        PSTEP=JBUF(2)
        PNAME=JBUF(3)
        NRB=NRB+1
       ENDIF
       READB=.TRUE.
      ELSE
       IF (EOFB) THEN
        IF ((IBUF(2).GT.JBUF(2)).OR.
     1      ((IBUF(2).EQ.JBUF(2)).AND.(IBUF(3).NE.JBUF(3))).OR.
     2      ((IBUF(2).EQ.JBUF(2)).AND.(IBUF(3).EQ.JBUF(3))
     3                           .AND.(IBUF(4).GT.JBUF(4)))) THEN
         CALL FBUFOUT(3,IBUF,LENA,K)
         PSTEP=IBUF(2)
         PNAME=IBUF(3)
         NRA=NRA+1
        ENDIF
        READA=.TRUE.
       ELSE

        IF ((IBUF(2).LT.JBUF(2) ).OR. 
     1      ((IBUF(2).EQ.JBUF(2)).AND.(IBUF(3).NE.JBUF(3)).AND.
     2       (IBUF(2).EQ.PSTEP  ).AND.(IBUF(3).EQ.PNAME  ).AND.
     3                                (JBUF(3).NE.PNAME  )).OR.
     4      ((IBUF(2).EQ.JBUF(2)).AND.(IBUF(3).EQ.JBUF(3)).AND.
     5                                (IBUF(4).LE.JBUF(4)))) THEN
         CALL FBUFOUT(3,IBUF,LENA,K)
         PSTEP=IBUF(2)
         PNAME=IBUF(3)
         NRA=NRA+1
         READA=.TRUE.
         IF ((IBUF(2).EQ.JBUF(2)).AND.
     1       (IBUF(3).EQ.JBUF(3)).AND.
     2       (IBUF(4).EQ.JBUF(4))) READB=.TRUE.
        ELSE
         CALL FBUFOUT(3,JBUF,LENB,K)
         PSTEP=JBUF(2)
         PNAME=JBUF(3)
         NRB=NRB+1
         READB=.TRUE.
        ENDIF
       ENDIF
      ENDIF
      GO TO 150
C--------------------------------------------------------------------
 6010 FORMAT('0RECORDS READ =',2I6,', MERGED FROM EACH =',2I6,
     1       ', FOR A MERGED TOTAL OF =',I6)
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
