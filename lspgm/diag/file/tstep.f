      PROGRAM  TSTEP
C     PROGRAM  TSTEP (XIN,       XOUT,       INPUT,       OUTPUT,       )       B2
C    1          TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     -----------------------------------------------------------               B2
C                                                                               B2
C     MAY 20/09 - S.KHARIN (USE DOUBLE PRECISION FOR SOME OPERATIONS.)          B3
C     MAR 18/09 - S.KHARIN (ADD YYYY AND YY INPUT FORMATS.                      
C                           ADD OPTION FOR FIXING Y2K BUG.)                     
C     DEC 15/04 - S.KHARIN (DO NOT ABORT WHEN CENTURY CHANGES WHILE CONVERTING  
C                           TO A 2-DIGIT YEAR FORMAT BUT CONTINUE WITH 3-DIGIT  
C                           YEAR FORMAT. HOWEVER ABORT WHEN MILLENIUM CHANGES.) 
C     NOV 02/04 - S.KHARIN (ADD INPUT FORMAT 'ACCMODEL' FOR RECORDS WITH
C                           ACCUMULATED QUANTITIES.
C                           ADD OUTPUT FORMATS 'YYYY' AND 'MODEL'.
C                           RELAX RESTRICTIONS ON THE MAXIMUM TIME STEP.
C                           LEAVE MODEL TIMESTEPS=0 UNCHANGED.)
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     MAR 05/02 - F.MAJAESS (REVISED TO HANDLE "CHAR" KIND RECORDS)
C     OCT.23/97 - D.LIU (REWRITTEN TO SUPPORT MANY FORMATS)
C     MAR 01/93 - S.LAMBERT, E.CHAN
C                                                                               B2
CTSTEP   - RELABELS TIMESTEP IBUF(2) IN VARIOUS FORMATS                 1  1 C  B1
C                                                                               B3
CAUTHORS - D.LIU,S.LAMBERT, E.CHAN                                              B3
C                                                                               B3
CPURPOSE - CHANGE THE TIMESTEP IN IBUF(2) FROM ONE OF THE FORMATS (FORMIN):     B3
C                 YYYYMMDDHH                                                    B3
C                   YYYYMMDD                                                    B3
C                     YYYYMM                                                    B3
C                       YYYY                                                    B3
C                   YYMMDDHH                                                    B3
C                     YYMMDD                                                    B3
C                       YYMM                                                    B3
C                         YY                                                    B3
C                      MODEL                                                    B3
C                   ACCMODEL                                                    B3
C                                                                               B3
C          TO ONE OF THE FOLLOWING FORMATS (FORMOUT):                           B3
C                 YYYYMMDDHH                                                    B3
C                   YYYYMMDD                                                    B3
C                     YYYYMM                                                    B3
C                       YYYY                                                    B3
C                   YYMMDDHH                                                    B3
C                     YYMMDD                                                    B3
C                       YYMM                                                    B3
C                      MODEL                                                    B3
C                  FIXY2KBUG                                                    B3
C                                                                               B3
C          INPUT FORMAT ACCMODEL MUST BE USED TO CONVERT MODEL TIME STEPS       B3
C          IN INPUT FILES WITH ACCUMULATED QUANTITIES TO OTHER FORMATS          B3
C          IN ORDER TO GET THE CORRECT DAY FOR ACCUMULATION PERIODS ENDING      B3
C          AT MIDNIGHT.                                                         B3
C                                                                               B3
C          IF FORMIN=MODEL/ACCMODEL, TIMESTEP=0 THAT IS NORMALLY USED FOR       B3
C          TIME INVARIANT FIELDS REMAINS UNCHANGED.                             B3
C                                                                               B3
C          THE OUTPUT FORMAT MODEL WORKS ONLY WITH INPUT FORMAT YYYYMMDDHH.     B3
C                                                                               B3
C          THE YEAR IN THE TIMESTEP MAY BE OFFSET BY THE USER.                  B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      XIN     =  INPUT FILE. "LABL" AND/OR "CHAR" KIND RECORDS AND RECORDS     B3
C                 WITH THE ZERO MODEL TIME STEPS, IF ANY, ARE WRITTEN TO THE    B3
C                 OUTPUT FILE UNCHANGED.                                        B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      XOUT    =  COPY OF XIN WITH THE TIMESTEP CHANGED                         B3
C
CINPUT PARAMETERS...
C                                                                               B5
C      MSTEP  = MODEL TIMESTEP IN MINUTES. (REQUIRED FOR                        B5
C               MODEL TIMESTEP CONVERSION ONLY)                                 B5
C      MOFFSET                                                                  B5
C             1) WHEN FORMIN=MODEL OR FORMIN=ACCMODEL                           B5
C                MOFFSET IS THE STARTING DATE AND TIME OF THE                   B5
C                MODEL RUN IN YYYYMMDDHH FORMAT                                 B5
C                (JAN 1 00Z OF YEAR 1 IS ASSUMED IF MOFFSET=0)                  B5
C             2) WHEN FORMIN=FORMATS OTHER THAN MODEL OR ACCMODEL               B5
C                MOFFSET IS THE OFFSET TO BE APPLIED TO THE YEAR                B5
C                IN THE INPUT TIMESTEP                                          B5
C                                                                               B5
C      FORMIN = TIMESTEP FORMAT OF INPUT DATA                                   B5
C                                                                               B5
C      FORMOUT= TIMESTEP FORMAT OF OUTPUT DATA                                  B5
C                                                                               B5
CNOTE- THIS PROGRAM TAKES TWO STEPS TO CONVERT A TIME STAMP. FIRST,             B5
C      INPUT TIMESTEP IS ALWAYS CONVERTED INTO THE YYYYMMDDHH FORMAT.           B5
C      WHEN THE INPUT FORMAT IS SHORTER THAN YYYYMMDDHH (THIS DOES              B5
C      NOT APPLY TO FORMIN=MODEL/ACCMODEL), THE PROGRAM WILL FILL IN ZEROS      B5
C      IN PLACE OF MISSING DIGITS. THEN THIS CONVERTED TIMESTEP                 B5
C      IS WRITTEN OUT ACCORDING TO THE OUTPUT FORMAT SPECIFIED.                 B5
C                                                                               B5
C      WHEN A YEAR YYYY IS CONVERTED INTO YY, WHERE YY IS THE LAST              B5
C      TWO DIGITS OF YYYY, AMBIGUITY MAY RISE WHEN YYYY SPANS OVER              B5
C      CENTURIES. THUS WHEN THIS IS THE CASE AND THE OUTPUT FORMAT              B5
C      IS ONE OF YYMMDDHH, YYMMDD, AND YYMM, THE PROGRAM ABORTS.                B5
C                                                                               B5
C      DUE TO THE 32-BIT INTEGER LIMITATION ON SOME HOST MACHINES,              B5
C      THE LARGEST YEAR IN YYYYMMDDHH FORMAT MUST NOT EXCEED YEAR 2147          B5
C      (2**31-1=2147483647) . OTHERWISE THE PROGRAM WILL ABORT.                 B5
C                                                                               B5
C      MODEL TIMESTEP CONVERSION IS BASED ON THE PREVIOUS VERSION               B5
C      OF PROGRAM TSTEP. THE INPUT PARAMETERS ARE ARRANGED IN SUCH A WAY        B5
C      THAT THE NEW PROGRAM IS COMPATIBLE WITH THE OLD VERSION: WHEN            B5
C      FORMIN IS NOT SPECIFIED IN THE INPUT CARD, IT WILL BE GIVEN THE          B5
C      VALUE OF 'MODEL'. IF FORMOUT IS NOT SPECIFIED, IT WILL BE YYYYMMDDHH.    B5
C                                                                               B5
C      IF FORMOUT=FIXY2KBUG THEN THE FORMIN IS USED FOR OUTPUT DATA BUT         B5
C      THE YEAR IS MADE MONOTONICALLY INCREASING BY ADDING 100 (2000) TO YEARS  B5
C      OF ALL RECORDS THAT FOLLOW YEAR 99 (1999).                               B5
C                                                                               B5
CEXAMPLES OF INPUT CARD...                                                      B5
C                                                                               B5
C*TSTEP      20       79070112                                                  B5
C      CONVERT MODEL TIMESTEPS WITH A 20-MINUTE INTERVAL TO YYYYMMDDHH          B5
C      FORMAT BEGINNING AT 79070112 (YYYYMMDDHH).                               B5
C                                                                               B5
C*TSTEP      20       79070112       ACCMODEL                                   B5
C      CONVERT MODEL TIMESTEPS OF ACCUMULATED QUANTITIES WITH A 20-MINUTE       B5
C      INTERVAL TO YYYYMMDDHH FORMAT BEGINNING AT 79070112 (YYYYMMDDHH).        B5
C                                                                               B5
C*TSTEP      20     1980010100          MODEL         YYYYMM                    B5
C      CONVERT MODEL TIMESTEPS WITH A 20-MINUTE INTERVAL TO YYYYMM              B5
C      FORMAT BEGINNING AT 1980010100 (YYYYMMDDHH).                             B5
C                                                                               B5
C*TSTEP                     -5           YYMM           YYMM                    B5
C      REPLACE YY BY THE VALUE OF YY-5.                                         B5
C                                                                               B5
C*TSTEP                   1900       YYMMDDHH         YYYYMM                    B5
C      ADD 1900 TO YY IN TIMESTEPS AND CONVERT THEM TO YYYYMM FORMAT.           B5
C                                                                               B5
C*TSTEP                   1900           YYYY           YYYY                    B5
C      ADD 1900 TO YYYY IN TIMESTEPS.                                           B5
C                                                                               B5
C*TSTEP                            YYYYMMDDHH         YYYYMM                    B5
C      CONVERT TIMESTEPS OF YYYYMMDDHH FORMAT TO YYYYMM.                        B5
C                                                                               B5
C*TSTEP                                YYYYMM           YYYY                    B5
C      CONVERT TIMESTEPS OF YYYYMM FORMAT TO YYYY.                              B5
C                                                                               B5
C*TSTEP      20       01010100     YYYYMMDDHH          MODEL                    B5
C      CONVERT TIMESTEPS OF YYYYMMDDHH FORMAT TO MODEL TIME STEPS.              B5
C                                                                               B5
C*TSTEP                   1900       YYMMDDHH      FIXY2KBUG                    B5
C      ADD 1900 TO YY IN TIMESTEPS AND MAKE YEARS MONOTONICALLY INCREASING.     B5
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION MINMON(12),MONTH(12),MONTHD(12)
      CHARACTER*10 FORMIN, FORMOUT
      CHARACTER*30 PFORM
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/

C     * DECLARE THE FOLLOWING VARIABLES AS REAL NUMBERS
C     * TO AVOID 32-BIT INTEGER OVER-FLOWING AND TO ALLOW
C     * NON-INTEGER TIME STEP

      REAL*8 MSTEP,MOFFSET,MINS,MINST,ITIME,LSHIFT,IDIV,SMALL,R8ONE

C     * MINYR=NUMBER OF MINUTES IN ONE MODEL YEAR
      DATA MINYR /525600/

C     * MONTH LENGTHS
      DATA MONTH /31,28,31,30,31,30,31,31,30,31,30,31/

C     * JULIAN DAY OFFSETS
      DATA MONTHD/0,31,59,90,120,151,181,212,243,273,304,334/

C     * LIMITD IS THE LARGEST 32-BIT DATE TIME STAMP IN YYYYMMDDHH
C     * THE PROGRAM IS DESIGNED TO HANDLE.

      DATA LIMITD/2147123124/,SMALL/1.D-12/,R8ONE/1.0/

C---------------------------------------------------------------------
      NF=4
      CALL JCLPNT(NF,1,2,5,6)
      REWIND 1
      REWIND 2

      READ(5,5010) MSTEP,MOFFSET,FORMIN,FORMOUT                                 B4
      MOFFSET0=MOFFSET

C     * CHECK RANGE OF INPUT PARAMETERS. PRINT THEM OUT.

      IF (FORMIN.EQ.'          ') THEN
C        * DEFAULT INPUT DATA TYPE
         FORMIN='     MODEL'
      ENDIF
      IF (FORMOUT.EQ.'          ') THEN
C        * DEFAULT OUTPUT DATA TYPE
         FORMOUT='YYYYMMDDHH'
      ENDIF
      WRITE(6,6005) FORMIN,FORMOUT

      IF (FORMIN.EQ.'     MODEL'.OR. FORMIN.EQ.'  ACCMODEL'.OR.
     1   FORMOUT.EQ.'     MODEL') THEN
C        * DEFAULT OFFSET FOR MODEL DATA
         IF (MOFFSET.EQ.0.E0) MOFFSET=1010100.E0
         WRITE(6,6010) MSTEP,MOFFSET
         IF (MSTEP.LE.0.D0) THEN
            WRITE(6,6015)
            CALL                                   XIT('TSTEP',-1)
         ENDIF
      ELSEIF (MOFFSET.NE.0) THEN
         WRITE(6,6030) MOFFSET
      ENDIF
      IF ((FORMOUT.EQ.'     MODEL'.AND. FORMIN.NE.'YYYYMMDDHH').OR.
     1    (FORMOUT.EQ.' FIXY2KBUG'.AND.
     2     (FORMIN.EQ.'     MODEL'.OR.FORMIN.EQ.'  ACCMODEL')))
     3     CALL                                    XIT('TSTEP',-2)

C     * SET VARIABLE LSHIFT ACCORDING TO INPUT FORMAT. VARIABLE
C     * LSHIFT IS USED TO LEFT SHIFT A TIME LABEL TO THE YYYYMMDDHH
C     * FORMAT. CONVERT INITIAL TIME (MOFFSET) TO MINUTES IF FORMIN
C     * IS MODEL.

      IF (FORMIN.EQ.'YYYYMMDDHH'.AND.
     1   FORMOUT.NE.'     MODEL')     THEN
         LSHIFT   = 1.D0
      ELSEIF (FORMIN.EQ.'  YYYYMMDD') THEN
         LSHIFT   = 1.D2
      ELSEIF (FORMIN.EQ.'    YYYYMM') THEN
         LSHIFT   = 1.D4
      ELSEIF (FORMIN.EQ.'      YYYY') THEN
         LSHIFT   = 1.D6
      ELSEIF (FORMIN.EQ.'  YYMMDDHH') THEN
         LSHIFT   = 1.D0
      ELSEIF (FORMIN.EQ.'    YYMMDD') THEN
         LSHIFT   = 1.D2
      ELSEIF (FORMIN.EQ.'      YYMM') THEN
         LSHIFT   = 1.D4
      ELSEIF (FORMIN.EQ.'        YY') THEN
         LSHIFT   = 1.D6
      ELSEIF (FORMIN.EQ.'     MODEL' .OR.
     1        FORMIN.EQ.'  ACCMODEL' .OR.
     2       FORMOUT.EQ.'     MODEL') THEN

C      * CONVERT JULIAN DAYS TO MINUTES
         DO I=1,12
            MINMON(I)=MONTHD(I)*1440
         ENDDO

C     * FOR FORMIN=MODEL, MAKE SURE MOFFSET IS LEGITIMATE DATE TIME.

         IYR=INT(MOFFSET/1.D6+SMALL)
         IREST=NINT(MOD(MOFFSET,1000000.))
         IMON=IREST/10000
         IF (IMON.LT.1.OR.IMON.GT.12) THEN
            WRITE(6,6040)
            CALL                                   XIT('TSTEP',-3)
         ENDIF
         IREST=MOD(IREST,10000)
         IDAY=IREST/100
         IF (IDAY.LT.1.OR.IDAY.GT.MONTH(IMON)) THEN
            WRITE(6,6045) MONTH(IMON)
            CALL                                   XIT('TSTEP',-4)
         ENDIF
         IHR=MOD(IREST,100)
         IF (IHR.GT.24) THEN
            WRITE(6,6050)
            CALL                                   XIT('TSTEP',-5)
         ENDIF

C     * FINALLY, MOFFSET IN MINUTES SINCE JAN 1 00Z OF YEAR 1.

         MINST=DFLOAT(IYR-1)*DFLOAT(MINYR)+DFLOAT(MINMON(IMON))+
     1         DFLOAT(IDAY-1)*1440.D0+DFLOAT(IHR)*60.D0

      ELSE

C     * IF INPUT FORMAT ILLEGITIMATE, ABORT.

         WRITE(6,6055)
         CALL                                      XIT('TSTEP',-6)
      ENDIF

C     * CHECK IF THE OUTPUT FORMAT IS LEGITIMATE AND SET
C     * VARIABLE IDIV ACCORDINGLY. IDIV IS USED TO RIGHT SHIFT
C     * A YYYYMMDDHH LABEL TO YIELD THE DESIRED OUTPUT FORMAT.

      IF (FORMOUT.EQ.'YYYYMMDDHH')     THEN
         IDIV    = 1.D0
      ELSEIF (FORMOUT.EQ.'  YYYYMMDD') THEN
         IDIV    = 1.D2
      ELSEIF (FORMOUT.EQ.'    YYYYMM') THEN
         IDIV    = 1.D4
      ELSEIF (FORMOUT.EQ.'      YYYY') THEN
         IDIV    = 1.D6
      ELSEIF (FORMOUT.EQ.'  YYMMDDHH') THEN
         IDIV    = 1.D0
      ELSEIF (FORMOUT.EQ.'    YYMMDD') THEN
         IDIV    = 1.D2
      ELSEIF (FORMOUT.EQ.'      YYMM') THEN
         IDIV    = 1.D4
      ELSEIF (FORMOUT.EQ.' FIXY2KBUG') THEN
         IDIV    = LSHIFT
      ELSEIF (FORMOUT.NE.'     MODEL') THEN
         WRITE(6,6060)
         CALL                                      XIT('TSTEP',-7)
      ENDIF

C     * READ THE NEXT GRID FROM FILE XIN.

      IYR1=-999999999
      NRECS=0
      MAXLEN=MAXX+8
 100  CALL FBUFFIN(1,IBUF,MAXLEN,K,LEN)
      IF (K.GE.0) THEN
        IF (NRECS.EQ.0) CALL                       XIT('TSTEP',-8)
        WRITE(6,6065) IBUF
        WRITE(6,6070) NRECS
        CALL                                       XIT('TSTEP', 0)
      ENDIF
C
C     * RESET IYR1 ON SUPERLABELS
C
      IF(IBUF(1).EQ.NC4TO8("LABL"))THEN
        IYR1=-999999999
        MOFFSET=MOFFSET0
      ENDIF
      IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR").OR.
     1  ((FORMIN.EQ.'     MODEL'.OR.FORMIN.EQ.'  ACCMODEL').AND.
     2   IBUF(2).EQ.0))THEN
         CALL RECPUT(2,IBUF)
         GO TO 100
      ENDIF

      IF (NRECS.EQ.0) WRITE(6,6065) IBUF

C     * CONSTRUCT TIME LABEL IN YYYYMMDDHH FORMAT; MODEL TIMESTEPS
C     * ARE DEALT WITH SEPARATELY.

      IF (FORMIN.EQ.'     MODEL'.OR.FORMIN.EQ.'  ACCMODEL') THEN
         MINS=DFLOAT(IBUF(2))*MSTEP+MINST
         IYR=INT(MINS/MINYR+SMALL)+1
         IREST=NINT(MOD(MINS,R8ONE*MINYR))
         DO 200 I=1,12
            IF (IREST.LT.MINMON(I)) GO TO 300
            INDEX=I
 200     CONTINUE
 300     IMON=INDEX
         IREST=IREST-MINMON(INDEX)
         IDAY=IREST/1440+1
         IHR=NINT(MOD(IREST,1440)/60.D0)
         IF(FORMIN.EQ.'  ACCMODEL'.AND.IHR.EQ.0)THEN
C
C           * FIX DATES FOR ACCUMULATED FIELDS
C
            IHR=24
            IDAY=IDAY-1
            IF (IDAY.EQ.0) THEN
               IMON=IMON-1
               IF (IMON.EQ.0) THEN
                  IYR=IYR-1
                  IMON=12
                  IDAY=31
               ELSE
                  IDAY=MONTH(IMON)
               ENDIF
            ENDIF
         ENDIF
         ITIME=1.D6*DFLOAT(IYR)+1.D4*DFLOAT(IMON)+
     1         1.D2*DFLOAT(IDAY)+DFLOAT(IHR)
      ELSEIF (FORMOUT.EQ.'     MODEL') THEN
         ITIME=DFLOAT(IBUF(2))
         IYR=INT(ITIME/1.D6+SMALL)
         IREST=NINT(MOD(ITIME,1000000.))
         IMON=IREST/10000
         IREST=MOD(IREST,10000)
         IDAY=IREST/100
         IHR=MOD(IREST,100)
         MINS=DFLOAT(IYR-1)*DFLOAT(MINYR)+DFLOAT(MINMON(IMON))+
     1        DFLOAT(IDAY-1)*1440.D0+DFLOAT(IHR)*60.D0-MINST
      ELSE
         ITIME=DFLOAT(IBUF(2))*LSHIFT+MOFFSET*1.D6
      ENDIF

C     * FIX Y2K BUG

      IF(FORMOUT.EQ.' FIXY2KBUG')THEN

C     * GET THE YEAR PART

         IYR=INT(ITIME/1.D6+SMALL)

C     * FIX THE CURRENT YEAR IF IT IS SMALLER THAN THE PREVIOUS YEAR

 110     IF(IYR .LT. IYR1) THEN

C     * KEEP ADDING CENTURIES UNTIL THE CURRENT YEAR >= THE PREVIOUS YEAR

           IYR=IYR+100
           ITIME=ITIME+1.D8
           MOFFSET=MOFFSET+100
           GOTO 110
         ENDIF
         IYR1=IYR

C     * CHECK THE MILLENIUM AND CENTURY PARTS WHEN CONVERTING TO 2-DIGIT YEAR.

      ELSEIF (FORMOUT.EQ.'  YYMMDDHH'.OR.FORMOUT.EQ.'    YYMMDD'.OR.
     1    FORMOUT.EQ.'      YYMM') THEN

C     * GET THE MILLENIUM PART

         IMILLEN=INT(ITIME/1.D9+SMALL)

C     * SUBTRACT THE CENTURY PART OF THE FIRST RECORD FROM ITIME

         IF (NRECS.EQ.0)THEN
           ICENTRY=INT(ITIME/1.D8+SMALL)
           JMILLEN=IMILLEN
         ENDIF
         ITIME=ITIME-DFLOAT(ICENTRY)*1.D8

C     * IF MILLENIUM VALUE CHANGES FROM THAT OF THE FIRST RECORD, ABORT.

         IF (IMILLEN.NE.JMILLEN) THEN
            WRITE(6,6085)
            CALL                                   XIT('TSTEP',-9)
         ENDIF
      ENDIF

C     * CONSTRUCT IBUF(2) ACCORDING TO FORMOUT.

      IF (FORMOUT.EQ.'     MODEL') THEN
        ITIME=MINS/MSTEP
      ELSE
        ITIME=ITIME/IDIV
      ENDIF
      IBUF(2)=NINT(ITIME)

C     * SAVE TO FILE XOUT

      IF (NRECS.EQ.0) WRITE(6,6065) IBUF
      CALL FBUFOUT(2,IBUF,LEN,K)
      NRECS=NRECS+1
      GO TO 100
C---------------------------------------------------------------------
 5010 FORMAT(10X,E5.0,E15.0,5X,A10,5X,A10)                                      B4
 6005 FORMAT(1X,'INPUT FORMAT= ',A10,';  OUTPUT FORMAT= ',A10)
 6010 FORMAT(1X,'MODEL TIMESTEP(MSTEP)=',F10.4,
     1     ';  START AT(MOFFSET)=',F15.0)
 6015 FORMAT(1X,/,'TIME INCREMENT MSTEP <=0.')
 6030 FORMAT(1X,'OFFSETTING YEAR IN LABEL BY ',F6.0)
 6035 FORMAT(1X,/,'YEAR IN MOFFSET OUT OF RANGE (<1).')
 6040 FORMAT(1X,/,'MONTH IN MOFFSET OUT OF RANGE (<1 OR >12).')
 6045 FORMAT(1X,/,'DAY IN MOFFSET OUT OF RANGE (<1 OR >',I2,').')
 6050 FORMAT(1X,/,'HOUR IN MOFFSET OUT OF RANGE (>24).')
 6055 FORMAT(1X,/,'INVALID INPUT FORMAT.')
 6060 FORMAT(1X,/,'INVALID OUTPUT FORMAT.')
 6065 FORMAT(1X,A4,1X,I10,2X,A4,5I6)
 6070 FORMAT(1X,'0 TSTEP READ ',I10,' RECORDS')
 6085 FORMAT(1X,/,'MILLENIUM IS NOT ALLOWED TO CHANGE WHEN CONVERTING'
     1       ,'FROM A 4-DIGIT YEAR TO A 2-DIGIT YEAR FORMAT.')
 6090 FORMAT(1X,/,' OUTPUT TIMESTEP OUT OF RANGE:',F12.0)
      END
