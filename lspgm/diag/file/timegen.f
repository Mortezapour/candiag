      PROGRAM TIMEGEN 
C     PROGRAM TIMEGEN (X,       Z,       OUTPUT,                        )       B2
C    1           TAPE1=X, TAPE2=Z, TAPE6=OUTPUT)
C     ------------------------------------------                                B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     AUG 21/00 - F.MAJAESS (ENSURE FIELD DIMENSION CHECK VERSUS "MAXRSZ")      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR 21/88 - F. MAJAESS (COSMETIC MODIFICATIONS)                           
C     DEC 14/84 - M.LAZARE. 
C                                                                               B2
CTIMEGEN - CREATES A FILE WITH VALUES SET EQUAL TO TIMESTEP NUMBER      1  1    B1
C                                                                               B3
CAUTHOR  - MIKE LAZARE                                                          B3
C                                                                               B3
CPURPOSE - CREATES A FILE WITH ALL VALUES SET EQUAL TO THE TIME VALUE           B3
C          FOUND IN IBUF(2).                                                    B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      X = ANY REAL OR COMPLEX FIELD.                                           B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      Z = FILE OF THE SAME KIND AS X BUT IT'S VALUES ARE SET EQUAL TO          B3
C          THE VALUE FOUND IN IBUF(2)                                           B3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/A(SIZES_BLONP1xBLAT) 
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX   /SIZES_BLONP1xBLATxNWORDIO/
      DATA MAXRSZ /SIZES_BLONP1xBLAT/
C---------------------------------------------------------------------
      NF=3
      CALL JCLPNT(NF,1,2,6) 
      IF(NF.LT.3) CALL                             XIT('TIMEGEN',-1)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEXT FIELD.
C 
      NR=0
  140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        IF(NR.EQ.0) CALL                           XIT('TIMEGEN',-2)
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('TIMEGEN',0) 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      TIME=FLOAT(IBUF(2)) 
C 
C     * SET EACH FIELD EQUAL TO IBUF(2).
C 
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
      IF (NWDS.GT.MAXRSZ) CALL                     XIT('TIMEGEN',-3)
      DO 210 I=1,NWDS 
  210 A(I)=TIME 
C 
C     * SAVE THE RESULT ON FILE Z.
C 
      CALL PUTFLD2(2,A,IBUF,MAXX)
      NR=NR+1 
      GO TO 140 
C---------------------------------------------------------------------
 6010 FORMAT('0',I6,'  RECORDS PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
