      PROGRAM SORTLAB
C     PROGRAM SORTLAB (XIN,       XOUT,       OUTPUT,                 )         C2
C    1          TAPE1=XIN, TAPE2=XOUT, TAPE6=OUTPUT)                            C2
C
C     AUTHOR  - Y.JIAO                                                          C3
C                                                                               C3
C     PURPOSE -
C       1. SELECT/OUTPUT ONLY IF IBUF(1)=GRID,DATA AND/OR SUBA
C       2. REMOVE TICTAC, RENAME !! INTO ZTOC
C       3. FOR 2D VARIABLE, IF IBUF(4)=0, CHANGE IBUF(4)=1;
C          FOR 3D VARIABLE, IF IBUF(4)=0, REMOVE IT (EXTRA LEVEL OUTPUT BY GEM4)
C       4. CHANGE IBUF(7)=0
C       5. CHANGE IBUF(8)=2
C---------------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_BLONP1,
     &                       SIZES_BLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      DATA MAXLON, MAXLAT, MAXLL /SIZES_BLONP1, SIZES_BLAT,
     &  SIZES_BLONP1xBLATxNWORDIO/
      COMMON/BLANCK/F(SIZES_BLONP1xBLATxNWORDIO)
C
      LOGICAL OK

      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_BLONP1xBLATxNWORDIO)
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C
C     * READ THE NEXT GRID FROM FILE XIN.
C
      NR=0
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXLL,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0)THEN
          CALL                                     XIT('SORTLAB',-1)
        ELSE
          WRITE(6,6025) IBUF
          WRITE(6,6010) NR
          CALL                                     XIT('SORTLAB',0)
        ENDIF
      ENDIF
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C
C     * OUTPUT GRID DATA ONLY
C
      IF (IBUF(1).NE.NC4TO8("GRID") .AND.
     *    IBUF(1).NE.NC4TO8("DATA") .AND.
     *    IBUF(1).NE.NC4TO8("SUBA")) GOTO 150
      IF (IBUF(3).EQ.NC4TO8("  HY")) GOTO 150
      IF (IBUF(3).EQ.NC4TO8("  >>")) GOTO 150
      IF (IBUF(3).EQ.NC4TO8("  ^^")) GOTO 150
      IF (IBUF(3).EQ.NC4TO8("  ^>")) GOTO 150

C     * OUTPUT TOC ONLY
      IF (IBUF(3).EQ.NC4TO8("  !!")) THEN
        IBUF(3)=NC4TO8("ZTOC")
        IBUF(4)=1
        IBUF(8)=1
      ENDIF

c     remove extra layer output by gem4 (like 2M TEMP, 10M U/V, which have ibuf(4)=0)
      IF (IBUF(2).EQ.JBUF(2).AND.
     *    IBUF(3).EQ.JBUF(3).AND.
     *    IBUF(4).EQ.0) GOTO 150

      IBUF(1)=NC4TO8("GRID")
      IF (IBUF(4).EQ.0) IBUF(4)=1
      IBUF(7)=0
      IF (IBUF(8).GE.2) IBUF(8)=2
      CALL PUTFLD2(2,F,IBUF,MAXLL)

      JBUF(2)=IBUF(2)
      JBUF(3)=IBUF(3)
C
      NR=NR+1
      GO TO 150
C---------------------------------------------------------------------
 6010 FORMAT('0 SORTLAB READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
