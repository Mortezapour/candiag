      PROGRAM VSINTH
C     PROGRAM VSINTH (XIN,     LNSP,      XOUT,      INPUT,      OUTPUT,)       J2
C    1         TAPE1=XIN,TAPE2=LNSP,TAPE3=XOUT,TAPE5=INPUT,TAPE6=OUTPUT)
C     ------------------------------------------------------------------        J2
C                                                                               J2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     DEC 06/93 - D. LIU   (CHANGED SIGMA_TAU)                                  
C     JAN 14/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)               
C     FEB 16/88 - R.LAPRISE.                                                    
C                                                                               J2
CVSINTH  - VERTICAL SIGMA INTEGRAL FOR DATA ON ETA (SIGMA/HYBRID)               J1
C          COORDINATES                                                  2  1 C  J1
C                                                                               J3
CAUTHOR  - R. LAPRISE                                                           J3
C                                                                               J3
CPURPOSE - COMPUTES THE VERTICAL SIGMA INTEGRAL FOR DATA ON ETA                 J3
C          (SIGMA/HYBRID) COORDINATES.                                          J3
C          UPWARD COMPATIBLE WITH FORMER VERSION (VSINT), EXCEPT FOR            J3
C          AN EXTRA INPUT FILE LNSP WHICH IS NOW REQUIRED, AND THE              J3
C          FACT THAT SPECTRAL FIELDS CAN NOT BE HANDLED IN ETA COORD.           J3
C                                                                               J3
CINPUT FILE...                                                                  J3
C                                                                               J3
C      XIN  = INPUT SERIES OF ETA LEVEL GRID FIELDS .                           J3
C      LNSP = INPUT SERIES OF LN(SURFACE PRESSURE) IN MB.                       J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      XOUT = OUTPUT SERIES OF VERTICAL SIGMA INTEGRALS OF XIN.                 J3
C 
CINPUT PARAMETERS...
C                                                                               J5
C      LEVTYP = FULL FOR MOMENTUM VARIABLE, AND                                 J5
C               HALF FOR THERMODYNAMIC ONE.                                     J5
C      CONST  = SCALES THE OUTPUT FIELD.                                        J5
C      LAY    = DEFINES THE POSITION OF LAYER INTERFACES IN RELATION            J5
C               TO LAYER CENTRES (SEE BASCAL).                                  J5
C               (ZERO DEFAULTS TO THE FORMER STAGGERING CONVENTION).            J5
C      ICOORD = 4H ETA/4H SIG FOR ETA/SIGMA LEVELS OF INPUT FIELDS.             J5
C      SIGTOP = VALUE OF SIGMA AT TOP OF DOMAIN FOR VERTICAL INTEGRAL.          J5
C               IF .LT.0., THEN INTERNALLY DEFINED BASED ON LEVTYP              J5
C               FOR UPWARD COMPATIBILITY.                                       J5
C      PTOIT  = PRESSURE (PA) OF THE RIGID LID OF THE MODEL.                    J5
C                                                                               J5
C      NOTE - LAY AND LEVTYP DEFINE THE TYPE OF LEVELLING FOR THE VARIABLE.     J5
C                                                                               J5
CEXAMPLE OF INPUT CARD...                                                       J5
C                                                                               J5
C*VSINTH.  HALF        1.    0  SIG  -1.        0.                              J5
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_PTMIN

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,SPEC 
  
      INTEGER LEV(SIZES_MAXLEV)
  
      REAL ETA (SIZES_MAXLEV),ETAB (SIZES_MAXLEV) 
      REAL AB  (SIZES_MAXLEV),BB   (SIZES_MAXLEV) 
  
      COMMON /LEVELS/ SIGB  (SIZES_LONP1xLAT,2)
      COMMON /BLANCK/ PSMBLN(SIZES_LONP1xLAT), ACC(SIZES_LONP1xLAT), 
     & GG(SIZES_LONP1xLAT) 
      REAL             PRESS(SIZES_LONP1xLAT)
      EQUIVALENCE     (PRESS,PSMBLN)
  
      COMMON /ICOM  /IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
  
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXLEV/SIZES_MAXLEV/ 
C---------------------------------------------------------------------
      NFIL = 5
      CALL JCLPNT(NFIL,1,2,3,5,6) 
      REWIND 1
      REWIND 2
      REWIND 3
  
C     * READ CONTROL DIRECTIVES.
  
      READ(5,5010,END=900) LEVTYP,CONST,LAY,ICOORD,SIGTOP,PTOIT                 J4
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0) 
      ELSE
        PTOIT=MAX(PTOIT,SIZES_PTMIN)
      ENDIF 
      WRITE(6,6005) LEVTYP,CONST,LAY,ICOORD,SIGTOP,PTOIT
  
C     * GET ETA VALUES OF LAYER CENTRES FROM XIN. 
  
      CALL FILEV (LEV,ILEV,IBUF,1)
      IF(ILEV.LT.1 .OR. ILEV.GT.MAXLEV)  CALL      XIT('VSINTH',-1) 
      WRITE(6,6007) ILEV,(LEV(L),L=1,ILEV)
  
      CALL LVDCODE(ETA,LEV,ILEV)
      DO 150 L=1,ILEV 
         ETA(L)=ETA(L)*0.001E0
  150 CONTINUE
  
C     * DETERMINE THE FIELD SIZE. 
  
      KIND=IBUF(1)
      NAME=IBUF(3)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) THEN 
         IF(ICOORD.NE.NC4TO8(" SIG")) CALL         XIT('VSINTH',-2)
         NWDS=2*NWDS
      ENDIF 
  
C     * EVALUATE LAYER INTERFACES FROM LEVTYP AND LAY.
  
      IF    (LEVTYP.EQ.NC4TO8("FULL")) THEN
         CALL BASCAL (ETAB,IBUF, ETA,ETA,ILEV,LAY)
      ELSEIF(LEVTYP.EQ.NC4TO8("HALF")) THEN
         CALL BASCAL (IBUF,ETAB, ETA,ETA,ILEV,LAY)
      ELSE
         CALL                                      XIT('VSINTH',-3) 
      ENDIF 
  
C     * EVALUATE THE PARAMETERS A AND B OF THE VERTICAL DISCRETIZATION
C     * FOR THE LAYER BASES.
  
      CALL COORDAB (AB,BB, ILEV,ETAB,ICOORD,PTOIT)
C---------------------------------------------------------------------
      NSETS=0 
  200 CONTINUE
  
C     * READ NEXT LN(PS) NEEDED TO EVALUATE THE SIGMA LEVELS. 
  
      IF(SPEC) THEN 
         ITIM=-1
         DO 250 I=1,NWDS
            PRESS(I)=0.E0 
  250    CONTINUE 
      ELSE
         CALL GETFLD2 (2,PSMBLN,KIND,-1,NC4TO8("LNSP"),1,IBUF,MAXX,OK)
         IF(NSETS.EQ.0) WRITE(6,6025) IBUF
         IF(.NOT.OK) THEN 
            IF(NSETS.GT.0) THEN 
               WRITE(6,6025) IBUF 
               WRITE(6,6020) NSETS
               CALL                                XIT('VSINTH',0)
            ELSE
               CALL                                XIT('VSINTH',-4) 
            ENDIF 
         ENDIF
         ITIM=IBUF(2) 
         DO 300 I=1,NWDS
            PRESS(I)=100.E0*EXP(PSMBLN(I))
  300    CONTINUE 
      ENDIF 
  
C     * INITIALIZE THE CUMULATIVE ARRAY, AND DEFINE TOP SIGMA VALUE.
  
      IGH = 1 
      LOW = 2 
      DO 400 I=1,NWDS 
         ACC (I)     = 0.E0 
  400 CONTINUE
  
      IF(SIGTOP.LT.0.E0)THEN
         IF(LEVTYP.EQ.NC4TO8("FULL"))THEN
            DO 440 I=1,NWDS 
               SIGB(I,IGH)=0.0E0
  440       CONTINUE
         ELSE 
            DO 450 I=1,NWDS 
               SIGB(I,IGH)=PTOIT/PRESS(I)
  450       CONTINUE
         ENDIF
      ELSE
         DO 460 I=1,NWDS
            SIGB(I,IGH)=SIGTOP
  460    CONTINUE 
      ENDIF 
  
C     * PROCESS ALL LEVELS AND ACCUMULATE SIGMA VERTICAL INTEGRAL.
  
      DO 800 L=1,ILEV 
  
         CALL GETFLD2 (1,GG,KIND,ITIM,NAME,LEV(L),IBUF,MAXX,OK)
         IF(NSETS.EQ.0) WRITE(6,6025) IBUF
         IF(.NOT.OK)  CALL                         XIT('VSINTH',-10-L)
  
         CALL NIVCAL  (SIGB(1,LOW), AB(L),BB(L),PRESS,1,NWDS,NWDS)
  
         DO 500 I=1,NWDS
            ACC(I) = ACC(I) + GG(I)*CONST*(SIGB(I,LOW)-SIGB(I,IGH)) 
  500    CONTINUE 
  
         KEEP= LOW
         LOW = IGH
         IGH = KEEP 
  
  800 CONTINUE
  
C     * PUT THE RESULT ONTO FILE XOUT.
  
      IBUF(4)=1 
      CALL PUTFLD2 (3,ACC,IBUF,MAXX) 
      NSETS=NSETS+1 
  
      GO TO 200 
  
C     * E.O.F. ON INPUT.
  
  900 CALL                                         XIT('VSINTH',-5) 
C---------------------------------------------------------------------
 5010 FORMAT(11X,A4,E10.0,I5,1X,A4,E5.0,E10.0)                                  J4
 6005 FORMAT('  LEVTYP = ',A4,', CONST = ',1P,E12.4,
     1       ', LAY = ',0P,I5,', COORD=',1X,A4,', SIGTOP =',F15.10,
     2       ', P.LID (PA)=',E10.3)
 6007 FORMAT(' ILEV,ETA =',I5,2X,20I5/(18X,20I5))
 6020 FORMAT('0',I6,' SETS WERE PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,5I10)
      END
