      PROGRAM SORTVAR
C     PROGRAM SORTVAR (XIN,       XOUT,       OUTPUT,              )
C
CAUTHOR  - Y.JIAO: MAY 18, 2018
C
CPURPOSE -
C     IN GEM4/5 OUTPUT, SOME VARIABLES (SUCH AS GZ AND PX) HAVE BEEN
C     OUTPUT ON BOTH THERMAL AND MOMENTUM LEVELS.
C     THIS CODE IS TO SELECT AND OUTPUT THE
C     SAME VERTICAL LEVELS AS IN THE 2ND FILE
CEXAMPLE:
C     sortvar GZ TEMP GZT    (OUTPUT GZT ON THERMAL LEVELS ONLY)
C     sortvar PX TEMP APT    (OUTPUT APT ON THERMAL LEVELS ONLY)
C     sortvar PX U    APM    (OUTPUT APM ON MOMENTUM LEVELS ONLY)
C-----------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_MAXLEVxBLONP1xBLAT, 
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER(MAXLEV=2*SIZES_MAXLEV)
      PARAMETER(MAXX=SIZES_MAXLEVxBLONP1xBLAT)
      REAL, ALLOCATABLE, DIMENSION(:) :: F
C
      LOGICAL OK
      INTEGER LEV(MAXLEV)

      COMMON/ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C---------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3

      ALLOCATE(F(MAXX))
C     GET THE THERMAL LEVELS FROM 2ND INPUT FILE
      CALL GETSET2(2,F,LEV,NLEV,IBUF,MAXX,OK)
C
C     * READ THE NEXT GRID FROM FILE XIN.
C
      NR=0
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0)THEN
          CALL                                     XIT('SORTVAR',-1)
        ELSE
          WRITE(6,6025) IBUF
          WRITE(6,6010) NR
          CALL                                     XIT('SORTVAR', 0)
        ENDIF
      ENDIF
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C
C     * OUTPUT GRID DATA ONLY
C
      IF (ANY(LEV(1:NLEV) .EQ. IBUF(4)) ) THEN
        CALL PUTFLD2(3,F,IBUF,MAXX)
      ENDIF
C
      NR=NR+1
      GO TO 150
C---------------------------------------------------------------------
 6010 FORMAT('0 SORTVAR OUTPUT',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END


