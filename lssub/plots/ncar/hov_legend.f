      SUBROUTINE HOV_LEGEND(POSI,CLRPLT,PATPLT)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C This routine draws legend.
C      IMPLICIT NONE
C Colour LEGEND-plotting routine.

      INTEGER POSI
      LOGICAL CLRPLT,PATPLT

      REAL XMAPMN,XMAPMX,YMAPMN,YMAPMX,DUM1,DUM2,DUM3,DUM4
      INTEGER NCLL
      INTEGER IDUM
      REAL XBARMN, XBARMX, YBARMN, YBARMX, BOXWDT
      INTEGER I
      REAL CLV
      INTEGER NLBLS,NK,NNK,ITMP
      PARAMETER(NLBLS = 35)
      CHARACTER*10 LBLS(NLBLS),TLBL
C For passing colour index array into colour FILL routines through
C Common Block.
      INTEGER NIPAT, NPAT, MAXPAT, MAXCLRS
      PARAMETER(NIPAT = 28)
      INTEGER IPAT(NIPAT)
      REAL ZLEV(NIPAT)
      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS

      INTEGER LFIN(NIPAT)
      REAL CHSIZE
      INTEGER LSTRBEG,LSTREND
      EXTERNAL LSTRBEG,LSTREND
C
      NCLL = NPAT-1
CC      CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', NCLL)
C Duplex font.
      CALL PCSETI('CD', 1)
      CALL PCSETI('FN', 0)
      CALL LBSETR('WLB',2)

C Finish defining the label bar parameters.
      CALL GETSET(XMAPMN, XMAPMX, YMAPMN, YMAPMX,
     1            DUM1,DUM2,DUM3,DUM4,IDUM)
      IF (POSI .EQ. 1) THEN
        XBARMX = XMAPMX + 0.10
        XBARMN = XMAPMX + 0.01
        YBARMN = YMAPMN + 0.025
        YBARMX = YMAPMN + 0.675
        BOXWDT = (YBARMX - YBARMN)/MAX(NPAT,7)
      ELSE
        XBARMN = XMAPMN -0.01
        XBARMX = XMAPMX - 0.10
        YBARMN = 0.08
        YBARMX = 0.14
        BOXWDT = (XBARMX - XBARMN)/MAX(NPAT,7)
      ENDIF
C
C Define the contour level numeric labels.
C
C      CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', ITMP)
      TOL=1.D-6
      TOLA=5.D-6
      DO I = 1, NCLL
         CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
         CALL CPGETR('CLV - CONTOUR LEVEL VALUES', CLV)
         IZZ=0
         FLVAL=CLV
         IF(FLVAL.EQ.0.0.OR.
     1        (FLVAL.GE.-TOL.AND.FLVAL.LE.TOL)) THEN
            WRITE(TLBL,2002)
         ELSEIF(ABS(FLVAL).GT.9999..OR.ABS(FLVAL).LT.0.001) 
     1           THEN
            WRITE(TLBL,2000) FLVAL            
         ELSE
            FVAL=ABS(1.0D0*FLVAL)+1.D0*TOLA
            FREST=10.D0*(1.D0*FVAL-AINT(1.D0*FVAL))
            DO J=1,3
               IFINT=AINT(FREST)
               IF(IFINT.GT.0) IZZ=J
               FREST=(10.D0*(1.D0*FREST-AINT(1.D0*FREST)))
            ENDDO
            IF(IZZ.EQ.0) THEN
               IF(FLVAL.LT.0.) THEN
                  IFLVAL=AINT(FLVAL-TOLA)
               ELSE
                  IFLVAL=AINT(FLVAL+TOLA)
               ENDIF
               WRITE(TLBL,992) IFLVAL
            ELSEIF(IZZ.EQ.1) THEN
               WRITE(TLBL,2030) FLVAL
            ELSEIF(IZZ.EQ.2) THEN
               WRITE(TLBL,2031) FLVAL
            ELSEIF(IZZ.EQ.3) THEN
               WRITE(TLBL,2032) FLVAL
            ELSE
               WRITE(TLBL,2000) FLVAL
            ENDIF
         ENDIF
         LBLS(I)=TLBL(LSTRBEG(TLBL):LSTREND(TLBL))
      ENDDO

CC         WRITE (LBLS(I), '(F8.02)') CLV
CC         TLBL = LBLS(I)
CC         LBLS(I)=TLBL(LSTRBEG(LBLS(I)):LSTREND(LBLS(I)))
CDEBUG
C!!!         WRITE(*,90) I,LBLS(I)
C!!! 90      FORMAT('LGND: CLV(',I3,'): "',A,'"')
CDEBUG
CC 10   CONTINUE

      IF (.NOT.PATPLT) THEN
         DO 20 I = 1, NPAT
            CALL DFNCLR(IPAT(I), LFIN(I))
C            LFIN(I) = IPAT(I) - 98
C            WRITE(6,*) 'FIRST ',I,NPAT,NCLL, IPAT(I),LFIN(I)
 20      CONTINUE
C Draw and fill a label bar.
         CALL GSLWSC(2.)
         IF (POSI .EQ. 1) THEN
            CALL LBSETR('WLB',3.)
            CALL LBLBAR(1, XBARMN, XBARMX,
     1           YBARMN, YBARMN + NPAT * BOXWDT, NCLL+1,
     2           .40, 1.0, LFIN, 0,
     3           LBLS, NCLL, 1)
         ELSE
            CALL LBLBAR(0, XBARMN, XBARMN + NPAT * BOXWDT,
     1           YBARMN, YBARMX, NCLL+1, 1.0, 0.5, LFIN, 0,
     2           LBLS, NCLL, 1)
         ENDIF
      ELSE IF(.NOT.CLRPLT) THEN
         DO 30 I = 1, NPAT
            CALL DFNCLR(IPAT(I), LFIN(I))
C            WRITE(6,*) 'MID ',I,NPAT,IPAT(I),LFIN(I)
 30      CONTINUE
C Draw and fill a label bar.
         IF(POSI.EQ.1) THEN
            CALL LBSETR('WLB',3.)
            CALL LBLBAR(1, XBARMN, XBARMX,
     1           YBARMN, YBARMN + NPAT * BOXWDT, NCLL+1,
     2           0.40, 1.0, LFIN, 2,
     3           LBLS, NCLL, 1)
         ELSE
            CALL LBLBAR(0, XBARMN, XBARMN + NPAT * BOXWDT,
     1           YBARMN, YBARMX, NCLL+1, 1.0, 0.5, LFIN, 2,
     2           LBLS, NCLL, 1)
         ENDIF
      ELSE
         DO 40 I = 1, NPAT
            IF (IPAT(I).GE.100 .AND. IPAT(I).LE.199) THEN
               LFIN(I) = IPAT(I) - 98
            ELSEIF (IPAT(I).GE.350 .AND. IPAT(I).LE.420) THEN
               LFIN(I) = IPAT(I) - 248
            ELSE
               CALL DFNCLR(IPAT(I), LFIN(I))
            ENDIF
C            WRITE(6,*) 'LAST ',I,NPAT,IPAT(I),LFIN(I)
 40      CONTINUE
C     Draw and fill a label bar.
         CALL GSLWSC(2.)
         IF (POSI .EQ. 1) THEN
            CALL LBSETR('WLB',3.)
            CALL LBLBAR(1, XBARMN, XBARMX,
     1           YBARMN, YBARMN + NPAT * BOXWDT, NCLL+1,
     2           .40, 1.0, LFIN, 2,
     3           LBLS, NCLL, 1)
         ELSE
            CALL LBLBAR(0, XBARMN, XBARMN + NPAT * BOXWDT,
     1           YBARMN, YBARMX, NCLL+1, 1.0, 0.5, LFIN, 2,
     2           LBLS, NCLL, 1)
         ENDIF
      ENDIF
  992 FORMAT(I7)
 2000 FORMAT(1P1E10.1)
 2001 FORMAT(F6.2)
 2002 FORMAT('0')
 2003 FORMAT(F5.1)
 2004 FORMAT(F6.2)
 2005 FORMAT(F7.3)
 2006 FORMAT(I10)
 2030 FORMAT(F6.1)
 2031 FORMAT(F7.2)
 2032 FORMAT(F8.3)
      RETURN
      END
