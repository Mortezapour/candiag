      SUBROUTINE RCOM(WORKA,WORKB,AA,BB,DATA,DATB,DATC,FIN,KP6,K3)
C 
C     *****   FEB 1976  -  ROGER DALEY   *****
C    * COMPLETES GRID TO FOURIER TRANSFORM FOR 3 TIMES A POWER OF 2.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX WORKA(1),WORKB(1),AA,BB,CC,DD 
      COMPLEX DATA(1),DATB(1),DATC(1),FIN(1)
C-----------------------------------------------------------------------
C 
      DO 100 K=1,KP6
      CC = WORKB(K)*DATB(K) 
      DD = WORKA(K)*DATC(K) 
      KP = K3+2-K 
      FIN(K) = DATA(K) + CC + DD
      FIN(KP) = CONJG(DATA(K) + AA*CC + BB*DD)
  100 CONTINUE
C 
      RETURN
      END
