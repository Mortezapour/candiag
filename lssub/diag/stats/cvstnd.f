      FUNCTION CVSTND(P)
C
C     * CVSTND RETURNS P UPPER TAIL CRITICAL VALUE OF THE 
C     * STANDARD NORMAL DISTRIBUTION.
C
C     * 7/APR 1999
C
C     * AUTHOR: SLAVA KHARIN
C     * (BISECTION METHOD)
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DATA SQR2/0.70710678119E0/,MAXIT/100/,EPS/1.E-6/
C--------------------------------------------------------------------
      PP=P
      IF (P.LT.0.5E0)PP=1.E0-P
      T1=0.E0
      T2=1.E+3
      P1=0.5E0*(1.E0+ERF1(T1*SQR2))
      P2=0.5E0*(1.E0+ERF1(T2*SQR2))
      IF ((P1-PP)*(P2-PP).GT.0.E0) THEN
         WRITE(6,'(A)')
     1        ' *** ERROR IN CVSTND: ROOT MUST BE BRACKETED.'
         CALL                                      XIT('CVSTND',-1)
      ENDIF
      DO IT=1,MAXIT
         T=0.5E0*(T1+T2)
         P1=0.5E0*(1.E0+ERF1(T*SQR2))
         IF (P1.GT.PP) THEN
            T2=T
         ELSE
            T1=T
         ENDIF
         IF (T2-T1.LT.EPS) GOTO 100
      ENDDO
C
      WRITE(6,'(A)')
     1     ' *** ERROR IN CVSTND: TOO MANY BISECTIONS.'
      CALL                                         XIT('CVSTND',-2)
C
 100  CONTINUE
      CVSTND=T
      IF (P.LT.0.5E0)CVSTND=-T
      RETURN
      END
