      SUBROUTINE LLIGGM(GG,ILG1,ILAT,DLAT, GLL,NLG1,NLAT,
     +                                    ALON,ALAT,SPVAL)
C 
C     * FEB 25/93 - F.MAJAESS (MODIFIED FROM LLIGG2 ROUTINE) 
C     * LINEARILY INTERPOLATES GLOBAL GAUSSIAN GRID 
C     * GG(ILG1,ILAT) FROM GLOBAL LAT-LONG GRID 
C     * GLL(NLG1,NLAT) AT THE LONGITUDES IN ALON(NLG1) AND 
C     * LATITUDES IN ALAT(NLAT).
C     * VALUES EQUAL TO "SPVAL" ARE TREATED AS BAD/MISSING
C     * DATA.
C     * DLAT = LAT  (DEG) OF GG  ROWS.
C     * ALON = LONG (DEG) OF GLL COLUMNS.
C     * ALAT = LAT  (DEG) OF GLL ROWS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GG(ILG1,ILAT),GLL(NLG1,NLAT) 
      REAL DLAT(ILAT),ALON(NLG1),ALAT(NLAT) 
C-------------------------------------------------------------------- 
      ILG=ILG1-1
      NLG=NLG1-1
C 
C     * ROWS ARE DONE FROM SOUTH TO NORTH.
C     * DROW = DEGREES OF ROW J FROM S POLE.
C     * (X,Y) = COORD OF GG(I,J) IN LAT-LONG GRID.
C 
      DO 240 J=1,ILAT 
      DROW=DLAT(J)
      IF(DROW.GE.ALAT(1).AND.DROW.LE.ALAT(NLAT)) THEN
       Y=0.0E0
       DO 100 JJ=1,NLAT-1
         IF( DROW.GE.ALAT(JJ).AND.DROW.LE.ALAT(JJ+1)) THEN
           Y=JJ+(DROW-ALAT(JJ))/(ALAT(JJ+1)-ALAT(JJ))
           GO TO 110
         ENDIF
  100  CONTINUE
      ELSE
        IF(DROW.LT.ALAT(1)) THEN
          Y=1.0E0
        ELSE
          Y=FLOAT(NLAT)
        ENDIF 
      ENDIF 
  110 CONTINUE
C 
C     * POINTS ARE INTERPOLATED FROM LEFT TO RIGHT. 
C     * DLON = DEGREES OF POINT I EAST FROM GREENWICH.
C 
      DO 220 I=1,ILG
      DLON=FLOAT(I-1)/FLOAT(ILG)*360.E0 
      IF(DLON.GE.ALON(1).AND.DLON.LE.ALON(NLG)) THEN
       X=0.0E0
       DO 150 II=1,NLG
         IF( DLON.GE.ALON(II).AND.DLON.LE.ALON(II+1)) THEN
           X=II+(DLON-ALON(II))/(ALON(II+1)-ALON(II))
           GO TO 160
         ENDIF
  150  CONTINUE
      ELSE
        IF(DLON.LT.ALON(NLG)) DLON=DLON+360.E0 
        X=NLG+(DLON-ALON(NLG))/(ALON(NLG1)-ALON(NLG))
      ENDIF 
  160 CONTINUE
      VAL=SPVAL
      CALL GINTLM(VAL,GLL,NLG1,NLAT,X,Y,SPVAL)
      GG(I,J)=VAL 
  220 CONTINUE
C 
      GG(ILG1,J)=GG(1,J)
  240 CONTINUE
C 
      RETURN
      END 
