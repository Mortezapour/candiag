      SUBROUTINE EATHL  (FS,LA,TH,NTHL,FTH,NSL,PSLOG,RLUP,RLDN,TEMPS,   
     1                   A,B,NSL1,                                 
     2                   FSC,THS,DFDLNTH,DLNTH)
                                                                        
C     * OCT  5/94 - J.KOSHYK (INTRODUCE COLUMN ARRAY FSC TO CORRECT    
C                   CALCULATION OF FTH).        
C     * JUL 27/94 - J. KOSHYK
                                                    
C     * INTERPOLATES MULTI-LEVEL SETS OF GRIDS FROM ETA OR PRESSURE LEVELS        
C     * TO THETA LEVELS.  THE INTERPOLATION IS LINEAR IN LN(THETA).              
C                                                                                 
C     * ALL GRIDS HAVE THE SAME HORIZONTAL SIZE (LA POINTS).                    
C     * FTH      = OUTPUT GRIDS ON THETA LEVELS.                               
C     * TH(NTHL) = VALUES OF INPUT THETA LEVELS (K).                        
C     *            (MUST BE MONOTONIC AND DECREASING).                      
C     * FS       = INPUT GRIDS ON ETA OR PRESSURE LEVELS.                      
C     * PSLOG    = INPUT GRID OF LN(SURFACE PRESSURE IN MB).                  
C     * RLUP     = LAPSE RATE, DF/D(LN (THETA)) USED TO EXTRAPOLATE
C     *            ABOVE TOP ETA.
C     * RLUP     = LAPSE RATE USED TO EXTRAPOLATE BELOW BOTTOM ETA.
C     * TEMPS    = INPUT GRIDS OF TEMPERATURE ON ETA LEVELS.
C     * NSL1     = NSL+1.                                                         
C                                                                                 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL FS(LA,NSL),PSLOG(LA),TEMPS(LA,NSL)
      REAL TH  (NTHL),A   (NSL),B(NSL)
      REAL FTH (LA,NTHL)                                        

C     * WORK SPACE.                                                               
                                                                        
      REAL FSC(NSL),THS(NSL),DFDLNTH(NSL1),DLNTH(NSL),KAPPA             
C
      PARAMETER(P0=101320.E0,KAPPA=0.285714E0)

C---------------------------------------------------------------------------------
C     * LOOP OVER ALL HORIZONTAL POINTS.                                          

      DO 500 I=1,LA                                                     
                                                                        
C     * COMPUTE THE POTENTIAL TEMPERATURE ON ETA LEVELS.

      DO 150 L=1,NSL
         PRES   = A(L)+B(L)*(100.E0*EXP(PSLOG(I)))
         THS(L) = TEMPS(I,L)*(P0/PRES)**KAPPA
  150 CONTINUE
C
      DO 180 L=1,NSL-1                                                  
         DLNTH(L) =1.E0/(LOG(THS(L+1))-LOG(THS(L)))                     
  180 CONTINUE                                                          
                                                                        
C
      DO 210 L=1,NSL                                                    
         FSC(L) = FS(I,L)                                
  210 CONTINUE                                                          
                                                                        
C     * COMPUTE VERTICAL DERIVATIVE OVER ALL ETA INTERVALS.                  
                                                                        
      DO 260 L=1,NSL-1                                                  
  260 DFDLNTH(L+1  ) = (FSC(L+1)-FSC(L))*DLNTH(L)                       
      DFDLNTH(1    ) = RLUP                                             
      DFDLNTH(NSL1 ) = RLDN                                             
C---------------------------------------------------------------------------------
                                                                        
C     * LOOP OVER THETA LEVELS TO BE INTERPOLATED.                             
                                                                        
      K=1                                                               
      DO 350 N=1,NTHL                                                   

C     * FIND WHICH SIGMA INTERVAL WE ARE IN.                                      
                                                                        
      DO 310 L=K,NSL                                                    
      INTVL=L                                                           
  310 IF(TH(N).GT.THS(L)) GO TO 320                                     
      INTVL=NSL1                                                        
  320 K=INTVL-1                                                         
      IF(K.EQ.0) K=1                                                    
                                                                        
C     * NOW INTERPOLATE AT THIS POINT.                                            

  350 FTH(I,N)  = FSC(K)+DFDLNTH(INTVL)*(LOG(TH(N))-LOG(THS(K)))        
  500 CONTINUE                                                          
                                                                        
      RETURN                                                            
      END  
