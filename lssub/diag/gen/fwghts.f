      SUBROUTINE FWGHTS(L0,K,W,LOPASS,SUMW) 

C     * JAN 5, 98 - F. ZWIERS (MINOR CORRECTION TO THE IMPLEMENTATION
C                              OF THE HANNING TAPER ... THE TAPER NOW
C                              DECAYS TO ZERO AT LAG K+1 RATHER THAN
C                              LAG K. REMOVAL OF THE CONVOLVED MOVING
C                              AVERAGE OPTION)
C     * JUILLET 18/85 - F. ZWIERS, N.E. SARGENT, B.DUGAS. 
  
C     * COMPUTE WEIGHTS FOR:  
  
C     * IDEAL LOW PASS FILTER WITH CUTOFF FREQUENCY L0
C     * USING HANNING TAPER WITH K WEIGHTS, 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION W(1)
      REAL      L0
      LOGICAL   LOPASS
      DATA      PI/3.1415927E0/,PII/0.3183099E0/
  
C---------------------------------------------------------------------------- 

      IF (K.GE.0)                                              THEN 
  
C         * IDEAL FILTER. 
  
          FK     = FLOAT(K+1) 
          W(K+1) = L0/PI
          SUMW   = W(K+1) 
          DO 110 I=1,K
              FI       = FLOAT(I) 
              AK       = SIN(FI*L0)*PII/FI
              HK       = 0.5E0*(1.0E0+COS(PI*FI/FK))
              BK       = HK*AK
              W(K+1+I) = BK 
              W(K+1-I) = BK 
              SUMW     = SUMW+BK+BK 
  110     CONTINUE
  
          IF (.NOT.LOPASS) SUMW = -SUMW 
          DO 120 I=1,2*K+1
              W(I) = W(I)/SUMW
  120     CONTINUE
          IF (.NOT.LOPASS) W(K+1) = W(K+1)+1
  
      ELSE

          WRITE(6,6010)
          CALL                                     XIT('0FWGHTS',-1)
  
      ENDIF 

 6010 FORMAT('0***** SEQUENTIAL MOVING AVERAGE FILTER OPTION',
     1       ' NO LONGER AVAILABLE *****')    
      RETURN
      END 
