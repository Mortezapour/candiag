      FUNCTION BETAI1(A,B,X)
C
C     * THIS FUNCTION IS TAKEN FROM NUMERICAL RECIPES.
C     * PAUSE STATEMENT IS REPLACED BY A CALL TO "XIT" SUBROUTINE.
C     * CALLS TO "BETACF" AND "GAMMLN" ARE REPLACED BY "BETACF1" 
C     * AND "GAMMLN1".
C     * THE ORIGINAL NAME "BETAI" IS CHANGED TO "BETAI1".
C
C     JUN 28/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL BETAI1,A,B,X
CU    USES BETACF1,GAMMLN1
      REAL BT,BETACF1,GAMMLN1
C--------------------------------------------------------------------
      IF(X.LT.0.E0.OR.X.GT.1.E0) THEN
         WRITE (6,'(A)') 
     1        ' *** ERROR IN BETAI1: BAD ARGUMENT X.'
         CALL                                      XIT('BETAI1',-1)
      ENDIF
      IF(X.EQ.0.E0.OR.X.EQ.1.E0)THEN
        BT=0.E0
      ELSE
        BT=EXP(GAMMLN1(A+B)-GAMMLN1(A)-
     &    GAMMLN1(B)+A*LOG(X)+B*LOG(1.E0-X))
      ENDIF
      IF(X.LT.(A+1.E0)/(A+B+2.E0))THEN
        BETAI1=BT*BETACF1(A,B,X)/A
      ELSE
        BETAI1=1.E0-BT*BETACF1(B,A,1.E0-X)/B
      ENDIF
C
      RETURN
      END
