      SUBROUTINE BASCAL (SGB,SHB, SG,SH, NK,LAY)
  
C     * JAN 25/93 - E.CHAN.  (CALL WRITLEV TO PRINT OUT LEVELS)
C     * MAY 30/88 - M.LAZARE (MODIFY FORMAT STATEMENT TO HANDLE 50 LEVS)
C     * JAN 29/88 - R.LAPRISE.
C     * CALCUL DES BASES DES COUCHES A PARTIR DE
C     * LA POSITION DES "MILIEUX".
C     * 
C     * SGB(K)    : BASES DES COUCHES DU VENT.
C     * SHB(K)    : BASES DES COUCHES DE TEMPERATURE. 
C     * SG(K)     : MILIEU DES COUCHES DU VENT. 
C     * SH(K)     : MILIEU DES COUCHES DE TEMPERATURE.
C     * NK        : NOMBRE DE COUCHES.
C     * 
C     * LAY = TYPE D'ARRANGEMENT DES INTERFACES PAR RAPPORT 
C     *       AUX MILIEUX DE COUCHES. 
C     *     = 0, COMME LAY=2 POUR V ET LAY=4 POUR T,
C     *          POUR COMPATIBILITE AVEC VERSION ANTERIEURE DU GCM. 
C     *     = 1 / 2, INTERFACES DES COUCHES PLACEES A DISTANCE EGALE
C     *              SIGMA / GEOMETRIQUE DU MILIEU DES COUCHES
C     *              ADJACENTES.
C     *     = 3 / 4, BASES DES COUCHES EXTRAPOLEES EN MONTANT 
C     *              DE SORTE QUE LES MILIEUX DES COUCHES SOIENT
C     *              AU CENTRE SIGMA / GEOMETRIQUE DES COUCHES. 
C     * DECOUPLAGE DE L'EPAISSEUR DES COUCHES PEUT RESULTER 
C     * DE L'APPLICATION DE LAY=3 OU 4 (AUSSI =0 POUR T) SI 
C     * LES MILIEUX DE COUCHES NE SONT PAS DEFINIS DE FACON 
C     * APPROPRIES. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SGB(NK), SHB(NK) 
      REAL  SG(NK), SH (NK) 
C-------------------------------------------------------------------- 
C     * DEFINITION DES BASES DES COUCHES. 
C 
      IF(LAY.EQ.0) THEN 
         SGB(NK)=1.E0 
         SHB(NK)=1.E0 
         DO 1 K=NK-1,1,-1 
            SGB(K) = SQRT(SG(K)*SG(K+1))
            SHB(K) = SH(K+1)**2/SHB(K+1)
    1    CONTINUE 
C 
      ELSEIF(LAY.EQ.1) THEN 
         DO 3 K=1,NK-1
            SGB(K)=0.5E0*(SG(K)+SG(K+1))
            SHB(K)=0.5E0*(SH(K)+SH(K+1))
    3    CONTINUE 
         SGB(NK)=1.0E0
         SHB(NK)=1.0E0
C 
      ELSEIF(LAY.EQ.2) THEN 
         DO 4 K=1,NK-1
            SGB(K)=SQRT(SG(K)*SG(K+1))
            SHB(K)=SQRT(SH(K)*SH(K+1))
    4    CONTINUE 
         SGB(NK)=1.0E0
         SHB(NK)=1.0E0
C 
      ELSEIF(LAY.EQ.3) THEN 
         SGB(NK)=1.0E0
         SHB(NK)=1.0E0
         DO 5 K=NK-1,1,-1 
            SGB(K)=2.E0*SG(K+1)-SGB(K+1)
            SHB(K)=2.E0*SH(K+1)-SHB(K+1)
    5    CONTINUE 
C 
      ELSEIF(LAY.EQ.4) THEN 
         SGB(NK)=1.0E0
         SHB(NK)=1.0E0
         DO 6 K=NK-1,1,-1 
            SGB(K)=SG(K+1)**2/SGB(K+1)
            SHB(K)=SH(K+1)**2/SHB(K+1)
    6    CONTINUE 
C 
      ELSE
         CALL XIT('BASCAL',-1) 
      ENDIF 
C 
      CALL WRITLEV(SG,NK,' SG ')
      CALL WRITLEV(SGB,NK,' SGB')
      CALL WRITLEV(SH,NK,' SH ')
      CALL WRITLEV(SHB,NK,' SHB')
      RETURN
C-------------------------------------------------------------- 
      END
