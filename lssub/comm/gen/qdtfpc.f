      SUBROUTINE QDTFPC(Q,D,P,C,LA,LSR,LM,ILEV,KASE)
C 
C     * APR 11/79 - J.D.HENDERSON 
C     * Q(LA,ILEV) = SPECTRAL VORTICITY 
C     * D(LA,ILEV) = SPECTRAL DIVERGENCE
C     * P(LA,ILEV) = SPECTRAL STREAMFUNCTION
C     * C(LA,ILEV) = SPECTRAL VELOCITY POTENTIAL
C 
C     * KASE = +1 CONVERTS P,C TO Q,D.
C     * KASE = -1 CONVERTS Q,D TO P,C.
C 
C     * LEVEL 2,Q,D,P,C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSR(2,1)
      COMPLEX Q(LA,ILEV),D(LA,ILEV),P(LA,ILEV),C(LA,ILEV) 
C-------------------------------------------------------------------
C     * KASE = +1 CONVERTS P,C TO Q,D.
C 
      IF(KASE.LT.0) GO TO 310 
      DO 290 L=1,ILEV 
      DO 290 M=1,LM 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      DO 290 K=KL,KR
      NS=(M-1)+(K-KL) 
      FNS1=FLOAT(NS*(NS+1)) 
      Q(K,L)=-FNS1*P(K,L) 
      D(K,L)=-FNS1*C(K,L) 
  290 CONTINUE
      RETURN
C 
C     * KASE = -1 CONVERTS Q,D TO P,C.
C 
  310 RFNS1=0.E0
      DO 390 L=1,ILEV 
      DO 390 M=1,LM 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      DO 390 K=KL,KR
      NS=(M-1)+(K-KL) 
      IF(NS.GT.0) RFNS1=1.E0/FLOAT(NS*(NS+1)) 
      P(K,L)=-RFNS1*Q(K,L)
      C(K,L)=-RFNS1*D(K,L)
  390 CONTINUE
C 
      RETURN
      END
