      SUBROUTINE CF1I64C(IEEEF,CF,NF)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * APR 15/92 - J. STACEY - A SLIGHTLY MODIFIED VERSION OF "CFI64C",
C     *                         WHICH WILL COMPILE ON BOTH 32 AND 64 BIT
C     *                         MACHINES (BUT OBVIOUSLY WILL GIVE USABLE
C     *                         RESULTS ONLY ON CRAYS).
C     *
C     * CONVERT FLOATING POINT, IEEE 64-BIT, TO CRAY FLOATING POINT
C     *
C     * INPUT : IEEEF   IEEE FLOATING POINT NUMBERS (DOUBLE PRECISION)
C     *         NF      NUMBER OF ELEMENTS IN IEEEF
C     * OUTPUT: CF      CRAY FLOATING POINT NUMBERS
C     * 
C     * FORMAT :
C     *           SIGN  EXPONENT  MANTISSA    UNUSED
C     * IEEE :     1      11        52
C     * CRAY :     1      15        48
 
      IMPLICIT INTEGER(A-Z)
      INTEGER IEEEF(1),CF(1)
      PARAMETER (CEXPBIAS=16384)   ! CRAY EXPONENT BIAS (40000(OCTAL))
      PARAMETER (IEXPBIAS=1023)    ! IEEE EXPONENT BIAS
      DIMENSION  ZERO(1)
      INTEGER    IZERO
      EQUIVALENCE (ZERO,IZERO)
      DATA ZERO/0.0E0/
C-----------------------------------------------------------------------------
      MBSE0 = 0
      MBSE63 = 63
      MASK1 = IBSET(MBSE0,MBSE63)
      MASK2 = IBITS(-1,0,11)
      MBITM1 = -1
      MBIT0 = 0
      MBIT52 = 52
      MASK3 = IBITS(MBITM1,MBIT0,MBIT52)
      MBSE0 = 0
      MBSE52 = 52
      IMPLIED=IBSET(MBSE0,MBSE52)

C     * SET SIGN BIT, EXPONENT AND MANTISSA IN ONE VECTOR LOOP

      DO 10 I=1,NF
        IF (IEEEF(I).NE.0) THEN
          MSHFM52 = -52
          MSHF48 = 48
          CF(I) = IOR(IOR(
     1            (IAND(IEEEF(I),MASK1))
     2              ,
     3            (ISHFT((IAND(ISHFT(IEEEF(I),MSHFM52),MASK2)-
     4               IEXPBIAS+CEXPBIAS+1),MSHF48) )         )
     5              ,
     6            (ISHFT((IAND(IEEEF(I),MASK3)+
     7               IMPLIED),-5) )
     8              )
        ELSE
          CF(I) = IZERO
        ENDIF
10    CONTINUE
 
      RETURN
      END
