      SUBROUTINE DECODR2(IEEEF,X)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * DEC 06/00 - F.MAJAESS - ENSURE 64-BIT IEEE "NEGATIVE ZERO" (IF ANY)
C     *                         CONVERTED INTO "POSITIVE ZERO" SINCE WITHOUT
C     *                         THIS FIX CONVERSION ROUTINES MAY PRODUCE WRONG
C     *                         VALUES (-2.0 IN THE CASE OF 32-BIT IEEE MODE).
C     * APR 15/92 - J. STACEY - REWRITE OF DECODR FUNCTION, CONVERTED TO
C     *                         SUBROUTINE CALL TO ALLOW READING OF
C     *                         64-BIT INTEGER CONSTANTS IN "IEEEF" EVEN
C     *                         ON 32-BIT MACHINES.
C     *
C     * PURPOSE: THIS ROUTINES PERFORMS THE INVERSE OPERATION TO
C     *          ENCODR2. A 64-BIT IEEE REAL*8 VALUE STORED IN "IEEEF"
C     *          IS CONVERTED INTO A NATIVE SINGLE PRECISION FLOATING
C     *          POINT VALUE.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER IEEEF(*)
C
C     * INTEGER REPRESENTATIONS FOR THE HIGH ORDER 32-BITS OF SQRT(2)
C     * ARE DIFFERENT ON IBM, CRAY AND IEEE MACHINES.  WE USE THIS TO
C     * TEST FOR THE EXACT TYPE OF MACHINE BEFORE CONVERTING TO THE
C     * STANDARD.
C
      PARAMETER  ( IAMIBM=1092001950)
      PARAMETER  (IAMCRAY=1073853700)
      PARAMETER  (IAMIEEE=1073127582)
      REAL*8     A,B
      INTEGER    IA(2),JB
      EQUIVALENCE (A,IA),(B,JB)
      LOGICAL    IBM,CRAY,IEEE,IM64BIT,INTEST2
      DIMENSION  DATUM(2)
      INTEGER    IDATUM(2)
      EQUIVALENCE (DATUM,IDATUM)
      COMMON /MACHTYP/ MACHINE,INTSIZE
C------------------------------------------------------------------------------
C     * USE THE STANDARD TRICK TO SEE IF WE HAVE 32- OR 64-BIT INTEGERS.
C
      IA(2) = 0
      B = 0.D0
      A          = DSQRT(2.0D0)
      JB         = IA(1)
      IF (A .EQ. B) THEN
        IM64BIT  = .TRUE.
        MSHFM32 = -32
        JB = ISHFT(JB,MSHFM32)
      ELSE
        IM64BIT  = .FALSE.
      END IF
C
C     * TEST FOR IBM, CRAY OR IEEE MACHINE.
C
      IBM        = JB .EQ. IAMIBM
      CRAY       = JB .EQ. IAMCRAY
      IEEE       = JB .EQ. IAMIEEE
C     TEST FOR IEEE ON LITTLE-ENDIAN 32-BIT INTEGER MACHINES
      IF(.NOT.IEEE.AND..NOT.IM64BIT) IEEE = IA(2) .EQ. IAMIEEE
      INTEST2   = INTSIZE .EQ. 2
C
C     * ENSURE "NEGATIVE ZERO" (IF ANY) CONVERTED
C     * INTO   "POSITIVE ZERO".
C
      IF(IBM .OR. CRAY .OR. IEEE ) THEN
        IF (IM64BIT) THEN
          MBSE0 = 0
          MBSE63 = 63
          MASKX = IBSET(MBSE0,MBSE63)
          MSHF1 = 1
          IF(IEEEF(1).EQ.MASKX) IEEEF(1)=ISHFT(MASKX,MSHF1)
        ELSE
          MBSE0 = 0
          MBSE31 = 31
          MASKX = IBSET(MBSE0,MBSE31)
          MSHF1 = 1
          IF(IEEEF(1).EQ.MASKX .AND. IEEEF(2).EQ.0 )
     1                          IEEEF(1)=ISHFT(MASKX,MSHF1)
        ENDIF
      ENDIF
C
C     * NOW BRANCH ON TYPE OF MACHINE.
C
      IF (IBM  .AND. IM64BIT) THEN
        IDATUM(1) = 0
        CALL BF1I64B(IEEEF,IDATUM,1)
        X       = DATUM(1)
        RETURN
      END IF

      IF (IBM .AND. .NOT.IM64BIT) THEN
        IF (.NOT.INTEST2) THEN !32' INTEGERS/32' REALS.
          IDATUM(1) = 0
          IDATUM(2) = 0
          CALL BF2I64B(IEEEF,IDATUM,1)
          X       = DATUM(1)
        ELSE                   !32' INTEGERS/64' REALS.
          IDATUM(1) = 0
          IDATUM(2) = 0
          CALL BF1I64B(IEEEF,IDATUM,1)
          X       = DATUM(1)
        END IF
        RETURN
      END IF

      IF (CRAY) THEN
        IDATUM(1) = 0
        CALL CF1I64C(IEEEF,IDATUM,1)
        X       = DATUM(1)
        RETURN
      END IF

      IF (IEEE .AND. IM64BIT) THEN
        IDATUM(1)     = IEEEF(1)
        X             = DATUM(1)
        RETURN
      END IF

      IF (IEEE .AND. .NOT.IM64BIT) THEN
        MBSE0 = 0
        MBSE31 = 31
        MASK1 = IBSET(MBSE0,MBSE31) !1 BIT MASK FOR SIGN BIT.
        MASK2 = IBITS(-1,0,8)     !8 BIT MASK FOR IEEE 32' EXPONENT.
        MASK3 = IBITS(-1,0,20)    !20 BIT MASK FOR IEEE 32' MANTISSA.
        MASK4 = IBITS(-1,0,3)     !3 BIT MASK FOR 3' OF IEEE MANTISSA.
        MASK5 = IBITS(-1,0,11)    !11 BIT MASK FOR IEEE 64' EXPONENT.
        IF (.NOT.INTEST2) THEN !INTEGERS ARE 32', REALS ARE 32'.
          IF (IEEEF(1).NE.0 .OR. IEEEF(2).NE.0) THEN
            MSHF23 = 23
            MSHF3 = 3
            MSHFM29 = -29
            IDATUM(1) = IOR(IOR(IOR(
     1                  IAND(IEEEF(1),MASK1),
     2              ISHFT(IAND(IBITS(IEEEF(1),20,11)-1023+127,MASK2),
     2              MSHF23)
     3                          ),
     4              ISHFT(IAND(IEEEF(1),MASK3),MSHF3)
     5                           ),
     6              IAND(ISHFT(IEEEF(2),MSHFM29),MASK4)
     7                    )
            X      = DATUM(1)
          ELSE
            X      = 0.0E0
          END IF
          IEXP = IBITS(IEEEF(1),20,11)-1023+127
          IF (IEXP .LT. 0) X = 0.0E0
          IF (IEXP .GT. MASK2) THEN
            WRITE(6,6050)
 6050       FORMAT('0 *** ERROR *** DECODR2: IEEE 32-BIT OVERFLOW.')
            CALL ABORT
          END IF
        ELSE                   !INTEGERS ARE 32', REALS ARE 64'.
          IDATUM(1) = IEEEF(1)
          IDATUM(2) = IEEEF(2)
          X         = DATUM(1)
        END IF
        RETURN
      END IF

C     * IF WE GET HERE THEN WE HAVE A FATAL ERROR (WE DON'T KNOW WHAT MACHINE
C     * WE'RE ON OR THE INTERNAL REPRESENTATION OF THE MAGIC CONSTANTS IS
C     * DIFFERENT.)

      WRITE(6,6100)
 6100 FORMAT('0 *** ERROR *** DECODR2: UNKNOWN MACHINE FORMAT.')
      CALL ABORT
      END
