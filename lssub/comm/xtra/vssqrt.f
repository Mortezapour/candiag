      SUBROUTINE VSSQRT(XOUT,XIN,LENI)
C
C     * AUG 24, 2017 - F. MAJAESS ROUTINE TO EMULATE IBM MASS
C     *                           VECTOR LIBRARY "VSSQRT" CALL
C     *                           ON OTHER PLATFORMS.
C
      INTEGER*4 LENI
      REAL*4 XOUT(LENI), XIN(LENI)
C--------------------------------------------------------------------
      DO N=1,LENI
        XOUT(N)=SQRT(XIN(N))
      ENDDO
C
      RETURN
      END
