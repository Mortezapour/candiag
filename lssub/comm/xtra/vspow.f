      SUBROUTINE VSPOW(ROUT,XIN,YIN,LENI)
C
C     * AUG 24, 2017 - F. MAJAESS ROUTINE TO EMULATE IBM MASS
C     *                           VECTOR LIBRARY "VSPOW" CALL
C     *                           ON OTHER PLATFORMS.
C
      INTEGER*4 LENI
      REAL*4 ROUT(LENI), XIN(LENI), YIN(LENI)
C--------------------------------------------------------------------
      DO N=1,LENI
        ROUT(N)=XIN(N)**YIN(N)
      ENDDO
C
      RETURN
      END
