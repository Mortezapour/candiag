      SUBROUTINE USPKD  (PACKED,NCHARS,UNPAKD,NCHMTB) 
C   IMSL ROUTINE NAME   - USPKD 
C 
C-----------------------------------------------------------------------
C 
C   COMPUTER            - CRAY/SINGLE 
C
C                       - FM. APR 03/93 - FIX "PACKED" DECLARATION.
C                       - ML. AUG 26/92 - REPLACE DECODE BY INTERNAL
C                                         READ. 
C   LATEST REVISION     - JUNE 1, 1982
C 
C   PURPOSE             - NUCLEUS CALLED BY IMSL ROUTINES THAT HAVE 
C                           CHARACTER STRING ARGUMENTS
C 
C   USAGE               - CALL USPKD  (PACKED,NCHARS,UNPAKD,NCHMTB) 
C 
C   ARGUMENTS    PACKED - CHARACTER STRING TO BE UNPACKED.(INPUT) 
C                NCHARS - LENGTH OF PACKED. (INPUT)  SEE REMARKS. 
C                UNPAKD - INTEGER ARRAY TO RECEIVE THE UNPACKED 
C                         REPRESENTATION OF THE STRING. (OUTPUT)
C                NCHMTB - NCHARS MINUS TRAILING BLANKS. (OUTPUT)
C 
C   PRECISION/HARDWARE  - SINGLE/ALL
C 
C   REQD. IMSL ROUTINES - NONE
C 
C   REMARKS  1.  USPKD UNPACKS A CHARACTER STRING INTO AN INTEGER ARRAY 
C                IN (A1) FORMAT.
C            2.  UP TO 129 CHARACTERS MAY BE USED.  ANY IN EXCESS OF
C                THAT ARE IGNORED.
C 
C   WARRANTY            - IMSL WARRANTS ONLY THAT IMSL TESTING HAS BEEN 
C                           APPLIED TO THIS CODE. NO OTHER WARRANTY,
C                           EXPRESSED OR IMPLIED, IS APPLICABLE.
C 
C-----------------------------------------------------------------------
C                                  SPECIFICATIONS FOR ARGUMENTS 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER            NC,NCHARS,NCHMTB 
C 
      CHARACTER*1 UNPAKD(*),IBLANK 
      CHARACTER          PACKED*129
      DATA               IBLANK /' '/ 
C                                  INITIALIZE NCHMTB
      NCHMTB = 0
C                                  RETURN IF NCHARS IS LE ZERO
      IF(NCHARS.LE.0) RETURN
C                                  SET NC=NUMBER OF CHARS TO BE DECODED 
      NC = MIN (129,NCHARS)
C     DECODE (NC,150,PACKED) (UNPAKD(I),I=1,NC) 
      READ(PACKED,150) (UNPAKD(I),I=1,NC)
  150 FORMAT (129A1)
C                                  CHECK UNPAKD ARRAY AND SET NCHMTB
C                                  BASED ON TRAILING BLANKS FOUND 
      DO 200 N = 1,NC 
         NN = NC - N + 1
         IF(UNPAKD(NN) .NE. IBLANK) GO TO 210 
  200 CONTINUE
  210 NCHMTB = NN 
      RETURN
      END 
