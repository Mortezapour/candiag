#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#  DESCRIPTION
# 
#  Create monthly averages and monthly zonal averages for age of air.
#    --- this modified version retrieves the age of air from the 
#          concentration of an SF6-like tracer with a specified
#          linearly increasing surface concentration
#
#  PARMSUB PARAMETERS
#
#    -- crate, delt, chem, coord, flabel, lat, lon, memory1, model1,
#       npg, plid, plv, pmax, pmin, run, stime, t1, t2, t3, t4.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- SS history files
#
#  CHANGELOG
#
#    2023-01-25: Converted to shell script and removed ztsave (D. Plummer)
#    2014-06-11: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-06-10: Add iesave option (Y. Jiao)
#    2011-11-29: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2011-08-09: Created from previous age-of-air diagnostic (D. Plummer)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ---- variables for selstep
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
#
#  ----  access model history files
#
.   spfiles.cdk
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select out required fields from model history
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   X3" |
              ccc select npaksp x3_se
    echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg x3_se x3_ge
#
#  ----  convert the tracer concentration to an age in years
#
    echo "SF6-AGE    $crate $delt" | ccc sf6-to-age x3_ge age_ge
    rm x3_se x3_ge npaksp
#
#  ---- interpolate on to constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl age_ge gslnsp age_gp
#
#  ----  create monthly and zonal averages
#
    timavg age_gp age_tav
#
    if [ "$gesave" = on ] ; then
      timavg age_ge age_etav
    fi
#
    zonavg age_tav age_ztav
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#
    echo "XSAVE.        SF6 STRATOSPHERIC AGE OF AIR - YEARS
NEWNAM.     AOA" > .aoasave_input_card
#
    if [ "$iesave" = on ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep age_ge age_ie input=ic.tstep_model
        release oldie
        access oldie ${flabel}ie      na
        xsave oldie age_ie newie input=.aoasave_input_card
        save   newie ${flabel}ie
        delete oldie                  na
        rm age_ie newie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        xsave oldie age_ge newie input=.aoasave_input_card
        save   newie ${flabel}ie
        delete oldie                  na
      fi
    fi
#
    if [ "$ipsave" = "on" ] ; then
      release oldip
      access oldip ${flabel}ip    na
      xsave  oldip age_gp newip input=.aoasave_input_card
      save   newip  ${flabel}ip
      delete oldip                 na
      rm newip
    fi
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta surfaces
#
      release oldxp
      access oldxp ${flabel}xp      na
      xsave  oldxp age_ztav newxp input=.aoasave_input_card
      save   newxp ${flabel}xp
      delete oldxp                  na
      rm age_ztav newxp
#
      release oldgp
      access oldgp ${flabel}gp      na
      xsave  oldgp age_tav newgp input=.aoasave_input_card
      save   newgp ${flabel}gp
      delete oldgp                  na
#
      if [ "$gesave" = "on" ] ; then
        release oldge
        access oldge ${flabel}ge      na
        xsave  oldge age_etav newge input=.aoasave_input_card
        save   newge ${flabel}ge
        delete oldge                  na
        rm age_etav newge
      fi
#
      rm newgp age_ge age_gp age_tav .aoasave_input_card
