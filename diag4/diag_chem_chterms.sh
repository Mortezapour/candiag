#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#  DESCRIPTION (original name: diag_chem_chterms_971p.sh)
# 
#  Create monthly averages and monthly zonal averages for accumulated
#  chemical diagnostics output from the model.
#  Specification of individual CHnn fields must match with what is
#  being output from the model.  This version assumes:
#  01) Ox chemical production
#  02) Ox chemical destruction
#  03) CH4 chemical destruction
#  04) OH number density
#  05) clear-sky J(O3 -> O1D)
#  06) clear-sky J(NO2)
#  07) all-sky J(O3 -> O1D)
#  08) all-sky J(NO2)
#  09) N2O5 hydrolysis gamma
#  10) sulphate aerosol SAD
#  11) alternate Ox production
#  12) alternate Ox destruction
#  13) 3-D lightning NOx emission
#
#  The OH mixing ratio (CH14) is calculated from the output OH number
#  density and the calculated background number density
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, flabel, lat, lon, memory1, model1, npg,
#       plid, plv, plv2, pmax, pmaxc, pmin, run, stime, r1, r2, r3,
#       r4.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#
#  CHANGELOG
#
#    2023-01-27: Converted to shell script and removed ztsave (D. Plummer)
#    2021-01-16: Harmonization with the set of fields output by the
#                 971p job and conversion of OH number density to
#                 mixing ratio with number density
#    2014-06-18: Creation of the deck aligned with the set of 
#                 accumulated chemical diagnostics developed for
#                 the ACCMIP runs using version 945a (D. Plummer)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ----  access model history files and previously calculated fields
#
.   spfiles.cdk
.   ggfiles.cdk
#
# -- background number density calculated in diag_chem_nd with
#    units of molec/cm^3
    access nd_ge ${flabel}_gend
#
#  ----  select the required fields from the model history
#
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME AVSP" |
       ccc select npakgg avgps
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS-9001 1000 NAME CH01 CH02 CH03 CH04" |
       ccc select npakgg CH01_ge CH02_ge CH03_ge CH04_ge
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS-9001 1000 NAME CH05 CH06 CH07 CH08" |
       ccc select npakgg CH05_ge CH06_ge CH07_ge CH08_ge
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS-9001 1000 NAME CH09 CH10 CH11 CH12" |
       ccc select npakgg CH09_ge CH10_ge CH11_ge CH12_ge
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS-9001 1000 NAME CH13" |
       ccc   select npakgg CH13_ge
#
# -- calculate the OH mixing ratio from the saved OH number density
    div CH04_ge nd_ge CH14_ge
#
#  ----  convert the accumulated surface pressure (Pa) to LN(PS) for
#        use by gsapl
#
    echo "XLIN......      0.01       0.0" | ccc xlin avgps avgps_1
    loge avgps_1 avgps_2
    echo "NEWNAM.... LNSP" | ccc newnam avgps_2 avglnsp
    rm avgps avgps_1 avgps_2
#
#  ----  interpolate onto constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
#
    gsapl CH01_ge avglnsp CH01_gp input=.gsapl_input_card
    gsapl CH02_ge avglnsp CH02_gp input=.gsapl_input_card
    gsapl CH03_ge avglnsp CH03_gp input=.gsapl_input_card
    gsapl CH04_ge avglnsp CH04_gp input=.gsapl_input_card
    gsapl CH05_ge avglnsp CH05_gp input=.gsapl_input_card
    gsapl CH06_ge avglnsp CH06_gp input=.gsapl_input_card
    gsapl CH07_ge avglnsp CH07_gp input=.gsapl_input_card
    gsapl CH08_ge avglnsp CH08_gp input=.gsapl_input_card
    gsapl CH09_ge avglnsp CH09_gp input=.gsapl_input_card
    gsapl CH10_ge avglnsp CH10_gp input=.gsapl_input_card
    gsapl CH11_ge avglnsp CH11_gp input=.gsapl_input_card
    gsapl CH12_ge avglnsp CH12_gp input=.gsapl_input_card
    gsapl CH13_ge avglnsp CH13_gp input=.gsapl_input_card
    gsapl CH14_ge avglnsp CH14_gp input=.gsapl_input_card
#
#  ----  create monthly and zonal averages
#
# -- monthly averages
#
    timavg CH01_gp  CH01_tav
    timavg CH02_gp  CH02_tav
    timavg CH03_gp  CH03_tav
    timavg CH04_gp  CH04_tav
    timavg CH05_gp  CH05_tav
    timavg CH06_gp  CH06_tav
    timavg CH07_gp  CH07_tav
    timavg CH08_gp  CH08_tav
    timavg CH09_gp  CH09_tav
    timavg CH10_gp  CH10_tav
    timavg CH11_gp  CH11_tav
    timavg CH12_gp  CH12_tav
    timavg CH13_gp  CH13_tav
    timavg CH14_gp  CH14_tav
#
    if [ "$gesave" = "on" ] ; then
      timavg CH01_ge  CH01_etav
      timavg CH02_ge  CH02_etav
      timavg CH03_ge  CH03_etav
      timavg CH04_ge  CH04_etav
      timavg CH05_ge  CH05_etav
      timavg CH06_ge  CH06_etav
      timavg CH07_ge  CH07_etav
      timavg CH08_ge  CH08_etav
      timavg CH09_ge  CH09_etav
      timavg CH10_ge  CH10_etav
      timavg CH11_ge  CH11_etav
      timavg CH12_ge  CH12_etav
      timavg CH13_ge  CH13_etav
      timavg CH14_ge  CH14_etav
    fi
    rm *_gp *_ge
#
# -- monthly zonal averages 
#
    zonavg CH01_tav   CH01_ztav
    zonavg CH02_tav   CH02_ztav
    zonavg CH03_tav   CH03_ztav
    zonavg CH04_tav   CH04_ztav
    zonavg CH05_tav   CH05_ztav
    zonavg CH06_tav   CH06_ztav
    zonavg CH07_tav   CH07_ztav
    zonavg CH08_tav   CH08_ztav
    zonavg CH09_tav   CH09_ztav
    zonavg CH10_tav   CH10_ztav
    zonavg CH11_tav   CH11_ztav
    zonavg CH12_tav   CH12_ztav
    zonavg CH13_tav   CH13_ztav
    zonavg CH14_tav   CH14_ztav
#
#  ---- save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta levels
#
echo "XSAVE.        OX CHEMICAL PRODUCTION - 1 OVER CM3 OVER SEC
NEWNAM.
XSAVE.        OX CHEMICAL DESTRUCTION - 1 OVER CM3 OVER SEC
NEWNAM.
XSAVE.        CH4 CHEMICAL DESTRUCTION - 1 OVER CM3 OVER SEC
NEWNAM.
XSAVE.        OH NUMDEN - 1 OVER CM3
NEWNAM.
XSAVE.        CLEAR-SKY O3-O1D PHOTOLYSIS - 1 OVER SEC
NEWNAM.
XSAVE.        CLEAR-SKY NO2 PHOTOLYSIS - 1 OVER SEC
NEWNAM.
XSAVE.        ALL-SKY O3-O1D PHOTOLYSI - 1 OVER SEC
NEWNAM.
XSAVE.        ALL-SKY NO2 PHOTOLYSIS - 1 OVER SEC
NEWNAM.
XSAVE.        N2O5 HYDROLYSIS GAMMA
NEWNAM.
XSAVE.        TROPOSPHERIC SULPHATE SAD - M2 OVER KG AIR
NEWNAM.
XSAVE.        ALT-OX CHEMICAL PRODUCTION - 1 OVER CM3 OVER SEC
NEWNAM.
XSAVE.        ALT-OX CHEMICAL DESTRUCTION - 1 OVER CM3 OVER SEC
NEWNAM.
XSAVE.        LIGHTNING NOX PRODUCTION - 1 OVER CM3 OVER SEC
NEWNAM.
XSAVE.        OH MIXING RATIO
NEWNAM.... CH14" > .chsave_input_card
#
    release oldxp
    access oldxp ${flabel}xp      na
    xsave  oldxp CH01_ztav CH02_ztav CH03_ztav CH04_ztav CH05_ztav CH06_ztav \
                 CH07_ztav CH08_ztav CH09_ztav CH10_ztav CH11_ztav CH12_ztav \
                 CH13_ztav CH14_ztav newxp input=.chsave_input_card
    save   newxp ${flabel}xp
    delete oldxp                  na
    rm *_ztav
#
    release oldgp
    access oldgp ${flabel}gp      na
    xsave  oldgp CH01_tav CH02_tav CH03_tav CH04_tav CH05_tav CH06_tav \
                 CH07_tav CH08_tav CH09_tav CH10_tav CH11_tav CH12_tav \
                 CH13_tav CH14_tav newgp input=.chsave_input_card
    save   newgp ${flabel}gp
    delete oldgp                  na
    rm *_tav
#
    if [ "$gesave" = on ] ; then
      access oldge ${flabel}ge      na
      xsave  oldge CH01_etav CH02_etav CH03_etav CH04_etav CH05_etav CH06_etav \
                   CH07_etav CH08_etav CH09_etav CH10_etav CH11_etav CH12_etav \
                   CH13_etav CH14_etav newge input=.chsave_input_card
      save   newge ${flabel}ge
      delete oldge                  na
      rm *_etav
    fi
