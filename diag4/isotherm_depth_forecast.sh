#!/bin/sh
#                                     BP June 2006

#
# Run loop for 12 months
#
    release all
    touch all
    for m in $months ; do
      if [ "$m" -ge "$mon_start" ] ; then
        year0=`expr $year + 0 | $AWK '{printf "%4d", $0}'`;
        access  gz mf_${modelh}_${year0}_m${m}_gz
      else
        year0=`expr $year + 1 | $AWK '{printf "%4d", $0}'`;
        access  gz mf_${modelh}_${year0}_m${m}_gz
      fi

      echo "XFIND     1   DATA DESCRIPTION
XFIND     1   OCEAN TEMPERATURE" | ccc xfind gz gridin tem3d
      release gz

      grido gridin beta darea dx dy dz
#
#   Compute a 2D field of the depth for the specified isotherm
#
      echo "THETA_DEPTH 20.0    " | ccc theta_depth dz beta tem3d p
      rm     tem3d
#
#   ---------------------------------- adjust labels and save results
#
      yeara=`expr ${year0} + ${year_offset} | $AWK '{printf "%4d", $0}'`
      echo "RELABL
RELABL             $yeara$m" | ccc relabl  p zz

      cat zz >> all
      rm  zz p
    done
#
# ------------------------------------save isotherm
#
    echo "XSAVE         OCEAN 20 C ISOTHERM DEPTH (M)
XSAVE" | ccc xsave old_dat all new_dat

# save
    access  old  ${atmos_file} na
    xappend old new_dat newa
    save    newa ${atmos_file}
    delete  old  na
    release newa all new_dat old_dat
