#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: rename tprhsc -> tprhsc_chem
#                             source the file of switches
#
#  DESCRIPTION (original name: diag_dyn_eta_971p.sh)
# 
#  Specialized deck to treat a standard set of meteorological
#    variables but output them as monthly averages on eta surfaces
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, flabel, lat, lon, memory1, model1, npg,
#       plid, plv, pmax, pmin, run, stime, t1, t2, t3, t4,
#       ztsave.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- SS and GS history files
#    -- _geshum, _gez and _get files of model level meteorological fields
#          calculated by geqtz.dk [now called pqtphirho.sh]
#    -- _gewe file of Eulerian vertical velocity on model levels calculated
#          by gpww.dk [now called gpww.sh]
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables
#    2022-12-30: Converted to shell script
#    2021-01-16: Added treatment of accumulated shortwave (TTPS) and 
#                longwave (TTPL) heating for a modified case of TPRHSC
#    2019-10-31: Added options under iesave to select a limited number of
#                  times to be saved, or to save all available times
#    2015-04-13: Modified to take advantage of calculation of many dynamical
#                  quantities on model levels by geqtz2 deck
#    2014-06-20: Created from accmip_met_diag by removing some of the
#                  specialized diagnostics like convective updraft
#                  mass flux (D. Plummer)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ---- variables for selstep
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

# Access required files, including previously calculated met fields on model
# levels by the geqtz2 and gpww decks.
# Note that omega (dp/dt) is output for the instantaneous fields.

    if [ "$datatype" = "specsig" ] ; then
.     spfiles.cdk
.     ggfiles.cdk

      if [ "$tprhsc_chem" = on ]; then
.       tdfiles.cdk
      fi

      access gstemp ${flabel}_get
      access gsshum ${flabel}_geshum
      access gsphi  ${flabel}_gez
      access gswe   ${flabel}_gewe
      access gsw    ${flabel}_gew

# Get the surface pressure field
      access gslnsp ${flabel}_gslnsp na

      if [ ! -s gslnsp ] ; then
        echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME LNSP" |
                ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat    0    1" | ccc cofagg sslnsp gslnsp
      fi

      echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME VORT  DIV" |
              ccc select npaksp ssvort ssdiv
      cwinds ssvort ssdiv ssbigu ssbigv
      echo "COFAGG.   $lon$lat    1$npg" | ccc cofagg ssbigu gsu
      echo "COFAGG.   $lon$lat    1$npg" | ccc cofagg ssbigv gsv
      rm ssbigu ssbigv

      if [ "$tprhsc_chem" = on ]; then
        echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME TTPS TTPL" |
                ccc select npaktd ttps ttpl
      fi

# Calculation of model layer thickness and mass using accumulated fields
# Also treat the convective updraft mass flux
      echo "SELECT.  STEPS  $g1 $g2 $g3 LEVS-9001 1000      AVSP" |
              ccc select npakgg acc_ps
      echo "SELECT.  STEPS  $g1 $g2 $g3 LEVS-9001 1000      AVTA CVUF" |
              ccc select npakgg acc_ta cmflux

# Convert accumulated surface pressure (Pa) to ln(PS) for use by gsapl
      echo "XLIN......      0.01       0.0" | ccc xlin acc_ps pswrk1
      loge pswrk1 pswrk2
      echo "NEWNAM.... LNSP" | ccc newnam pswrk2 acc_lnsp
      echo "NEWNAM.... TEMP" | ccc newnam acc_ta acc_temp

# Calculate layer thickness and mass
      echo "ACCGRIDC   HALF$lay$coord$topsig$plid" |
              ccc gridcalc_eta acc_temp acc_lnsp delz delm

# Instantaneous fields of surface pressure and cloud fraction
      if [ "$iesave" = on ] ; then
        expone gslnsp sfpr
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME  CLD" |
                ccc select npakgg scld
      fi

# Put the convective mass flux on to constant pressure surfaces using
# the accumulated surface pressure
      echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl cmflux acc_lnsp cmflux_gp

# Calculate monthly averages on model levels
      timavg cmflux_gp cmflux_tav

      if [ "$gesave" =  on ] ; then
        timavg pswrk1 acc_ps_tav
        timavg gstemp gstemp_etav
        timavg gsu    gsu_etav
        timavg gsv    gsv_etav
        timavg gswe   gswe_etav
        timavg gsphi  gsphi_etav
        timavg gsshum gsshum_etav
        timavg delz   delz_etav
        timavg delm   delm_etav
        timavg cmflux cmflux_etav
        if [ "$tprhsc_chem" = on ] ; then
          timavg ttps ttps_etav
          timavg ttpl ttpl_etav
        fi
      fi
      rm pswrk1 pswrk2

# Save instantaneous fields at a reduced frequency if ieslct=on,
# or at all available times otherwise

      if [ "$iesave" = on ] ; then
        if [ "$ieslct" = on ] ; then
          echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
          selstep     sfpr     sfpr_ie input=ic.tstep_model
          selstep     gstemp gstemp_ie input=ic.tstep_model
          selstep     gsu       gsu_ie input=ic.tstep_model
          selstep     gsv       gsv_ie input=ic.tstep_model
          selstep     gsw       gsw_ie input=ic.tstep_model
          selstep     gsphi   gsphi_ie input=ic.tstep_model
          selstep     gsshum gsshum_ie input=ic.tstep_model
          selstep     scld     scld_ie input=ic.tstep_model
          if [ "$tprhsc_chem" = on ]; then
            selstep     ttps     ttps_ie input=ic.tstep_model
            selstep     ttpl     ttpl_ie input=ic.tstep_model
          fi

          release oldie
          access oldie ${flabel}ie    na
          echo "XSAVE.        INSTANTANEOUS PS
NEWNAM.      PS
XSAVE.        T ON MODEL LEVELS
NEWNAM.
XSAVE.        U ON MODEL LEVELS
NEWNAM.
XSAVE.        V ON MODEL LEVELS
NEWNAM.
XSAVE.        W ON MODEL LEVELS - PA OVER SEC
NEWNAM.       W
XSAVE.        GEOPOTENTIAL ON MODEL LEVELS - M2 OVER SEC2
NEWNAM.
XSAVE.        Q ON MODEL LEVELS
NEWNAM.
XSAVE.        SCLD
NEWNAM." | ccc xsave oldie sfpr_ie gstemp_ie gsu_ie gsv_ie gsw_ie gsphi_ie \
                      gsshum_ie scld_ie new1
          if [ "$tprhsc_chem" = on ]; then
            echo "XSAVE.        TTPS ON MODEL LEVELS - K OVER SEC
NEWNAM.
XSAVE.        TTPL ON MODEL LEVELS - K OVER SEC
NEWNAM." | ccc xsave new1 ttps_ie ttpl_ie new2
            mv new2 new1
          fi
          save new1 ${flabel}ie
          delete oldie     na
        fi
#
        if [ "$ieslct" = off ] ; then
          release oldie
          access oldie ${flabel}ie    na
          echo "XSAVE.        INSTANTANEOUS PS
NEWNAM.      PS
XSAVE.        T ON MODEL LEVELS
NEWNAM.
XSAVE.        U ON MODEL LEVELS
NEWNAM.
XSAVE.        V ON MODEL LEVELS
NEWNAM.
XSAVE.        W ON MODEL LEVELS - PA OVER SEC
NEWNAM.       W
XSAVE.        GEOPOTENTIAL ON MODEL LEVELS - M2 OVER SEC2
NEWNAM.
XSAVE.        Q ON MODEL LEVELS
NEWNAM.
XSAVE.        SCLD
NEWNAM." | ccc xsave oldie sfpr gstemp gsu gsv gsw gsphi \
                        gsshum scld new1
          if [ "$tprhsc_chem" = on ]; then
            echo "XSAVE.        TTPS ON MODEL LEVELS - K OVER SEC
NEWNAM.
XSAVE.        TTPL ON MODEL LEVELS - K OVER SEC
NEWNAM." | ccc xsave new1 ttps ttpl new2
            mv new2 new1
          fi
          save new1 ${flabel}ie
          delete oldie     na
        fi
        rm new1
      fi
      rm acc_ps gstemp gsshum
#
# ------- save monthly averaged fields
#
      release oldgp
      access oldgp ${flabel}gp
      echo "+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        PS ACCUMULATED
NEWNAM.
XSAVE.        CONVECTIVE UPDRAFT MASS FLUX - KG OVER M2 OVER SEC
NEWNAM." | ccc xsave  oldgp acc_ps_tav cmflux_tav new1
      save   new1  ${flabel}gp
      delete oldgp
      rm cmflux_gp cmflux_tav new1
#
      if [ "$gesave" =  on ] ; then
        release oldge
        access oldge ${flabel}ge    na
        echo "XSAVE.        PS ACCUMULATED
NEWNAM.
XSAVE.        T ON MODEL LEVELS
NEWNAM.
XSAVE.        U ON MODEL LEVELS
NEWNAM.
XSAVE.        V ON MODEL LEVELS
NEWNAM.
XSAVE.        WE ON MODEL LEVELS - M OVER SEC
NEWNAM.      WE
XSAVE.        GEOPOTENTIAL ON MODEL LEVELS - M2 OVER SEC2
NEWNAM.
XSAVE.        Q ON MODEL LEVELS
NEWNAM.
XSAVE.        GRID CELL THICKNESS - M
NEWNAM.
XSAVE.        GRID CELL MASS - KG OVER M2
NEWNAM.
XSAVE.        CONVECTIVE UPDRAFT MASS FLUX - KG OVER M2 OVER SEC
NEWNAM." | ccc xsave oldge acc_ps_tav gstemp_etav gsu_etav gsv_etav gswe_etav \
                     gsphi_etav gsshum_etav delz_etav delm_etav \
                     cmflux_etav new2
#
        if [ "$tprhsc_chem" = on ] ; then
          echo "XSAVE.        TTPS ON MODEL LEVELS - K OVER SEC
NEWNAM.
XSAVE.        TTPL ON MODEL LEVELS - K OVER SEC
NEWNAM." | ccc xsave new2 ttps_etav ttpl_etav new3
          mv new3 new2
        fi
        save   new2  ${flabel}ge
        delete oldge     na
        rm new2
        rm acc_ps_tav gstemp_etav gsshum_etav
        rm delz_etav delm_etav cmflux_etav
      fi
#
      save delz ${flabel}_delz
      save delm ${flabel}_delm
    fi
