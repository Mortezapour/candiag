#!/bin/sh
#                                     BP June 2006
#
#---------------------------FIXED CONSTANTS ------
#rho="1025";             #density of sea water kg/m^3
#cp="3990";              #specific heat of water J/kg/deg C
#H="300";                #depth in m of water column
#k0="13632.5";           #k0=rho*cp/H  (J/m^3)
#1/H=0.003333333

#
# Run loop for 12 months
#
    release all
    touch all
    for m in $months ; do
      if [ "$m" -ge "$mon_start" ] ; then
        year0=`expr $year + 0 | $AWK '{printf "%4d", $0}'`;
        access  gz mf_${modelh}_${year0}_m${m}_gz
      else
        year0=`expr $year + 1 | $AWK '{printf "%4d", $0}'`;
        access  gz mf_${modelh}_${year0}_m${m}_gz
      fi

      echo "XFIND     1   DATA DESCRIPTION
XFIND     1   OCEAN TEMPERATURE" | ccc xfind gz gridin tem3d
      release gz

      grido gridin beta darea dx dy dz
      rm darea dx dy

      echo "XLIN              1.   -273.16" | ccc xlin tem3d x1
      mlt  x1 beta tem3d
      rm   x1

      echo " VZINTV            0      3000" | ccc vzintv tem3d dz x2
      echo " XLIN     0.00333333" | ccc xlin x2 hcon
      rm     tem3d x2
#
#   ---------------------------------- adjust labels and save results
#
      yeara=`expr ${year0} + ${year_offset} | $AWK '{printf "%4d", $0}'`
      echo "RELABL
RELABL             $yeara$m OHC" | ccc relabl hcon hcon1
      mv hcon1 hcon

      cat hcon >> all
      rm  hcon
    done
#
# ------------------------------------save heat content
#
    echo "XSAVE         OCEAN 300M DEPTH AVG HEAT CONTENT (DEG C)is
XSAVE" | ccc xsave old_dat all new_dat

# save
    access  old  ${atmos_file} na
    xappend old new_dat newa
    save    newa ${atmos_file}
    delete  old  na
    release newa all new_dat old_dat
