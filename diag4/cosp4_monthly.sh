#!/bin/sh
# jcl - Jan 23/19
#   added cloud tau vs ice radius and cloud tau vs liquid radius historgrams
#
# jcl - Sept 4/14
#   added code to handle extra cloud droplet number concentration
#   computed using MODIS fields and to convert all labels to uppercase
#   to avoid issues with Cview.
#
# jcl - Aug 22/14
#   added code to handle new diagnostic output produced by version 1.4
#   of COSP and new diagnostics related to cloud microphysics
#
# jcl - Feb. 16, 2012
#   added code to handle variable CHMK and fields for rain and snow when 
#   the radar is turned on.  Also added script to handle the old and new 
#   way to output ISCCP p-tau fields.
#
# jcl - Nov. 22, 2011 - Previous cosp2.dk.
#   added code to handle MSTZ which have more than 100 levels which is 
#   problematic for statsav
#
# jcl - Sept 19, 2011
#   added code to handle CERES fields along an orbital swath
# 
# jcl - May 16/11 
#   added code to handle MODIS output
#
# jcl - May 11/11
#   changed code to reflect new way to save some multi-level fields 
#   and MISR output.
#
# jcl - Mar 09/11
#   added code to properly weight, i.e., by cloud fraction.
#
# jcl - Feb 03/11
#   added code to handle effective radius and droplet number 
#   concentration for liquid clouds
#
#             cosp4_monthly            jcl - Apr 30/12 - jcl ----- cosp4_monthly
#   ---------------------------------- module to compute monthly mean fields 
#                                      related to output created by COSP
#                                      simulator.
#                                      The output from COSP is rather flexible
#                                      so deck has been set up to check for 
#                                      every possible field from COSP and if 
#                                      they are present generate diagnostics 
#                                      for them.
# -------------------------------------------------------------------------

.   spfiles.cdk
.   ggfiles.cdk

# ----------------------------------------------------- Possible input fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar

# CL1C,CL1M -> High liquid cloud fraction
# CL2C,CL2M -> Middle liquid cloud fraction
# CL3C,CL3M -> Low liquid cloud fraction
# CL4C,CL4M -> Total liquid cloud fraction
# CI1C,CI1M -> High ice cloud fraction
# CI2C,CI2M -> Middle ice cloud fraction
# CI3C,CI3M -> Low ice cloud fraction
# CI4C,CI4M -> Total ice cloud fraction
# CU1C,CU1M -> High undefined cloud fraction
# CU2C,CU2M -> Middle undefined cloud fraction
# CU3C,CU3M -> Low undefined cloud fraction
# CU4C,CU4M -> Total undefined cloud fraction
# CLPP,CLPM -> Liquid cloud amount profile
# CIPP,CIPM -> Ice cloud amount profile 
# CUPP,CUPM -> Undefined cloud amount profile
# CT1P,CT1M -> Total cloud temperature profile
# CT2P,CT2M -> Liquid cloud temperature profile
# CT3P,CT3M -> Ice cloud temperature profile
# CT4P,CT4M -> Undefined cloud temperature profile

# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE*        -> Radar reflectivity/height histogram
# CCTC, CCTM -> Total cloud fraction using lidar and radar
# CHMK       -> Mask of land topography (needed when interpolating data to height grid)
# SMIX, SREF -> Snow mixing ratio and snow effective radius
# RMIX, RREF -> Rain mixing ratio and rain effective radius
#
# CERES-like fields
#
# CCCF -> Cloud fraction (liquid + ice)
# CCNT -> Cloud occurence (liquid + ice)
# CCTP -> Cloud top pressure 
# CCCT -> Cloud visible optical thickness (linear average)
# CCLT -> Cloud visible optical thickness (log average)
# CCFL -> Liquid cloud fraction
# CCFI -> Ice cloud fraction
# CLWP -> Cloud liquid water path in each pressure range
# CIWP -> Cloud ice water path in each pressure range
# CNLC -> Occurence of liquid cloud in each pressure range
# CNIC -> Occurence of ice cloud in each pressure range
# CREL -> Cloud top effective radius for liquid clouds
# CCDD -> Cloud droplet number concentrations for liquid clouds
# CCFM -> Liquid cloud fraction for microphysical parameters
# CNLC -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES-like cloud fields along orbit
#
# CS01 -> Cloud fraction (liquid + ice)
# CS02 -> Cloud occurence (liquid + ice)
# CS03 -> Cloud top pressure 
# CS04 -> Cloud visible optical thickness (linear average)
# CS05 -> Cloud visible optical thickness (log average)
# CS06 -> Liquid cloud fraction
# CS07 -> Ice cloud fraction
# CS08 -> Cloud liquid water path in each pressure range
# CS09 -> Cloud ice water path in each pressure range
# CS10 -> Occurence of liquid cloud in each pressure range
# CS11 -> Occurence of ice cloud in each pressure range
# CS12 -> Cloud top effective radius for liquid clouds
# CS13 -> Cloud droplet number concentrations for liquid clouds
# CS14 -> Liquid cloud fraction for microphysical parameters
# CS15 -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES radiative fluxes by cloud type along orbit
#
# ORBA -> Sum of occurrences of orbit in gridboxes (total orbit)
# ORBS -> Sum of occurrences of orbit in gridboxes (sunlit orbit)
# PWTC -> Precipitable water
# GTC  -> Surface temperature
# GCC  -> Surface type
# CSZC -> Cosine of solar zenith angle
# CDSW -> Upward solar flux at TOA for each cloud type
# CULW -> Upward thermal flux at TOA for each cloud type
# CFSO -> Downward solar flux at the TOA (for each cloud type for convience)
# CLRF -> Occurrence of clear sky
#
# MISR related fields
#
# MSTC -> Total cloud fraction from MISR
# MSMZ -> Cloud-mean cloud top height
# MSDT -> Occurence of model layers in particular MISR height ranges
# MSTZ -> Cloud top height versus optical thickness 
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
# MDTI -> Cloud optical thikness versus ice effective radius histogram
# MDTL -> Cloud optical thikness versus liquid effective radius histogram
# MLN1 -> Cloud droplet number coincentration from algorithm
# MLN2 -> Cloud droplet number coincentration from model
# MDLQ -> Warm (>273K) clouds fore which liquid cloud particle retrieved
#         by MODIS simulator
#
# ----------------------------------------------------- Possible output fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar
# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE*  -> Radar reflectivity/height histogram
# CHMK -> Mask of land topography (needed when interpolating data to height grid)
#
# CERES-like fields
#
# CCCF -> Cloud fraction (liquid + ice)
# CCNT -> Cloud occurence (liquid + ice)
# CCTP -> Cloud top pressure 
# CCCT -> Cloud visible optical thickness (linear average)
# CCLT -> Cloud visible optical thickness (log average)
# CCFL -> Liquid cloud fraction
# CCFI -> Ice cloud fraction
# CLWP -> Cloud liquid water path in each pressure range
# CIWP -> Cloud ice water path in each pressure range
# CNLC -> Occurence of liquid cloud in each pressure range
# CNIC -> Occurence of ice cloud in each pressure range
# CREL -> Cloud top effective radius for liquid clouds
# CCDD -> Cloud droplet number concentrations for liquid clouds
#
# CERES-like cloud fields along orbit
#
# CS01 -> Cloud fraction (liquid + ice)
# CS02 -> Cloud occurence (liquid + ice)
# CS03 -> Cloud top pressure 
# CS04 -> Cloud visible optical thickness (linear average)
# CS05 -> Cloud visible optical thickness (log average)
# CS06 -> Liquid cloud fraction
# CS07 -> Ice cloud fraction
# CS08 -> Cloud liquid water path in each pressure range
# CS09 -> Cloud ice water path in each pressure range
# CS10 -> Occurence of liquid cloud in each pressure range
# CS11 -> Occurence of ice cloud in each pressure range
# CS12 -> Cloud top effective radius for liquid clouds
# CS13 -> Cloud droplet number concentrations for liquid clouds
# CS14 -> Liquid cloud fraction for microphysical parameters
# CS15 -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES radiative fluxes by cloud type along orbit
#
# ORBA -> Sum of occurrences of orbit in gridboxes (total orbit)
# ORBS -> Sum of occurrences of orbit in gridboxes (sunlit orbit)
# PWTC -> Precipitable water
# GTC  -> Surface temperature
# GCC  -> Surface type
# CSZC -> Cosine of solar zenith angle
# CDSW -> Upward solar flux at TOA for each cloud type
# CULW -> Upward thermal flux at TOA for each cloud type
# CFSO -> Downward solar flux at the TOA (for each cloud type for convience)
# CLRF -> Occurrence of clear sky
#
# MISR related fields
#
# MSTC -> Total cloud fraction from MISR
# MSMZ -> Cloud-mean cloud top height
# MSDT -> Occurence of model layers in particular MISR height ranges
# MSTZ -> Cloud top height versus optical thickness 
#      -> This is saved as MST1 and MST2 to avoid problems with statsav
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
# MDTI -> Cloud optical thikness versus ice effective radius histogram
# MDTL -> Cloud optical thikness versus liquid effective radius histogram
# MLN1 -> Cloud droplet number coincentration from algorithm
# MLN2 -> Cloud droplet number coincentration from model
# MDLQ -> Warm (>273K) clouds fore which liquid cloud particle retrieved
#         by MODIS simulator
#
# ---------------------------------- Notes
# There are two rather important issues to take note of for this deck
#
# 1. It only processes variables that exist in the "gs" file.  Basically, the error
#    checking is turned off, we attempt to read in the variable.
#    Therefore if the variable is not present,
#    OR the read does not work properly, the script continues onward without the variable.
#    Therefore, if you expected a particular field in the "gp" file and it 
#    is not there check first if it is present in the "gs" file and the correct
#    options were set in the GCM submission job.
#
# 2. Most of the variables need to be weighted to get the proper monthly, seasonal, etc.
#    means.  Since the pooling deck have trouble with missing values the next step is
#    to average all of the weighted mean denominators and numerators.  Potentially adds
#    a lot of extra output to the diagnostic files but is necessary to get the proper 
#    means at averaging times greater than a month.
# 
# 3. When averaging the profile data, and when it is interpolated onto the special height grid, 
#    you will need to account for the points that wind up below the height of the land.
#
# Manipulations needed to get the proper weighted means using the fields from the diagnostic
# output
#
# ICCA = ICCA/ICCF (cloud fraction weighted)
# ICCT = ICCT/ICCF (cloud fraction weighted)
# ICCP = ICCP/ICCF (cloud fraction weighted)
# ICCF = ICCF/SUNL (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
# ICTP = ICTP/SUNL  (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
#
# CLHC = CLHC/CLHM (weighted by "valid observation", i.e., backscatter above threshold)
# CLMC = CLMC/CLMM (weighted by "valid observation", i.e., backscatter above threshold)
# CLLC = CLLC/CLLM (weighted by "valid observation", i.e., backscatter above threshold)
# CLTC = CLTC/CLTM (weighted by "valid observation", i.e., backscatter above threshold)
# CLCP = CLCP/CLCM (weighted by "valid observation", i.e., backscatter above threshold)
# CCCP = CCCP/CCCM (weighted by "valid observation", i.e., backscatter above threshold)
# CL1C = CL1C/CL1M (weighted by "valid observation", i.e., backscatter above threshold)
# CL2C = CL2C/CL2M (weighted by "valid observation", i.e., backscatter above threshold)
# CL3C = CL3C/CL3M (weighted by "valid observation", i.e., backscatter above threshold)
# CL4C = CL4C/CL4M (weighted by "valid observation", i.e., backscatter above threshold)
# CI1C = CI1C/CI1M (weighted by "valid observation", i.e., backscatter above threshold)
# CI2C = CI2C/CI2M (weighted by "valid observation", i.e., backscatter above threshold)
# CI3C = CI3C/CI3M (weighted by "valid observation", i.e., backscatter above threshold)
# CI4C = CI4C/CI4M (weighted by "valid observation", i.e., backscatter above threshold)
# CU1C = CU1C/CU1M (weighted by "valid observation", i.e., backscatter above threshold)
# CU2C = CU2C/CU2M (weighted by "valid observation", i.e., backscatter above threshold)
# CU3C = CU3C/CU3M (weighted by "valid observation", i.e., backscatter above threshold)
# CU4C = CU4C/CU4M (weighted by "valid observation", i.e., backscatter above threshold)
# CLPP = CLPP/CLPM (weighted by "valid observation", i.e., backscatter above threshold)
# CIPP = CIPP/CIPM (weighted by "valid observation", i.e., backscatter above threshold)
# CUPP = CUPP/CUPM (weighted by "valid observation", i.e., backscatter above threshold)
# CT1P = CT1P/CT1M (weighted by "valid observation", i.e., backscatter above threshold)
# CT2P = CT2P/CT2M (weighted by "valid observation", i.e., backscatter above threshold)
# CT3P = CT3P/CT3M (weighted by "valid observation", i.e., backscatter above threshold)
# CT4P = CT4P/CT4M (weighted by "valid observation", i.e., backscatter above threshold)
#
# PL01 = PL01/ML01 (weighted by valid surface type, i.e., not over land)
# PL02 = PL02/ML02 (weighted by valid surface type, i.e., not over land)
# PL03 = PL03/ML03 (weighted by valid surface type, i.e., not over land)
# PL04 = PL04/ML04 (weighted by valid surface type, i.e., not over land)
# PL05 = PL05/ML05 (weighted by valid surface type, i.e., not over land)
#
# The topography mask (CHMK) will be needed when averaging the profiles over regions, 
# i.e., zonally, when the profile has been interpolated onto the heights above mean
# sea level.
#
# For the CERES-like fields we assume that things are properly weighted, i.e., they use
# cloud fraction weighting, as needed.
#
# CCCF = CCCF/CCNT
# CCFL = CCFL/CNLC
# CCFI = CCFI/CNIC
# CCTP = CCTP/CCCF
# CCCT = CCCT/CCCF
# CCLT = CCLT/CCCF
# CLWP = CLWP/CCFL
# CIWP = CIWP/CCFI
# CREL = CREL/CCFM
# CCDD = CCDD/CCFM
# CCFM = CCFM/CNLM
#
# For MISR we need
#
# MSTC = MSTC/SUNL
# MSMZ = MSMZ/MSTC
# MSTZ = (Join up MST1 and MST2) and then MSTZ/SUNL
# MSDT = MSDT/SUNL
#
# For MODIS we need
#
# MDTC = MDTC/SUNL
# MDWC = MDWC/SUNL
# MDIC = MDIC/SUNL
# MDHC = MDHC/SUNL
# MDMC = MDMC/SUNL
# MDLC = MDLC/SUNL
# MDTT = MDTT/MDTC
# MDWT = MDWT/MDWC
# MDIT = MDIT/MDIC
# MDGT = MDGT/MDTC
# MDGW = MDGW/MDWC
# MDGI = MDGI/MDIC
# MDRE = MDRE/MDWC
# MDRI = MDRI/MDIC
# MDPT = MDPT/MDTC
# MDWP = MDWP/MDWC
# MDIP = MDIP/MDIC
# MDTP = MDTP/SUNL
# MDTI = MDTI/SUNL
# MDTL = MDTL/SUNL
# MLN1 = MLN1/MDLQ
# MLN2 = MLN2/MDLQ
# MDLQ = MDLQ/SUNL
#
# Variables to hold fields for output
      vars=""

# ----------------------------------- Select all of the ISCCP related fields at once
    varnames="icca icct iccf iccp ictp sunl"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# I initially saved the histograms over cloud top pressure and optical thickness into separate 
# variables.  Try to select these here to support old implementations, i.e., that for CMIP5.
# If they are not present then this section will do nothing.

    varnames="it01 it02 it03 it04 it05 it06 it07 it08 it09 it10 it11 it12 it13 it14"
    varnames="$varnames it15 it16 it17 it18 it19 it20 it21 it22 it23 it24 it25 it26 it27 it28"
    varnames="$varnames it29 it30 it31 it32 it33 it34 it35 it36 it37 it38 it39 it40 it41 it42"
    varnames="$varnames it43 it44 it45 it46 it47 it48 it49"

    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Select CALIPSO and CloudSat cloud fraction fields at once

    varnames="clhc clhm clmc clmm cllc cllm cltc cltm cctm clcp clcm cccp cccm"
    varnames="${varnames} cl1c cl2c cl3c cl4c cl1m cl2m cl3m cl4m"
    varnames="${varnames} ci1c ci2c ci3c ci4c ci1m ci2m ci3m ci4m"
    varnames="${varnames} cu1c cu2c cu3c cu4c cu1m cu2m cu3m cu4m"
    varnames="${varnames} clpp cipp cupp clpm cipm cupm"
    varnames="${varnames} ct1p ct2p ct3p ct4p ct1m ct2m ct3m ct4m"

    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Backscatter/height histogram for CALIPSO
    varnames="sr01 sr02 sr03 sr04 sr05 sr06 sr07 sr08 sr09 sr10 sr11 sr12 sr13 sr14 sr15"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Reflectances for PARASOL
    varnames="pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Radar reflectivity/height histogram for CloudSat
    varnames="ze01 ze02 ze03 ze04 ze05 ze06 ze07 ze08 ze09 ze10 ze11 ze12 ze13 ze14 ze15"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Select all of the CERES related fields at once

    varnames="cccf cctp ccct cclt ccnt clwp ciwp ccfl ccfi cnlc cnic crel ccdd ccfm cnlm"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

  
# ----------------------------------- Select all of the CERES related fields along the swath at once

    varnames="cs01 cs02 cs03 cs04 cs05 cs06 cs07 cs08 cs09 cs10 cs11 cs12 cs13 cs14 cs15"
    varnames="$varnames orba orbs pwtc gtc gcc cszc cdsw culw cfso clrf"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Select all of the MISR related fields at once
    varnames="mstc msmz msdt"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# Special case for MSTZ
    echo "C*SELECT   STEP         0 999999999    1     -9001   99 NAME MSTZ" | ccc select npakgg MST1 || true
    echo "C*SELECT   STEP         0 999999999    1       10099999 NAME MSTZ" | ccc select npakgg MST2 || true

    for ivar in "MST1" "MST2" ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Select all of the MODIS related fields at once
    varnames="mdtc mdwc mdic mdhc mdmc mdlc mdtt mdwt mdit mdgt mdgw mdgi"
    varnames="$varnames mdre mdri mdpt mdwp mdip mdtp mdti mdtl mln1 mln2 mdlq"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# ----------------------------------- Select misc fields used by COSP but not computed in COSP
    varnames="chmk smix sref rmix rref"
    varnames_u=`echo ${varnames} | tr 'a-z' 'A-Z'`

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames_u} || true

    for ivar in ${varnames_u} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

    echo $vars

# Only try to save the variables if there is something to save, i.e., $vars is not empty

      if [ -n "$vars" ]; then
# ----------------------------------- Compute the statistics and save the fields to a file

        statsav ${vars} new_gp new_xp off
        rm ${vars}

#  ---------------------------------- save results.

        access oldgp ${flabel}gp na
        xjoin oldgp new_gp newgp
        save newgp ${flabel}gp na
        delete oldgp na
        
#    if [ "$rcm" != "on" ] ; then
#      access oldxp ${flabel}xp
#      xjoin oldxp new_xp newxp
#      save newxp ${flabel}xp
#      delete oldxp
#    fi
      fi
