#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
#
#  DESCRIPTION (original name: diag_dyn_daily_971p.sh)
#
#  Select out once-daily data (instantaneous) on constant pressure surfaces 
#
#  PARMSUB PARAMETERS
#
#    -- flabel, delt, lat, lon, npg, r1, r2, r3
#
#  PREREQUISITES
#
#    -- instantaneous fields of:
#        - dynamical fields _gpu, _gpv,... files created by gpint.dk
#        - vertical velocity from gpww.dk
#        - potential vorticity from Ertelpv1
#        - residual circulation and EP-flux from epf_total
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables
#    2023-01-05: Converted to script (D. Plummer)
#    2021-02-16: Removed some of the instantaneos radiation fields
#                 because only the outputs averaged over ISRAD were
#                 included in the current job
#    2014-06-20: Created from accmip_met_diag by removing some of the
#                 specialized diagnostics like convective updraft
#                 mass flux (D. Plummer)
# -----------------------------------------------------------------------------

#
#  ---- number of timesteps in one day used for selection of once daily output
    dyfrq=`echo $delt | awk '{printf "%04d",86400/$1}'`
#
# ---- access history files and pre-computed fields of variables on
#      constant pressure surfaces
#
.   spfiles.cdk
.   ggfiles.cdk
#
#   ---- calculated by gpint
    access u ${flabel}_gpu
    access v ${flabel}_gpv
    access t ${flabel}_gpt
    access z ${flabel}_gpz
#
#   ---- calculated by gpww
    access w ${flabel}_gpwe
#
#   ---- calculated by Ertelpv1
    access pv ${flabel}_gthpv
#
#   ---- calculated by epf_total
    access vres ${flabel}_gpvres
    access wres ${flabel}_gpwres
    access epfy ${flabel}_gpepfy
    access epfz ${flabel}_gpepfz
    access epfd ${flabel}_gpepfd
#
# ---- select daily fields on requested pressure levels
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   10   10 NAME    U" |
       ccc select u u_10
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   50   50 NAME    U" |
       ccc select u u_50
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  100  100 NAME    U" |
       ccc select u u_100
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  500  500 NAME    U" |
       ccc select u u_500
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS 1000 1000 NAME    U" |
       ccc select u u_1000
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   10   10 NAME    V" |
       ccc select v v_10
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   50   50 NAME    V" |
       ccc select v v_50
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  100  100 NAME    V" |
       ccc select v v_100
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  500  500 NAME    V" |
      ccc select v v_500
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS 1000 1000 NAME    V" |
       ccc select v v_1000
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   50   50 NAME   WE" |
       ccc select w w_50
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   10   10 NAME TEMP" |
       ccc select t t_10
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   30   30 NAME TEMP" |
       ccc select t t_30
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   50   50 NAME TEMP" |
       ccc select t t_50
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   70   70 NAME TEMP" |
       ccc select t t_70
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  100  100 NAME TEMP" |
       ccc select t t_100
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  500  500 NAME TEMP" |
       ccc select t t_500
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS 1000 1000 NAME TEMP" |
       ccc select t t_1000
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   10   10 NAME  PHI" |
       ccc select z z_10
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS   50   50 NAME  PHI" |
       ccc select z z_50
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  100  100 NAME  PHI" |
       ccc select z z_100
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  500  500 NAME  PHI" |
       ccc select z z_500
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS 1000 1000 NAME  PHI" |
       ccc select z z_1000
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  840  840 NAME   PV" |
       ccc select pv pv_840k
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS  480  480 NAME   PV" |
       ccc select pv pv_480k
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS 000000000 999999999 $dyfrq LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    else
      echo "SELECT    STEPS 000000000 999999999 $dyfrq LEVS    1    1 NAME LNSP" |
         ccc select gslnsp work1
      mv work1 gslnsp
    fi
#
# -- surface pressure to Pa
    expone gslnsp sfpr
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS    1    1 NAME   ST   GT" |
       ccc select npakgg ta_srf tg_srf
    echo "SELECT.   STEPS $r1 $r2 $r3 LEVS    1    1 NAME STMX STMN" |
       ccc select npakgg stmx stmn
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS    1    1 NAME  SNO SICN" |
       ccc select npakgg sno sicn
#
# ---- save instantaneous fields
#
    release oldip
    access oldip ${flabel}ip        na
    echo "XSAVE.        U AT 10 HPA
NEWNAM.
XSAVE.        U AT 50 HPA
NEWNAM.
XSAVE.        U AT 100 HPA
NEWNAM.
XSAVE.        U AT 500 HPA
NEWNAM.
XSAVE.        U AT 1000 HPA
NEWNAM.
XSAVE.        V AT 10 HPA
NEWNAM.
XSAVE.        V AT 50 HPA
NEWNAM.
XSAVE.        V AT 100 HPA
NEWNAM.
XSAVE.        V AT 500 HPA
NEWNAM.
XSAVE.        V AT 1000 HPA
NEWNAM.
XSAVE.        WE AT 50 HPA
NEWNAM.
XSAVE.        T AT 10 HPA
NEWNAM.
XSAVE.        T AT 30 HPA
NEWNAM.
XSAVE.        T AT 50 HPA
NEWNAM.
XSAVE.        T AT 70 HPA
NEWNAM.
XSAVE.        T AT 100 HPA
NEWNAM.
XSAVE.        T AT 500 HPA
NEWNAM.
XSAVE.        T AT 1000 HPA
NEWNAM.
XSAVE.        GZ AT 10 HPA
NEWNAM.
XSAVE.        GZ AT 50 HPA
NEWNAM.
XSAVE.        GZ AT 100 HPA
NEWNAM.
XSAVE.        GZ AT 500 HPA
NEWNAM.
XSAVE.        GZ AT 1000 HPA
NEWNAM.
XSAVE.        ERTELS PV ON 840K
NEWNAM.
XSAVE.        ERTELS PV ON 480K
NEWNAM.
XSAVE.        INSTANTANEOUS PS - HPA
NEWNAM.
XSAVE.        INSTANTANEOUS SAT
NEWNAM.
XSAVE.        INSTANTANEOUS GT
NEWNAM.
XSAVE.        DAILY MAXIMUM SAT
NEWNAM.
XSAVE.        DAILY MINIMUM SAT
NEWNAM.
XSAVE.        INSTANTANEOUS SNOW MASS
NEWNAM.
XSAVE.        INSTANTANEOUS SEA-ICE FRACTION
NEWNAM." | ccc xsave oldip u_10 u_50 u_100 u_500 u_1000 v_10 v_50 v_100 v_500 v_1000 \
                     w_50 t_10 t_30 t_50 t_70 t_100 t_500 t_1000 \
                     z_10 z_50 z_100 z_500 z_1000 pv_840k pv_480k sfpr \
                     ta_srf tg_srf stmx stmn sno sicn new1
    save new1 ${flabel}ip
    delete oldip              na
#
    rm -f sicn sfpr gslnsp sslnsp
    rm -f dss_clr prir sno stmn stmx tg_srf ta_srf
    rm -f u_* v_* w_* t_* z_* pv pv_*
#
# ---- calculated daily instantaneous cross sections
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME    U" | ccc select u u_dly
    zonavg u_dly u_dzav
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME TEMP" | ccc select t t_dly
    zonavg t_dly t_dzav
#
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME  PHI" | ccc select z z_dly
    zonavg z_dly z_dzav

    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME VRES" | ccc select vres vres_dly
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME WRES" | ccc select wres wres_dly
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME EPFY" | ccc select epfy epfy_dly
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME EPFZ" | ccc select epfz epfz_dly
    echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME EPFD" | ccc select epfd epfd_dly
#
# ---- save cross sections
#
    access oldix ${flabel}ix        na
    echo "XSAVE.        INSTANTANEOUS ZONAL AVG U
NEWNAM.
XSAVE.        INSTANTANEOUS ZONAL AVG T
NEWNAM.
XSAVE.        INSTANTANEOUS ZONAL AVG GZ
NEWNAM.
XSAVE.        INSTANTANEOUS VRES
NEWNAM.
XSAVE.        INSTANTANEOUS WRES
NEWNAM.
XSAVE.        INSTANTANEOUS EPFY
NEWNAM.
XSAVE.        INSTANTANEOUS EPFZ
NEWNAM.
XSAVE.        INSTANTANEOUS EPFD
NEWNAM." | ccc xsave oldix u_dzav t_dzav z_dzav vres_dly wres_dly epfy_dly \
                     epfz_dly epfd_dly new2
      save new2 ${flabel}ix
      delete oldix                  na

      rm -f u u_* v v_* w w_* t t_* z z_* pv pv_*
      rm -f vres vres_* wres wres_* epfy epfy_* epfz epfz_* epfd epfd_*
