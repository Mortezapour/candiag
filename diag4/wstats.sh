#!/bin/sh
#                  wstats              gjb,ec,dl. jan 24/01 - sk.
#   ---------------------------------- calculate omega statistics.
#
#   sk: re-write for new timavg/xsave, phase out xtrans.
#
      if [ "$wxstats" != "on" ]; then
         echo "Deck gpw.dk is not executed when \\\$wxstats is not 'on'."
         exit 0
      fi

      access w ${flabel}_gpw

      if [ "$datatype" = "specsig" ]; then
	echo "DIFF       OMEG $t1 $t2 $delt" | ccc dif w dwdt
      else
	echo "DIFF       OMEG $t1 $t2 $delt       1.0" | ccc dif w dwdt
      fi
      timavg w tw wp twp2

#   ---------------------------------- xsave
      echo "XSAVE.        W
NEWNAM.
XSAVE.        DW/DT
NEWNAM.      W.
XSAVE.        W\"W\"
NEWNAM.    W\"W\"" | ccc xsave new_gp tw dwdt twp2 new.
      mv new. new_gp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp
