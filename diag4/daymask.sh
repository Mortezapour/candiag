#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: replace $daylev with -100
#                             source the file of switches
#
#  DESCRIPTION
# 
#  Compute day/night mask on pressure surfaces.
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, daylev, days, flabel, memory1, model1, plid,
#       plv, run, stime, t1, t2, t3.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#
#  CHANGELOG
#
#    2023-01-08: Converted to shell script, removed ztsave and the logic
#                to support running chemistry on a subset of model levels
#                controlled by trchem (D. Plummer)
#    2014-08-06: Include changes for CMAM20 high-frequency output and
#                standardize from gp6save to ipsave flag
#    2008-04-24: Minor clean-up to reduce size of log-file (D. Plummer)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-12: Added $chem flag to daymask and number density decks
#                (A.Jonsson)
#    2007-10-??: Mask extended from chemistry domain to full domain
#                (A.Jonsson)
#    2007-10-??: DNMASK input card modified to use $daylev parmsub
#                parameter for vertical level choice (A.Jonsson)
#    2007-03-??: Original version from S. Beagley
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#  ----  access model history files
#
.   spfiles.cdk
.   ggfiles.cdk

#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat    0    1" | ccc cofagg sslnsp gslnsp
        rm npaksp sslnsp
      fi
    fi
#
#  ----  get the target chemical species - one that varies strongly in concentration
#        between day and night - currently targetting O(1D)
    echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000        O1" |
       ccc select npakgg o1d_ge
#
#  ----  interpolate on to constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl o1d_ge gslnsp o1d_gp
#
# Use proxy species O(1D) to create day/night mask
# $daylev is the model level that is used to calculate the day/night mask,
# one mask for all levels. This is now hardcoded as -100
#
    #echo "DN_MASK...${daylev}" | ccc dnmask o1d_gp day_gp
    echo "DN_MASK... -100" | ccc dnmask o1d_gp day_gp
#
    rm npakgg o1d_gp o1d_ge
#
#  ----  save the instantaneous day/night mask
    save day_gp ${flabel}_gpday
    rm day_gp
