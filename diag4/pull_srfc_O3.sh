#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original:  define the station list
#
#  DESCRIPTION
# 
#  Pulls out instantaneous values of ozone at a set of pre-defined
#  station locations.  This particular version takes advantage of a
#  special saving of ozone in the bottom-most model layer every hour
#  to create high-frequency station data.  For mountain-top sites that
#  sample air from the free troposphere, ozone is pulled from the full
#  3-D fields saved at a much lower frequency.
#
#  PARMSUB PARAMETERS
#
#    -- stime, memory1, model1, flabel, lat, lon, pmax, pmin, 
#       t1, t2, t3, t4, co_cmdl_stlist, delt
#
#  PREREQUISITES
#
#    -- SS history files
#
#  CHANGELOG
#
#    2023-11-23: Save output as individual months to avoid problems when
#                 diagnostics are done in parallel (D. Plummer)
#    2023-01-30: Converted to shell script and removed the logic to support
#                 running chemistry on a subset of model levels (D. Plummer)
#    2015-06-26: Name of surface ozone field changed to reflect
#                change in model
#    2008-05-28: Version one modified from earlier stand-alone scripts
# -----------------------------------------------------------------------------
#
# -- access the list of station locations
o3_srfc_stlist="/home/rdp001/cmam/diags/stations/o3_stations"
ln -s ${o3_srfc_stlist} stations1
#
# -- access model history files
.   ggfiles.cdk
#
echo "SELECT    STEPS $t1 $t2    1 LEVS    1    1 NAME O3LL" | ccc   select npakgg O3_sf
echo "SELECT    STEPS $t1 $t2    1 LEVS-9001 1000 NAME   O3" | ccc   select npakgg O3_ge
rm npakgg
#
#  ---- pull data at selected stations
#    ---- the first flag on the PULLSTN card specifies that
#           data is to be extracted from both O3_sf and O3_ge
#           depending on the vertical level of each station
#
echo "PULLSTN.  3 $delt" | ccc pull_sfc_stns_v2 O3_sf O3_ge stations1 txtout1
#
# -- there should not be a pre-existing output file for this month
access oldgp o3_srfc_st_${runid}_${year}_m${mon} na
delete oldgp   na
save txtout1 o3_srfc_st_${runid}_${year}_m${mon}
rm -f O3_sf O3_ge stations1 txtout1
