#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#  DESCRIPTION (original name: diag_chem_Nitrogen_962b.sh)
# 
#  Create monthly averages and monthly zonal averages for nitrogen
#  species: N, NO, NO2, NO3, N2O5, HNO3, HNO4, N2O and the advected
#  family NX (NO + NO2 + NO3 + 2*N2O5); and diagnosed families: NOx
#  (NO + NO2) and NOy (N + NO + NO2 + NO3 + 2*N2O5 + HNO3 + HNO4 +
#  ClONO2 + BrONO2).
#
#  HNO3 is diagnosed in its total amount and in its gas phase and
#  condensed phase parts, HNO3(g) and HNO3(s) respectively.
#
#  NOy is diagnosed in two versions: NOY (including both HNO3(g) and
#  HNO3(s)) and NOY2 (including only HNO3(g)). Note that NOy can
#  either be derived from the advected family NX or be entirely
#  recreated from its individual components, in which case the
#  advected family NX is also recreated as NX2. Note that ClONO2 and
#  BrONO2 are included in NOy, but are not saved separately (see
#  Chlorine and Bromine decks).
#
#  Zonal averages include daytime and nighttime masked values for NO
#  and NO2.
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, dnsave, flabel, lat, lon, memory1, model1,
#       npg, plid, plv, plv2, run, stime, t1, t2, t3, t4, ge6hcsv.
#    -- p01-p100 (pressure level set)
#    -- hybrid parameters for tropospheric NOx and HNO3
#
#  PREREQUISITES
#
#    -- GS and SS history files
#    -- DAYMASK deck (GPDAY data file, unaveraged)
#    -- NUMBERDENSITY deck (GEND data file, unaveraged)
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables and xref/xpow for hybrid
#                 nitrogen species retrieved from trinfo
#    2023-01-18: Converted to shell script, removed ztsave and the logic
#                 to support running chemistry on a subset of model levels
#                 controlled by the trchem switch (D. Plummer)
#    2019-11-05: Removed iehcsv option and harmonized everything under iesave
#    2015-06-23: With removal of advected water (T6), hybrid tracers are
#                 now tracers 30 and 31
#    2014-06-04: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-07-19: Hybrid NOx and HNO3 are now tracer 29 and 30 - modified
#                 xref and xpow
#    2013-06-10: Add iesave option (Y. Jiao)
#    2013-04-02: Added calculation of the N2O burden and chemical loss rates
#                along with 6-hourly output to ge6 for ge6hcsv=on
#    2011-11-29: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2009-06-22: Save instantaneous HNO3 -M. Neish
#    2009-02-28: Modifications for tropospheric hybrid NOx and HNO3
#                tracers (D. Plummer)
#    2007-07-01: Added saving of instantaneous (un-averaged) NOy to ie
#                file (A.Jonsson)
#    2008-06-30: Changed output names for diagnostic families: NY to
#                NOY; CY to CLY; BY to BRY; to avoid confusion with
#                new simulated advected families (NY, CY, BY). For
#                consistency CZ was changed to CLZ and BZ to BRZ
#                (A.Jonsson)
#    2008-06-28: Commented out derivation of NX2 (A.Jonsson)
#    2008-06-25: Modified to handle case with chemistry calculated on
#                all model levels (trchem=on); Added diagnostics for
#                atomic nitrogen (N) and added N to NOy estimate;
#                Switched back to use NX instead of NX2 for NOy
#                estimate; Changed super labels for families (NOy,
#                NOy-HNO3(s)) to show all components (A.Jonsson)
#    2008-05-04: Switched to use NX2 instead of NX for NOy
#                derivations, i.e. now using NO+NO2+NO3+2*N2O5
#                recreated from chemical components rather than using
#                advected form; Corrected super labels for NX and NOX
#                to be consistent for xp and gp data (A.Jonsson)
#    2008-04-24: Modified to handle case with chemistry calculated on
#                all model levels (trchem=on) (D. Plummer)
#    2008-03-25: Changed output name for gas phase NOy to NY2
#                (A.Jonsson)
#    2008-03-19: Reverted to diagnosing NOy (NY) as including all
#                HNO3 (i.e. HNO3(g)+HNO3(s)). Added new diagnosed output 
#                variable NY2, identical to NY, but excluding HNO3(s) 
#                (A.Jonsson)
#    2007-12-17: Added code to recreate NX from chemistry domain
#                components; Removed HNO3(s) from NOy diagnostic
#                estimate (A.Jonsson)
#    2007-11-23: Capitalized all super labels (affecting H20(g),
#                H2O(s), HNO3(g), HNO3(s) and labels containing Br,
#                Cl, x, y or z) (A.Jonsson)
#    2007-11-23: Changed super label unit from "[MOL/M3]" to "[CM-3]"
#                (A.Jonsson)
#    2007-11-23: Corrected NOY output name to NY (A.Jonsson)
#    2007-11-18: Bug fix: Added $dnsave if-statement for daymask
#                select input card (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-15: Renamed 'NOY' to 'NOy' in nitrogen decks for
#                consistency with other families (A.Jonsson)
#    2007-11-15: Added parmsub parameter $dnsave to control day/night
#                time zonal averaging (A.Jonsson)
#    2007-11-15: Renamed VMR and ND output of H2O(s) and HNO3(s) to
#                AH2O and AHNO (A.Jonsson)
#    2007-11-13: Revised $ztsave if-statement to only encompass xp
#                file data (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-13: Replaced hard coded level specification for upper
#                limit of "tropospheric" domain by $pmint (A.Jonsson)
#    2007-11-12: Bug fix: Switched upper limit from $pmin to $p01 in
#                input card for daymask selection; Bug fix: Fixed NOy
#                diagnostic; Extended HNO3(g) diagnostic to surface
#                (A.Jonsson)
#    2007-11-05: Added Nitrogen deck (A.Jonsson)
#    2007-11-??: Derived from the NOy deck (A.Jonsson)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ---- variables for selstep
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
#
#  ---- number of timesteps in one day used for selection of once daily output
    dyfrq=`echo $delt | awk '{printf "%04d",86400/$1}'`
#
#  ----  find the xref and xpow for the hybrid NOx (TA) and HNO3 (TB) tracers
    i2=1
    while [ $i2 -le $ntrac ] ; do
        i2d=`echo $i2 | awk '{printf "%02d",$1}'` # 2-digit tracer number
        eval x=\${it$i2d}
        if [[ $x == 'TA' ]] ; then
            eval xrta=\${xref$i2d}
            eval xpta=\${xpow$i2d:=1.} # default 1.
        elif [[ $x == 'TB' ]] ; then
            eval xrtb=\${xref$i2d}
            eval xptb=\${xpow$i2d:=1.}
        fi
        i2=`expr $i2 + 1`
    done
    if [ -z "$xrta" ] ; then
        echo "Error: **** tracer parameter xref for TA is undefined. ****"
        exit 1
    fi
    if [ -z "$xrtb" ] ; then
        echo "Error: **** tracer parameter xref for TB is undefined. ****"
        exit 1
    fi

    XRTA=`echo $xrta | awk '{printf "%10.2e",$1}'` # 10-character XREF
    XPTA=`echo $xpta | awk '{printf "%10.2f",$1}'` # 10-character XPOW
#
    XRTB=`echo $xrtb | awk '{printf "%10.2e",$1}'`
    XPTB=`echo $xptb | awk '{printf "%10.2f",$1}'`
#
#  ----  access model history files and pre-generated fields
#
.   spfiles.cdk
.   ggfiles.cdk
#
#  ---- access the background number density field calculated
#       from the previously called diag_chem_nd script
    access nd_ge  ${flabel}_gend 
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select out required fields
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   N1   N2   N3   N4
SELECT       BN" | ccc select npakgg NO_ge NO2_ge NO3_ge N2O5_ge BrONO2_ge
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME SHNO" |
       ccc select npakgg fr_sHNO3_ge
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   NX   N5   N7   T5
SELECT       C4" | ccc select npaksp NX_se HNO3_se HNO4_se N2O_se ClONO2_se

    echo "COFAGG    $lon$lat    0$npg" > .cofagg_input_card
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
    
    cofagg NX_se   NX_ge input=.cofagg_input_card
    cofagg HNO3_se HNO3_ge input=.cofagg_input_card
    cofagg HNO4_se HNO4_ge input=.cofagg_input_card
    cofagg N2O_se  N2O_ge input=.cofagg_input_card
    cofagg ClONO2_se  ClONO2_ge input=.cofagg_input_card
#
# --- select the hybrid NOx and HNO3 tracers, convert to physically
#       meaningful numbers and merge with the standard NOx and HNO3 tracers
#   ---- NOTE: gstracx will now convert from spectral to gridpoint if
#              the input field is a spectral field
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   TA   TB" |
      ccc select npaksp NXhy_se HNO3hy_se
#
    echo "GSTRACX...$coord $itrvar$XRTA$XPTA" > .gstracx_input_card
   
    cat .cofagg_input_card >> .gstracx_input_card
    cat .gsapl_input_card  >> .gstracx_input_card

    ccc gstracx NXhy_se dum1 dum2 NXt_ge input=.gstracx_input_card
    rm .gstracx_input_card dum1 dum2
    
    echo "GSTRACX...$coord $itrvar$XRTB$XPTB" > .gstracx_input_card
    cat .cofagg_input_card >> .gstracx_input_card
    cat .gsapl_input_card  >> .gstracx_input_card

    ccc gstracx HNO3hy_se dum1 dum2 HNO3t_ge input=.gstracx_input_card
    rm NXhy_se HNO3hy_se .gstracx_input_card dum1 dum2
#
# --- add the hybrid NOx and the mixing ratio NOx together to create
#      the full model NOx
    add NX_ge NXt_ge temp1
    echo "NEWNAM.      NX" | ccc newnam temp1 temp2
    mv temp2 NX_ge
#
    add HNO3_ge HNO3t_ge temp3
    echo "NEWNAM.      N5" | ccc newnam temp3 temp4
    mv temp4 HNO3_ge
    rm NXt_ge HNO3t_ge temp1 temp3
    rm *_se npaksp
#
#  ----  create new variables
#
# --- separate total nitric acid (HNO3(g)+HNO3(s)) into its components
    mlt  fr_sHNO3_ge HNO3_ge    sHNO3_ge      
    sub  HNO3_ge     sHNO3_ge   new1
    echo "NEWNAM.    HNO3" | ccc newnam new1 gHNO3_ge
    rm new1
#
# -- create NOx (NO+NO2)
    add NO_ge NO2_ge NOx_ge
#
# -- Create total nitrogen family (NOy)
    add  NX_ge  HNO3_ge   aw
    add  aw     HNO4_ge   bw
    add  bw     ClONO2_ge cw
    add  cw     BrONO2_ge dw
    echo "NEWNAM.     NOY" | ccc newnam dw NOy_ge
#
    rm aw bw cw dw ClONO2_ge BrONO2_ge
#
# -- define a new NOy (NOY2) that does not include HNO3(s)
    sub   NOy_ge   sHNO3_ge aw
    echo "NEWNAM.    NOY2" | ccc newnam aw NOy2_ge
#
    rm aw
#
#  ----  calculate number density fields
#
#     nd_tracer = vmr_tracer * nd_air
#     nd  - number density [molecules/m3]
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ND" |
      ccc select nd_ge nd_c_ge
    mlt sHNO3_ge nd_c_ge sHNO3_nd_ge
    rm nd_* npakgg
#
#  ----  interpolate on to constant pressure surfaces
#
    gsapl NO_ge       gslnsp NO_gp input=.gsapl_input_card
    gsapl NO2_ge      gslnsp NO2_gp input=.gsapl_input_card
    gsapl NOx_ge      gslnsp NOx_gp input=.gsapl_input_card
    gsapl NO3_ge      gslnsp NO3_gp input=.gsapl_input_card
    gsapl N2O5_ge     gslnsp N2O5_gp input=.gsapl_input_card
    gsapl NX_ge       gslnsp NX_gp input=.gsapl_input_card
    gsapl HNO3_ge     gslnsp HNO3_gp input=.gsapl_input_card
    gsapl HNO4_ge     gslnsp HNO4_gp input=.gsapl_input_card
    gsapl NOy_ge      gslnsp NOy_gp input=.gsapl_input_card
    gsapl NOy2_ge     gslnsp NOy2_gp input=.gsapl_input_card
    gsapl gHNO3_ge    gslnsp gHNO3_gp input=.gsapl_input_card
    gsapl sHNO3_ge    gslnsp sHNO3_gp input=.gsapl_input_card
    gsapl sHNO3_nd_ge gslnsp sHNO3_nd_gp input=.gsapl_input_card
    gsapl N2O_ge      gslnsp N2O_gp input=.gsapl_input_card
#
#  ----  create monthly and zonal averages
#
# -- monthly averages
    timavg NO_gp       NO_tav
    timavg NO2_gp      NO2_tav
    timavg NOx_gp      NOx_tav
    timavg NO3_gp      NO3_tav
    timavg N2O5_gp     N2O5_tav
    timavg NX_gp       NX_tav
    timavg HNO3_gp     HNO3_tav
    timavg HNO4_gp     HNO4_tav
    timavg NOy_gp      NOy_tav
    timavg NOy2_gp     NOy2_tav
    timavg gHNO3_gp    gHNO3_tav
    timavg sHNO3_gp    sHNO3_tav
    timavg sHNO3_nd_gp sHNO3_nd_tav
    timavg N2O_gp      N2O_tav
#
    if [ "$gesave" = "on" ] ; then
      timavg NO_ge       NO_etav
      timavg NO2_ge      NO2_etav
      timavg NOx_ge      NOx_etav
      timavg NO3_ge      NO3_etav
      timavg N2O5_ge     N2O5_etav
      timavg NX_ge       NX_etav
      timavg HNO3_ge     HNO3_etav
      timavg HNO4_ge     HNO4_etav
      timavg NOy_ge      NOy_etav
      timavg NOy2_ge     NOy2_etav
      timavg gHNO3_ge    gHNO3_etav
      timavg sHNO3_ge    sHNO3_etav
      timavg sHNO3_nd_ge sHNO3_nd_etav
      timavg N2O_ge      N2O_etav
    fi
#
# -- monthly zonal averages
    zonavg NO_tav       NO_ztav
    zonavg NO2_tav      NO2_ztav
    zonavg NOx_tav      NOx_ztav
    zonavg NO3_tav      NO3_ztav
    zonavg N2O5_tav     N2O5_ztav
    zonavg NX_tav       NX_ztav
    zonavg HNO3_tav     HNO3_ztav
    zonavg HNO4_tav     HNO4_ztav
    zonavg NOy_tav      NOy_ztav
    zonavg NOy2_tav     NOy2_ztav
    zonavg gHNO3_tav    gHNO3_ztav
    zonavg sHNO3_tav    sHNO3_ztav
    zonavg sHNO3_nd_tav sHNO3_nd_ztav
    zonavg N2O_tav      N2O_ztav
#
# -- if requested, calculate separate daytime and nighttime
#     zonal averages
    if [ "$dnsave" = on ] ; then
      access day_gp ${flabel}_gpday
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME DMSK" |
        ccc select day_gp day
      rzonavg3 NO_gp  day NO_z  NO_d  NO_n
      rzonavg3 NO2_gp day NO2_z NO2_d NO2_n
#
#       Monthly zonal/day/night averages 
#
      timavgmsk NO_d NO_dztav
      timavgmsk NO_n NO_nztav
#
      timavgmsk NO2_d NO2_dztav
      timavgmsk NO2_n NO2_nztav
#
      rm day* *_z *_d *_n
    fi
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#     ix - instanteneous zonal average cross-sections
#
    if [ "$iesave" = "on" ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep    NO_ge    NO_ie input=ic.tstep_model
        selstep   NO2_ge   NO2_ie input=ic.tstep_model
        selstep   NO3_ge   NO3_ie input=ic.tstep_model
        selstep  N2O5_ge  N2O5_ie input=ic.tstep_model
        selstep  HNO4_ge  HNO4_ie input=ic.tstep_model
        selstep  HNO3_ge  HNO3_ie input=ic.tstep_model
        selstep gHNO3_ge gHNO3_ie input=ic.tstep_model
        selstep sHNO3_ge sHNO3_ie input=ic.tstep_model
        selstep   NOy_ge   NOy_ie input=ic.tstep_model
        selstep  NOy2_ge  NOy2_ie input=ic.tstep_model
        selstep   N2O_ge   N2O_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        NO
NEWNAM.
XSAVE.        NO2
NEWNAM.
XSAVE.        NO3
NEWNAM.
XSAVE.        N2O5
NEWNAM.
XSAVE.        HNO4
NEWNAM.
XSAVE.        HNO3
NEWNAM.
XSAVE.        HNO3_G
NEWNAM.     N5G
XSAVE.        HNO3_S
NEWNAM.    AHNO
XSAVE.        NOY - NO NO2 NO3 2XN2O5 HNO3 HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        NOY_2 - NO NO2 NO3 2XN2O5 HNO3_G HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        N2O
NEWNAM." | ccc xsave oldie NO_ie NO2_ie NO3_ie N2O5_ie HNO4_ie HNO3_ie \
                      gHNO3_ie sHNO3_ie NOy_ie NOy2_ie N2O_ie newie
        save   newie  ${flabel}ie
        delete oldie                  na
        rm *_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        NO
NEWNAM.
XSAVE.        NO2
NEWNAM.
XSAVE.        NO3
NEWNAM.
XSAVE.        N2O5
NEWNAM.
XSAVE.        HNO4
NEWNAM.
XSAVE.        HNO3
NEWNAM.
XSAVE.        HNO3_G
NEWNAM.     N5G
XSAVE.        HNO3_S
NEWNAM.    AHNO
XSAVE.        NOY - NO NO2 NO3 2XN2O5 HNO3 HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        NOY_2 - NO NO2 NO3 2XN2O5 HNO3_G HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        N2O
NEWNAM." | ccc xsave oldie NO_ge NO2_ge NO3_ge N2O5_ge HNO4_ge HNO3_ge \
                      gHNO3_ge sHNO3_ge NOy_ge NOy2_ge N2O_ge newie
        save newie ${flabel}ie
        delete oldie              na
      fi
    fi
#
    if [ "$ipsave" = "on" ] ; then
      release oldip
      access oldip ${flabel}ip        na
      echo "XSAVE.        NO
NEWNAM.
XSAVE.        NO2
NEWNAM.
XSAVE.        NOX - NO NO2
NEWNAM.     NOX
XSAVE.        NO3
NEWNAM.
XSAVE.        N2O5
NEWNAM.
XSAVE.        NOX_2 - NO NO2 NO3 2XN2O5
NEWNAM.
XSAVE.        HNO3
NEWNAM.
XSAVE.        HNO3_G
NEWNAM.
XSAVE.        HNO3_S
NEWNAM.    AHNO
XSAVE.        HNO3_S NUMDEN - 1 OVER CM3
NEWNAM.    AHNO
XSAVE.        HNO4
NEWNAM.
XSAVE.        NOY - NO NO2 NO3 2XN2O5 HNO3 HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        NOY_2 - NO NO2 NO3 2XN2O5 HNO3_G HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        N2O
NEWNAM." | ccc xsave oldip NO_gp NO2_gp NOx_gp NO3_gp N2O5_gp NX_gp \
                  HNO3_gp gHNO3_gp sHNO3_gp sHNO3_nd_gp HNO4_gp NOy_gp \
                  NOy2_gp N2O_gp newip
      save newip ${flabel}ip
      delete oldip                    na
    fi
#
    if [ "$dailysv" = on ] ; then
      echo "SELECT.   STEPS 000000000 999999999 $dyfrq LEVS-9001 1000 NAME NOY2" |
         ccc select NOy2_gp NOy2_dly_gp
      zonavg NOy2_dly_gp NOy2_ixp
#
      release oldix
      access oldix ${flabel}ix        na
      echo "XSAVE.        INSTANTANEOUS NOY_2
NEWNAM." | ccc xsave oldix NOy2_ixp newix
      save   newix ${flabel}ix
      delete oldix                    na
    fi
#
    rm *_gp
    rm *_ge
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta levels
#
    echo "XSAVE.        NO
NEWNAM.
XSAVE.        NO2
NEWNAM.
XSAVE.        NOX - NO NO2
NEWNAM.     NOX
XSAVE.        NO3
NEWNAM.
XSAVE.        N2O5
NEWNAM.
XSAVE.        NOX_2 - NO NO2 NO3 2XN2O5
NEWNAM.
XSAVE.        HNO3
NEWNAM.
XSAVE.        HNO3_G
NEWNAM.
XSAVE.        HNO3_S
NEWNAM.    AHNO
XSAVE.        HNO3_S NUMDEN - 1 OVER CM3
NEWNAM.    AHNO
XSAVE.        HNO4
NEWNAM.
XSAVE.        NOY - NO NO2 NO3 2XN2O5 HNO3 HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        NOY_2 - NO NO2 NO3 2XN2O5 HNO3_G HNO4 CLONO2 BRONO2 
NEWNAM.
XSAVE.        N2O
NEWNAM." | ccc xsave oldxp NO_ztav NO2_ztav NOx_ztav NO3_ztav N2O5_ztav \
                 NX_ztav HNO3_ztav gHNO3_ztav sHNO3_ztav sHNO3_nd_ztav \
                 HNO4_ztav NOy_ztav NOy2_ztav N2O_ztav newxp
#
    if [ "$dnsave" = on ] ; then
      echo "XSAVE.        NO DAY
NEWNAM.
XSAVE.        NO2 DAY
NEWNAM.
XSAVE.        NO NIGHT
NEWNAM.
XSAVE.        NO2 NIGHT
NEWNAM." | ccc xsave newxp NO_dztav NO2_dztav NO_nztav NO2_nztav newxp1
      mv newxp1 newxp
    fi
#
    release old_xp
    access old_xp ${flabel}xp      na
    xjoin  old_xp newxp new_xp
    save   new_xp ${flabel}xp
    delete old_xp                  na
#
    release oldgp
    access oldgp ${flabel}gp        na
    echo "XSAVE.        NO
NEWNAM.
XSAVE.        NO2
NEWNAM.
XSAVE.        NOX - NO NO2
NEWNAM.     NOX
XSAVE.        NO3
NEWNAM.
XSAVE.        N2O5
NEWNAM.
XSAVE.        NOX_2 - NO NO2 NO3 2XN2O5
NEWNAM.
XSAVE.        HNO3
NEWNAM.
XSAVE.        HNO3_G
NEWNAM.
XSAVE.        HNO3_S
NEWNAM.    AHNO
XSAVE.        HNO3_S NUMDEN - 1 OVER CM3
NEWNAM.    AHNO
XSAVE.        HNO4
NEWNAM.
XSAVE.        NOY - NO NO2 NO3 2XN2O5 HNO3 HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        NOY_2 - NO NO2 NO3 2XN2O5 HNO3_G HNO4 CLONO2 BRONO2 
NEWNAM.
XSAVE.        N2O
NEWNAM." | ccc xsave oldgp NO_tav NO2_tav NOx_tav NO3_tav N2O5_tav NX_tav \
                 HNO3_tav gHNO3_tav sHNO3_tav sHNO3_nd_tav HNO4_tav NOy_tav \
                 NOy2_tav N2O_tav newgp
    save   newgp ${flabel}gp
    delete oldgp                    na
#
    if [ "$gesave" = "on" ] ; then
      access oldge ${flabel}ge        na
      echo "XSAVE.        NO
NEWNAM.
XSAVE.        NO2
NEWNAM.
XSAVE.        NOX - NO NO2
NEWNAM.     NOX
XSAVE.        NO3
NEWNAM.
XSAVE.        N2O5
NEWNAM.
XSAVE.        NOX_2 - NO NO2 NO3 2XN2O5
NEWNAM.
XSAVE.        HNO3
NEWNAM.
XSAVE.        HNO3_G
NEWNAM.
XSAVE.        HNO3_S
NEWNAM.    AHNO
XSAVE.        HNO3_S NUMDEN - 1 OVER CM3
NEWNAM.    AHNO
XSAVE.        HNO4
NEWNAM.
XSAVE.        NOY - NO NO2 NO3 2XN2O5 HNO3 HNO4 CLONO2 BRONO2
NEWNAM.
XSAVE.        NOY_2 - NO NO2 NO3 2XN2O5 HNO3_G HNO4 CLONO2 BRONO2 
NEWNAM.
XSAVE.        N2O
NEWNAM." | ccc xsave oldge NO_etav NO2_etav NOx_etav NO3_etav N2O5_etav \
                 NX_etav HNO3_etav gHNO3_etav sHNO3_etav sHNO3_nd_etav \
                 HNO4_etav NOy_etav NOy2_etav N2O_etav newge
      save newge ${flabel}ge
      delete oldge                na
    fi
