#deck pcldst2
jobname=pcldst2 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   revise calculation of del for fields on eta surfaces.
#                   pcldst2             e.chan. jan 27/93 - ec.
#   ----------------------------------- computation of long term or pooled
#                                       cloud statistics. based on deck
#                                       pcldsta except to be used with deck
#                                       cloud5 which no longer saves cld
#                                       on pressure surfaces.
#
.   gpattach.cdk
    if [ "$pooladd" = on ] ; then
      set_switch j1='pooladd' 
      access zgg1 ${model1}xp
    fi
    if [ "$pooladd" != on ] ; then
      set_switch j1='notadd' 
    fi
.   calcons.cdk
#   ----------------------------------- xfind rhc and generate masks with all
#                                       values set to one so that rzonavg in
#                                       comdecks calcsdz and standev behave
#                                       like zonavg.
.   gpxfind.cdk
    if [ "$pooladd" = on ] ; then
      xlin x2 del2 
    fi
    xlin x mdel 
    timavg mdel del 
#   ----------------------------------- compute stats for rhc.
.   calcsdz.cdk
    if [ "$xtracld" = on ] ; then
#     ----------------------------------- xfind emi and compute stats.
.     gpxfind.cdk
.     calcsdz.cdk
#     ----------------------------------- xfind tac and compute stats.
.     gpxfind.cdk
.     calcsdz.cdk
#     ----------------------------------- xfind ztac and compute stats.
.     gpxfind.cdk
.     calcsdz.cdk
    fi
#   ----------------------------------- now sigma-level fields.
#                                       first return all previous local files
#                                       and recalculate "constant" datasets.
    rm del del2 mdel x x1 x2 xx1 xx2
    rm y y1 y2
    rm m mm1 mm2 zm zmm1 zmm2
.   calcons.cdk
#   ----------------------------------- xfind scld and compute stats.
.   gpxfind.cdk
    timavg x  tx 
    zonavg tx  ztx 
    xlin tx  del 
.   calcsd.cdk
#   ----------------------------------- now single-level fields.
#                                       first return all previous local files
#                                       and recalculate "constant" datasets.
    rm del del2 mdel x x1 x2 xx1 xx2
    rm y y1 y2
    rm m mm1 mm2 zm zmm1 zmm2
.   calcons.cdk
#   ----------------------------------- xfind cldt and compute stats.
.   gpxfind.cdk
    timavg x  tx 
    zonavg tx  ztx 
    xlin tx  del 
.   calcsd.cdk
#   ----------------------------------- xfind tlwc and compute stats.
.   gpxfind.cdk
.   calcsd.cdk
    if [ "$blckcld" = on ] ; then
#     ----------------------------------- now block-level fields.
#                                         first return all previous local files
#                                         and recalculate "constant" datasets.
      rm del del2 mdel x x1 x2 xx1 xx2
      rm y y1 y2
      rm m mm1 mm2 zm zmm1 zmm2
.     calcons.cdk
#     ----------------------------------- xfind block cloud and compute stats.
.     gpxfind.cdk
      timavg x  tx 
      zonavg tx  ztx 
      xlin tx  del 
.     calcsd.cdk
#     ----------------------------------- xfind block cloud emissivity and
#                                         calculate stats.
.     gpxfind.cdk
.     calcsd.cdk
#     ----------------------------------- xfind block cloud optical depth and
#                                         compute stats.
.     gpxfind.cdk
.     calcsd.cdk
    fi

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         FLABEL   = $flabel
         YEARS    = $nyr
         DAYS     = $days
         RUN      = $run
         MODEL1   = $model1
0PCLDST2 --------------------------------------------------------------- PCLDST2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         RHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0     $nyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      2.E0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      RHC
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      1.E0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S(RHC)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK S(RHC)$plv        2.      100.   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF RHC  S(RHC)  DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     IRHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         RHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     IRHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(RHC)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      (RHC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S((RHC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK    RHC$plv        5.      100.   22   33$kax$kin
DAYS $days.  $nyr YEAR MEAN CLOUD RELATIVE HUMIDITY(RHC).
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 ZXLOOKS((RHC))$plv        2.      100.   22   33$kax$kin
 $nyr YEAR STANDARD DEVIATION OF ZONAL RHC  S((RHC))  DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      RHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (RHC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      RHC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((RHC))
fi
if [ "$xtracld" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      EMI
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S(EMI)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK S(EMI)$plv        5.      100.   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF EMI  S(EMI)  DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     IEMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     IEMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(EMI)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      (EMI)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S((EMI))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK    EMI$plv        5.      100.   22   33$kax$kin
DAYS $days.  $nyr YEAR MEAN CLOUD EMISSIVITY(EMI).
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 ZXLOOKS((EMI))$plv        5.      100.   22   33$kax$kin
 $nyr YEAR STANDARD DEVIATION OF ZONAL EMI  S((EMI))  DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (EMI)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((EMI))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      TAC
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S(TAC)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK S(TAC)$plv        5.       20.   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF TAC  S(TAC)  DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ITAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ITAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(TAC)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      (TAC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S((TAC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK    TAC$plv        5.       20.   22   33$kax$kin
DAYS $days.  $nyr YEAR MEAN CLOUD OPTICAL DEPTH(TAC).
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 ZXLOOKS((TAC))$plv        5.       20.   22   33$kax$kin
 $nyr YEAR STANDARD DEVIATION OF ZONAL TAC  S((TAC))  DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (TAC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((TAC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      ZTAC
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S(ZTAC)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(ZTAC)$plv        5.       20.   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZTAC  S(ZTAC)  DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ZTAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         ZTAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ZTAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(ZTAC)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      (ZTAC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      S((ZTAC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   ZTAC$plv        5.       20.   22   33$kax$kin
DAYS $days.  $nyr YEAR MEAN CLOUD OPTICAL DEPTH PER 100 MB (ZTAC).
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 ZXLOOK S(ZTAC)$plv        5.       20.   22   33$kax$kin
 $nyr YEAR STANDARD DEVIATION OF ZONAL ZTAC  S((ZTAC))  DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ZTAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (ZTAC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ZTAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((ZTAC))
fi
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0     $nyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      2.E0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S(SCLD)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(SCLD)    1      0.E0     1.E-1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF SIGMA LEVEL CLOUD S(SCLD) DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(SCLD)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S((SCLD))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   SCLD    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN SIGMA LEVEL CLOUD (SCLD)      DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(SCLD)    1      0.E0     1.E-1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL SIGMA-LEVEL. S((SCLD))  DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (SCLD)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((SCLD))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      SCLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0     $nyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      2.E0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S(CLDT)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(CLDT)    1      0.E0     1.E-1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF TOTAL CLOUD COVER S(CLDT) DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(CLDT)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S((CLDT))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   CLDT    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN TOTAL CLOUD COVER (CLDT)      DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(CLDT)    1      0.E0     1.E-1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL TOTAL CLOUD. S((CLDT))  DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (CLDT)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((CLDT))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      TLWC
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S(TLWC)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(TLWC)    1      0.E0       25.   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF TOTAL WATER PATH S(TLWC) DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     TLWC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TLWC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     TLWC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(TLWC)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S((TLWC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   TLWC   40        0.      100.   22   33$kax$kin
  $nyr YEAR MEAN TOTAL WATER PATH (TLWC)      DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(TLWC)    1      0.E0       25.   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL TOTAL WATER PATH. S((TLWC)) DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     TLWC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (TLWC)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     TLWC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((TLWC))
fi
if [ "$blckcld" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      BLOCK CLOUDINESS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0     $nyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      2.E0
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      BLOCK CLOUDINESS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN            0.E0      1.E0
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S(BLKC)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD AMOUNT IN BLOCK 1 S(BLKC) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD AMOUNT IN BLOCK 2 S(BLKC) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD AMOUNT IN BLOCK 3 S(BLKC) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD AMOUNT IN BLOCK 4 S(BLKC) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD AMOUNT IN BLOCK 5 S(BLKC) DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ICLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         BLOCK CLOUDINESS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      CLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(BLKC)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S((BLKC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   BLKC    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD AMOUNT IN BLOCK 1 (BLKC)      DAYS $days
  ZXLOOK   BLKC    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD AMOUNT IN BLOCK 2 (BLKC)      DAYS $days
  ZXLOOK   BLKC    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD AMOUNT IN BLOCK 3 (BLKC)      DAYS $days
  ZXLOOK   BLKC    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD AMOUNT IN BLOCK 4 (BLKC)      DAYS $days
  ZXLOOK   BLKC    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD AMOUNT IN BLOCK 5 (BLKC)      DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CLOUD IN BLOCK 1. S((BLKC)) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CLOUD IN BLOCK 2. S((BLKC)) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CLOUD IN BLOCK 3. S((BLKC)) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CLOUD IN BLOCK 4. S((BLKC)) DAYS $days
  ZXLOOKS(BLKC)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CLOUD IN BLOCK 5. S((CLDT)) DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      CLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (BLKC)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      CLD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((BLKC))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      BLOCK EMISSIVITY
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S(BLKE)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD EMISS. IN BLOCK 1 S(BLKE) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD EMISS. IN BLOCK 2 S(BLKE) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD EMISS. IN BLOCK 3 S(BLKE) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD EMISS. IN BLOCK 4 S(BLKE) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD EMISS. IN BLOCK 5 S(BLKE) DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     IEMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         BLOCK EMISSIVITY
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(BLKE)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S((BLKE))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   BLKE    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD EMISSIVITY IN BLOCK 1 (BLKE)      DAYS $days
  ZXLOOK   BLKE    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD EMISSIVITY IN BLOCK 2 (BLKE)      DAYS $days
  ZXLOOK   BLKE    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD EMISSIVITY IN BLOCK 3 (BLKE)      DAYS $days
  ZXLOOK   BLKE    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD EMISSIVITY IN BLOCK 4 (BLKE)      DAYS $days
  ZXLOOK   BLKE    1        0.        1.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD EMISSIVITY IN BLOCK 5 (BLKE)      DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*EM IN BLOCK 1. S((BLKE)) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*EM IN BLOCK 2. S((BLKE)) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*EM IN BLOCK 3. S((BLKE)) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*EM IN BLOCK 4. S((BLKE)) DAYS $days
  ZXLOOKS(BLKE)    1      0.E0      1.E0   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*EM IN BLOCK 5. S((BLKE)) DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (BLKE)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      EMI
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((BLKE))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
IN-XFIND      BLOCK OPTICAL DEPTH
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S(BLKT)
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD TAU IN BLOCK 1 S(BLKT) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD TAU IN BLOCK 2 S(BLKT) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD TAU IN BLOCK 3 S(BLKT) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD TAU IN BLOCK 4 S(BLKT) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF CLOUD TAU IN BLOCK 5 S(BLKT) DAYS $days
if [ "$ggsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM     ITAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         BLOCK OPTICAL DEPTH
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S(BLKT)
fi
if [ "$pooladd" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         S((BLKT))
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOK   BLKT    1        0.       10.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD OPTICAL DEPTH IN BLOCK 1 (BLKT)      DAYS $days
  ZXLOOK   BLKT    1        0.       10.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD OPTICAL DEPTH IN BLOCK 2 (BLKT)      DAYS $days
  ZXLOOK   BLKT    1        0.       10.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD OPTICAL DEPTH IN BLOCK 3 (BLKT)      DAYS $days
  ZXLOOK   BLKT    1        0.       10.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD OPTICAL DEPTH IN BLOCK 4 (BLKT)      DAYS $days
  ZXLOOK   BLKT    1        0.       10.   22   33$kax$kin
  $nyr YEAR MEAN CLOUD OPTICAL DEPTH IN BLOCK 5 (BLKT)      DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*OD IN BLOCK 1. S((BLKT)) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*OD IN BLOCK 2. S((BLKT)) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*OD IN BLOCK 3. S((BLKT)) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*OD IN BLOCK 4. S((BLKT)) DAYS $days
  ZXLOOKS(BLKT)    1      0.E0      1.E1   22   33$kax$kin
  $nyr YEAR STANDARD DEVIATION OF ZONAL CL*OD IN BLOCK 5. S((BLKT)) DAYS $days
if [ "$zxsave" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (BLKT)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM      TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         S((BLKT))
fi
fi

end_of_data

. endjcl.cdk


