#!/bin/bash
# Driver script to archive files associated with a run
#   
#   The main workhorse of this script in 'archive-files', which is a tool in CCCma_tools.
#       It also relies on make_file_name_lists, data_file_lists, and other functions within 
#       CanESM_shell_functions.sh

# Restart files
#   TODO: there is a notable amount of repeated code in here.. we can probably take a functional
#           approach to clean this up
if (( with_dump_rs == 1 )); then
    # NOTE: the restarts actually have the normal "model file" prefix, but they get archived
    #   with an restart specific prefix..
    if [[ $job_start_date == $run_start_date ]] && [[ ! -z "$canesm_dump_histRS0_suffix_list" ]] ; then
        #--------------------------
        # archive initial restarts 
        #--------------------------
        # produce list of files
        #   - first run legacy scrip make_file_name_list to produce key/value pair file 
        #       - here we set 'make_file_name_list' stop/stop be the same date as we only want files for this month
        export suffix_list=$canesm_dump_histRS0_suffix_list
        export prefix_list=${uxxx}
        export runid
        start_arg=$(printf "%04d:%02d" $restart_year $restart_month)
        stop_arg=$(printf "%04d:%02d" $restart_year $restart_month)
        filelist_name=tmp_arch_rs0_filelist
        make_file_name_list --start=$start_arg --stop=$stop_arg $filelist_name || bail "Failed to make $filelist_name!"

        #   - now extract file entries into shellvar
        files2archive=$(get_file_entries $filelist_name)

        # archive
        date_string=$(printf "%04d_m%02d" $restart_year $restart_month)
        time_stamp=$(date -u +%Y%j%H%M)
        archive_name=${canesm_dump_histRS0_prefix}_${runid}_${date_string}_${time_stamp}
        if [[ $canesm_dump_histRS_short_term == "on" ]]; then
            archive_project="crd_short_term"
        else
            archive_project="crd_cccma"
        fi
        archive-files -c -a $archive_name -p $archive_project $files2archive
    fi

    #------------------------------------------------------------------------
    # archive produced restarts (associated with the end of the model chunk)
    #------------------------------------------------------------------------
    # produce list of files
    #   - first run legacy scrip make_file_name_list to produce key/value pair file 
    export suffix_list=$canesm_dump_histRS_suffix_list
    export prefix_list=${uxxx}
    export runid
    filelist_name=tmp_arch_rs_filelist
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    make_file_name_list --start=$start_arg --stop=$stop_arg $filelist_name || bail "Failed to make $filelist_name!"

    #   - now extract file entries into shellvar
    files2archive=$(get_file_entries $filelist_name)

    # archive
    date_string=$(printf "%04d_m%02d" $job_stop_year $job_stop_month)
    time_stamp=$(date -u +%Y%j%H%M)
    archive_name=${canesm_dump_histRS_prefix}_${runid}_${date_string}_${time_stamp}
    if [[ $canesm_dump_histRS_short_term == "on" ]]; then
        archive_project="crd_short_term"
    else
        archive_project="crd_cccma"
    fi
    archive-files -c -a $archive_name -p $archive_project $files2archive
fi

#~~~~~~~~~~~~~~~~
# History files
#~~~~~~~~~~~~~~~~
if (( with_dump_hist == 1 )); then
    # archive history files for every month of the chunk

    # set constant input variables
    export suffix_list=$canesm_dump_hist_suffix_list
    export prefix_list=${canesm_dump_hist_prefix}
    export runid
    if [[ $canesm_dump_hist_short_term == "on" ]]; then
        archive_project="crd_short_term"
    else
        archive_project="crd_cccma"
    fi

    # interate through all the months
    tmp_year=$job_start_year
    tmp_month=$job_start_month
    while true ; do
        # check if we should run this iteration
        if (( tmp_year > job_stop_year )) || (( tmp_year == job_stop_year && tmp_month > job_stop_month )); then
            break
        fi

        # produce filelist
        #   - first run legacy scrip make_file_name_list to produce key/value pair file 
        #       - here we set 'make_file_name_list' stop/stop be the same date as we only want files for this month
        filelist_name=tmp_arch_hist_${tmp_year}_${tmp_month}
        start_arg=$(printf "%04d:%02d" $tmp_year $tmp_month)
        stop_arg=$(printf "%04d:%02d" $tmp_year $tmp_month)
        make_file_name_list --start=$start_arg --stop=$stop_arg $filelist_name || make_file_name_list_status="failed"
        if [[ "$make_file_name_list_status" == "failed" ]] && is_file $filelist_name ; then
            # make_file_name_list failed for an unknown reason, bail
            bail "Unexpected error from make_file_name_list!"
        fi

        # archive files if any were found
        if is_file $filelist_name; then
            # extract file entries into a shell var
            files2archive=$(get_file_entries $filelist_name)

            # archive
            date_string=$(printf "%04d_m%02d" $tmp_year $tmp_month)
            time_stamp=$(date -u +%Y%j%H%M)
            archive_name=${canesm_dump_hist_prefix}_${runid}_${date_string}_${time_stamp}
            archive-files -c -a $archive_name -p $archive_project $files2archive
        fi

        # increment month
        if (( tmp_month == 12 )); then
            tmp_month=1
            tmp_year=$((tmp_year+1))
        else
            tmp_month=$((tmp_month+1))
        fi
    done
fi

#~~~~~~~~~~~~~~~~~~
# Diagnostic files
#~~~~~~~~~~~~~~~~~~
if (( with_dump_diag == 1 )); then
    file_prefix=${diag_uxxx:-dc}

    # produce list of files
    #   - first run legacy scrip make_file_name_list to produce key/value pair file 
    export suffix_list=$canesm_dump_diag_suffix_list
    export prefix_list=$file_prefix
    export runid
    filelist_name=tmp_arch_diag_filelist
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    make_file_name_list --start=$start_arg --stop=$stop_arg $filelist_name || bail "Failed to make $filelist_name!"

    #   - now extract file entries into a shell var
    files2archive=$(get_file_entries $filelist_name)

    # archive
    date_string=$(printf "%04d%02d_%04d%02d" $job_start_year $job_start_month $job_stop_year $job_stop_month)
    time_stamp=$(date -u +%Y%j%H%M)
    archive_name=${file_prefix}_${runid}_${date_string}_${time_stamp}
    if [[ $canesm_dump_diag_short_term == "on" ]]; then
        archive_project="crd_short_term"
    else
        archive_project="crd_cccma"
    fi
    archive-files -c -a $archive_name -p $archive_project $files2archive
fi

#~~~~~~~~~~~~~~~~~~~~
# Time Series Files
#~~~~~~~~~~~~~~~~~~~~
if (( with_dump_tser == 1 )); then
    # produce intermediate filelists
    file_prefix=${tser_uxxx:-sc}
    
    #   - first run legacy script data_file_lists to produce lists of specific filetypes
    data_directory=${RUNPATH}
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    data_file_lists $runid $data_directory --start=${start_arg} --stop=${stop_arg}
    if is_zero_size ${file_prefix}_time_series_files; then
        bail "Failed to build timeseries file list!"
    fi

    #   - join rtd/time-series file lists together into a shell variable
    files2archive=$(cat ${file_prefix}_time_series_files)
    if ! is_zero_size ${file_prefix}_rtd_files; then
        files2archive="${files2archive} $(cat ${file_prefix}_rtd_files)"
    fi
    
    # archive
    date_string=$(printf "%04d%02d_%04d%02d" $job_start_year $job_start_month $job_stop_year $job_stop_month)
    time_stamp=$(date -u +%Y%j%H%M)
    archive_name=${file_prefix}_${runid}_${date_string}_${time_stamp}
    if [[ $canesm_dump_tser_short_term == "on" ]]; then
        archive_project="crd_short_term"
    else
        archive_project="crd_cccma"
    fi
    archive-files -c -a $archive_name -p $archive_project $files2archive
fi

#~~~~~~~~~~~~~~
# Netcdf Files
#~~~~~~~~~~~~~~
if (( with_dump_nc == 1 )); then
    file_prefix=${ncdf_uxxx:-nc}
    netcdf_dir="${RUNPATH}/nc_output"

    # get list of netcdf files (function from CanESM_shell_functions.sh)
    files2archive=$( get_list_of_cmorized_netcdf_files -f -c data_directory=${netcdf_dir} chunk_start_date=${job_start_date} chunk_stop_date=${job_stop_date} )

    # archive
    date_string=$(printf "%04d%02d_%04d%02d" $job_start_year $job_start_month $job_stop_year $job_stop_month)
    time_stamp=$(date -u +%Y%j%H%M)
    archive_name=${file_prefix}_${runid}_${date_string}_${time_stamp}
    if [[ $canesm_dump_nc_short_term == "on" ]]; then
        archive_project="crd_short_term"
    else
        archive_project="crd_cccma"
    fi

    # archive the files but maintain part of the file structure within the archive (achieved via '-s')
    archive-files -s ${netcdf_dir}/ -a $archive_name -p $archive_project $files2archive
fi

#~~~~~~~~~~~~~~
# Config Files
#~~~~~~~~~~~~~~
if (( with_dump_config == 1 )) && [[ "$job_stop_date" == "$run_stop_date" ]]; then
    # if we're tracking code version, update the config repo first
    if (( CODE_CHECKS == 1 )); then
        (( PRODUCTION == 1 )) &&  flgs="" || flgs="-d"
        strict_check $flgs $runid archivecfgfiles_${job_start_date}_${job_stop_date}
    fi

    file_prefix=cfg
    config_dir=${WRK_DIR}/config
    date_string=$(printf "%04d%02d_%04d%02d" $run_start_year $run_start_month $run_stop_year $run_stop_month)
    time_stamp=$(date -u +%Y%j%H%M)
    archive_name=${file_prefix}_${runid}_${date_string}_${time_stamp}
    if [[ $canesm_dump_cfg_short_term == "on" ]]; then
        archive_project="crd_short_term"
    else
        archive_project="crd_cccma"
    fi
    archive-files -n -a $archive_name -p $archive_project $config_dir
fi
