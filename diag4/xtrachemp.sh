#!/bin/sh
#                  xtrachemp           KVS - Dec 05/13
#   ---------------------------------- Generates extra information about
#                                      tracer sources/sinks
# ----------------------------------------------------------------------
#
#  Output variables:
#
#  1) Anthropogenic emission fluxes
#
#  EAIS : Total sulphur aircraft emissions
#  ESFS : Total sulphur surface emissions
#  ESTS : Total sulphur stack emissions
#  EFIS : Total sulphur vegetation fire emissions
#  EAIB : Total black carbon aircraft emissions
#  ESFB : Total black carbon surface emissions
#  ESTB : Total black carbon stack emissions
#  EFIB : Total black carbon vegetation fire emissions
#  EAIO : Total organic carbon aircraft emissions
#  ESFO : Total organic carbon surface emissions
#  ESTO : Total organic carbon stack emissions
#  EFIO : Total organic carbon vegetation fire emissions
#
#  2) Natural emission fluxes
#
#  EDSL : Sulphur emissions from DMS land sources
#  EDSO : Sulphur emissions from DMS marine sources
#  EDSV : Sulphur emissions from continuously emitting volcanoes
#  ESVE : Sulphur emissions from erruptive volcanoes
#  NOXD : Sulphur nighttime oxidation of DMS to sulphur dioxide
#  DOXD : Sulphur daytime oxidation of DMS to sulphur dioxide
#
#  3) Chemical and microphysical processes (***currently unavailable***)
#
#  DOX4 : Sulphur daytime oxidation by hydroxyl
#  WDD4 : Sulphur wet deposition due to deep convection
#  WDS4 : Sulphur wet deposition due to shallow convection
#   DD4 : Sulphur dry deposition
#
.   ggfiles.cdk
#
#  ---------------------------------- select sulphate-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      DOX4" | ccc select npakgg DOX4
#
#  ---------------------------------- select sulphur dioxide-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      WDD4 WDS4  DD4 DOXD
SELECT     ESFS EAIS ESTS EFIS ESVC ESVE NOXD" | ccc select npakgg WDD4 WDS4 DD4 DOXD \
                  ESFS EAIS ESTS EFIS \
                  ESVC ESVE NOXD
#
#  ---------------------------------- select black carbon-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      ESFB EAIB ESTB EFIB" | ccc select npakgg ESFB EAIB ESTB EFIB
#
#  ---------------------------------- select organic carbon-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      ESFO EAIO ESTO EFIO" | ccc select npakgg ESFO EAIO ESTO EFIO
#
#  ---------------------------------- select DMS-related fields
#
echo "SELECT          $r1 $r2 $r3         1    1      EDSL EDSO" | ccc select npakgg EDSL EDSO
#
#  ----------------------------------- compute and save statistics.
#
    statsav DOX4 \
            WDD4 WDS4 DD4 DOXD \
            ESFS EAIS ESTS EFIS \
            ESVC ESVE NOXD \
            ESFB EAIB ESTB EFIB \
            ESFO EAIO ESTO EFIO \
            EDSL EDSO \
            new_gp new_xp $stat2nd
#
#  ----------------------------------- save results.
#
    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp

    if [ "$rcm" != "on" ] ; then
     access oldxp ${flabel}xp
     xjoin oldxp new_xp newxp
     save newxp ${flabel}xp
     delete oldxp
    fi
