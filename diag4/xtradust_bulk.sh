#!/bin/sh
#                  xtradust_bulk       Y.PENG - Apr 30/12
#   ---------------------------------- DUST RELATED ARRAY
#                                      surface properties
#                                      dust wind speed
#                                      dust emission fluxes
#

.     ggfiles.cdk
#   ----------------------------------
#                                      select mineral dust emission
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1       ESD" | ccc select npakgg ESD
#                                      surface properties
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME GUST VGFR SMFR ZSPD" | ccc   select npakgg GUST VGFR SMFR ZSPD
#                                      dust wind speed
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME DUWD DUST DUTH USMK" | ccc   select npakgg DUWD DUST DUTH USMK
#                                      dust emission fluxes
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME FALL FA10  FA2  FA1" | ccc   select npakgg FALL FA10  FA2  FA1
#
      release npakgg

#  ---------------------------------- Compute and save statistics.
      statsav ESD \
              GUST VGFR SMFR ZSPD DUWD DUST DUTH USMK \
              FALL FA10 FA2  FA1  \
              new_gp new_xp $stat2nd

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi
