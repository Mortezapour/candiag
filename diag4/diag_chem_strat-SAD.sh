#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#  DESCRIPTION
# 
#   Diagnose the combined background sulphate / STS aerosol surface
#     area (CSD1) and PSC-II (water ice) (CSD2) surface area fields
#
#  PARMSUB PARAMETERS
#
#    -- days, flabel, memory1, model1, run, stime, t1, t2, t3, t4.
#
#  PREREQUISITES
#
#    -- output of CSD1 and CSD2 fields from the model
#
#  CHANGELOG
#
#    2023-10-27: Calculation of variables required by selstep and daily save 
#                 from available CanESM5 variables
#    2023-01-23: Converted to shell script and removed ztsave (D. Plummer)
#    2021-01-21: Convert units from cm^2/cm^3 to m^2/m^3 to align
#                 with CCMI data request units
#    2015-07-05: Deck created (D. Plummer)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ---- variables for selstep
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
#
#  ----  access model history files
#
.   spfiles.cdk
.   ggfiles.cdk
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select out required fields from model history
#
    echo "SELECT.         $t1 $t2 $t3 LEVS-9001 1000 NAME CSD1 CSD2" |
           ccc select npakgg work1 work2
#
# -- convert units to m^2/m^3
    echo "XLIN......     100.0       0.0" | ccc xlin work1 sts-sad_ge
    echo "XLIN......     100.0       0.0" | ccc xlin work2 ice-sad_ge
#
#  ----  create data on constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
#
    gsapl sts-sad_ge gslnsp sts-sad_gp input=.gsapl_input_card
    gsapl ice-sad_ge gslnsp ice-sad_gp input=.gsapl_input_card
#
    timavg sts-sad_gp sts-sad_tav
    timavg ice-sad_gp ice-sad_tav
#
    zonavg sts-sad_tav sts-sad_ztav
    zonavg ice-sad_tav ice-sad_ztav
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model eta levels
#     ip - instantaneous data on constant pressure surfaces
#
    if [ "$iesave" = on ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep  sts-sad_ge  sts-sad_ie input=ic.tstep_model
        selstep  ice-sad_ge  ice-sad_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        STS-SULPHATE SURFACE AREA DENSITY - M2 OVER M3
NEWNAM.
XSAVE.        ICE PSC SURFACE AREA DENSITY - M2 OVER M3
NEWNAM." | ccc xsave oldie sts-sad_ie ice-sad_ie newie
        save newie ${flabel}ie
        delete oldie                  na
        rm sts-sad_ie ice-sad_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        STS-SULPHATE SURFACE AREA DENSITY - M2 OVER M3
NEWNAM.
XSAVE.        ICE PSC SURFACE AREA DENSITY - M2 OVER M3
NEWNAM." | ccc xsave oldie sts-sad_ge ice-sad_ge newie
        save newie  ${flabel}ie
        delete oldie                  na
      fi
    fi
#
    if [ "$ipsave" = on ] ; then
      release oldip
      access oldip ${flabel}ip      na
      echo "XSAVE.        STS-SULPHATE SURFACE AREA DENSITY - M2 OVER M3
NEWNAM.
XSAVE.        ICE PSC SURFACE AREA DENSITY - M2 OVER M3
NEWNAM." | ccc xsave oldip sts-sad_gp ice-sad_gp newip
      save   newip  ${flabel}ip
      delete oldip                  na
    fi
    rm *_gp
    rm *_ge
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#
    release oldxp
    access oldxp ${flabel}xp      na
    echo "XSAVE.        STS-SULPHATE SURFACE AREA DENSITY - M2 OVER M3
NEWNAM.
XSAVE.        ICE PSC SURFACE AREA DENSITY - M2 OVER M3
NEWNAM." | ccc xsave oldxp sts-sad_ztav ice-sad_ztav newxp
    save newxp ${flabel}xp
    delete oldxp                  na
#
    access oldgp ${flabel}gp      na
    echo "XSAVE.        STS-SULPHATE SURFACE AREA DENSITY - M2 OVER M3
NEWNAM.
XSAVE.        ICE PSC SURFACE AREA DENSITY - M2 OVER M3
NEWNAM." | ccc xsave oldgp sts-sad_tav ice-sad_tav newgp
    save newgp ${flabel}gp
    delete oldgp                  na
