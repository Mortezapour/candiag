#!/bin/sh
# Improve code for better handling of model year 001,002,... format.
#                  spltdiag            sk,dy - Jul 02/2013 - sk
# Change format of time series from YYYY to YYYYMM
#
# ---- Split superlabeled files into time series
#
# Input diagnostic files are defined as
#
# ${diag_uxxx}_${runid}_${yr}_m${mon}_${sfx}
#
# where sfx is in the list diag_suffix_list, e.g., diag_suffix_list="gp xp cp"
#
# The format of time series is
#
# ${mts_uxxx}_${runid}_${time_start_year}${time_start_month}_${time_stop_year}${time_stop_month}_${diag_suffix_list}_${var}
#
# By default, time series for all variables in diagnostic files are constructed.
#
# If only a subset of variables is needed, the list of variables can be
# specified by defining a variable $spltdiag_vars as follows:
#
# spltdiag_vars="
#             ST
#             PCP
#             GT
#             SIC
#             SICN
#             PMSL
#             T
#             GZ"
#
# The variable names are equal to the superlabels in the diagnostic files and start at column 15.
#
# make sure that all necessary parameters are defined
    if [ -z "$runid" ] ; then
      echo " *** Error in spltdiag.dk: Model runid is undefined. ***"
      echo " *** Error in spltdiag.dk: Model runid is undefined. ***" >> haltit
      exit 1
    fi

    if [ -z "$diag_uxxx" ] ; then
      echo " *** Error in spltdiag.dk: Diagnostic file prefix diag_uxxx is undefined. ***"
      echo " *** Error in spltdiag.dk: Diagnostic file prefix diag_uxxx is undefined. ***" >> haltit
      exit 1
    fi

    if [ -z "$diag_suffix_list" ] ; then
      echo " *** Error in spltdiag.dk: Diagnostic file suffix list diag_suffix_list is undefined. ***"
      echo " *** Error in spltdiag.dk: Diagnostic file suffix list diag_suffix_list is undefined. ***" >> haltit
      exit 1
    fi

    if [ -z "$mts_uxxx" ] ; then
      echo " *** Error in spltdiag.dk: Monthly time series prefix mts_uxxx is undefined. ***"
      echo " *** Error in spltdiag.dk: Monthly time series prefix mts_uxxx is undefined. ***" >> haltit
      exit 1
    fi

    if [ -z "$time_start_year" ] ; then
      echo " *** Error in spltdiag.dk: time_start_year is undefined. ***"
      echo " *** Error in spltdiag.dk: time_start_year is undefined. ***" >> haltit
      exit 1
    fi
    if [ -z "$time_start_month" ] ; then
      echo " *** Error in spltdiag.dk: time_start_month is undefined. ***"
      echo " *** Error in spltdiag.dk: time_start_month is undefined. ***" >> haltit
      exit 1
    fi
    if [ -z "$time_stop_year" ] ; then
      echo " *** Error in spltdiag.dk: time_stop_year is undefined. ***"
      echo " *** Error in spltdiag.dk: time_stop_year is undefined. ***" >> haltit
      exit 1
    fi
    if [ -z "$time_stop_month" ] ; then
      echo " *** Error in spltdiag.dk: time_stop_month is undefined. ***"
      echo " *** Error in spltdiag.dk: time_stop_month is undefined. ***" >> haltit
      exit 1
    fi
#   set default year_offset if not defined
    if [ -z "$year_offset" ] ; then
      year_offset=0
    fi
    time_start_year_off=`expr $time_start_year + $year_offset | $AWK '{printf "%04d",$1}'` # at least 3 digits
    time_stop_year_off=`expr $time_stop_year + $year_offset | $AWK '{printf "%04d",$1}'` # at least 3 digits
#   set spltdiag_keep_last_month=on if not defined
    if [ -z "$spltdiag_keep_last_month" ] ; then
      spltdiag_keep_last_month=on
    fi
    
#   diagnostic file type loop
    for sfx in ${diag_suffix_list} ; do
      [[ -e input_file_list ]]          && rm input_file_list
      [[ -e input_file_list_delete ]]   && rm input_file_list_delete

#     year/month loop
      yr=$time_start_year
      while [ $yr -le $time_stop_year ] ; do
        yr=`echo $yr | awk '{printf "%04d", $1}'`
        yr_off=`echo $yr + $year_offset | awk '{printf "%04d", $1+$2}'`
        if [ $yr -eq $time_start_year ] ; then
          mon1=$time_start_month
        else
          mon1=1
        fi
        if [ $yr -eq $time_stop_year ] ; then
          mon2=$time_stop_month
        else
          mon2=12
        fi
        mon=$mon1
        while [ $mon -le $mon2 ] ; do
      	  mon=`echo $mon | awk '{printf "%02d", $1}'`
          release $yr$mon
          access  $yr$mon ${diag_uxxx}_${runid}_${yr}_m${mon}_${sfx}
          sfx1=`echo "$sfx" | cut -c1`
          sfx2=`echo "$sfx" | cut -c2`

#         assume that sfx starting with 'd' are for daily data
          if [ "$daily" = "on" -o "$sfx1" = "d" -o "$sfx2" = "h" -o "$sfx" = "site" ] ; then
#           time steps of daily files are unchanged
            echo "$yr$mon -1"      >> input_file_list
          else
#           time steps of monthly files are $yr$mon
            echo "$yr$mon $yr_off$mon" >> input_file_list
          fi
          echo "$yr$mon" >> input_file_list_delete
          mon=`expr $mon + 1`
        done # mon
        yr=`expr $yr + 1`
      done # yr

#     split input superlabeled files into time series
      varskip="    1" # skip all (co)variances X"Y" etc.
      tsnpack=${tsnpack:-"    2"} # repack (default at 2:1)
      if [ "$superlabels" = "off" -o "$superlabels" = "no" ] ; then
	lname="   -1" # no superlabels
      fi
      if [ "$sfx" = "gz" ] ; then
        lname=${lname:-"    1"} # use IBUF(2) in time series file names
        lslab=${lslab:-"    1"} # add superlabel
      else
        lname=${lname:-"    0"} # use superlabel in time series file names
        lslab=${lslab:-"    0"} # don't add superlabel
      fi

#     check if spltdiag_vars with variable list is defined
      if [ -z "$spltdiag_vars" ] ; then
        nvars="    0"
        spltdiag_vars=""
      else
        nvars="    1"
        spltdiag_vars=`echo "$spltdiag_vars" | sed -e '/^[ \t]*$/d'`
      fi
      if [ "$spltinfo" = "on" ] ; then
        linfo="    1"
      else
        linfo="    0"
      fi
      cat input_file_list
      echo "          $varskip$lname$lslab$tsnpack$nvars$linfo
$spltdiag_vars" | ccc spltslb input_file_list output_var_list
      cat  output_var_list
#     use yyyymm format
      time_start_month=`echo $time_start_month | awk '{printf "%02d", $1}'`
      time_stop_month=`echo $time_stop_month | awk '{printf "%02d", $1}'`
      rm -f output_file_list
      for var in `cat output_var_list` ; do
	echo "${mts_uxxx}_${runid}_${time_start_year_off}${time_start_month}_${time_stop_year_off}${time_stop_month}_${sfx}_${var}"  | tr 'A-Z' 'a-z' >> output_file_list
	save "$var" "${mts_uxxx}_${runid}_${time_start_year_off}${time_start_month}_${time_stop_year_off}${time_stop_month}_${sfx}_${var}"
	release "$var"
      done # var
#     add *_file_list to output_file_list
      echo "${mts_uxxx}_${runid}_${time_start_year_off}${time_start_month}_${time_stop_year_off}${time_stop_month}_${sfx}_file_list" | tr 'A-Z' 'a-z' >> output_file_list
      save output_file_list "${mts_uxxx}_${runid}_${time_start_year_off}${time_start_month}_${time_stop_year_off}${time_stop_month}_${sfx}_file_list"
      release input_file_list output_var_list

#     delete diagnostic files, if requested
      if [ "$delete_diag_files" = "on" ] ; then
        for f in `cat input_file_list_delete` ; do
	  if [ "$spltdiag_keep_last_month" = "on" -a "$f" = ${time_stop_year}${time_stop_month} ] ; then
#           keep last month but delete last month for the previous period
	    yr=`echo $time_start_year | awk '{printf "%04d", $1-1}'`
	    mon=`echo $time_stop_month | awk '{printf "%02d", $1}'`
	    release $yr$mon
	    access  $yr$mon ${diag_uxxx}_${runid}_${yr}_m${mon}_${sfx} na
	    delete  $yr$mon na dpalist=off
	  else
	    delete $f dpalist=off
	  fi
        done
      fi
    done # sfx
