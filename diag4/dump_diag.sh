#!/bin/bash
# Dumps AGCM diagnostic files to HPNLS using hpcarchive
# NCS, 04 2018

WRK_DIR=`pwd`

# Archive directory name
now=`date -u +%Y%j%H%M`
this_archive=${diag_uxxx}_${runid}_${previous_year}${previous_month}_${current_year}${current_month}_${now}

# check if archive already exists
[ -d $this_archive ] && { echo "Archive $this_archive appears to exist!" ; exit 1 ; }
mkdir $this_archive

# Change into the archive dir
cd $this_archive

# canesm_dump_diag_suffix_list defines a list of suffixes to be dumped.

# These are used by "make_file_name_list" to make the list of files.
[ -z "${canesm_dump_diag_suffix_list}" ] && { echo "canesm_dump_diag_suffix_list must not be empty when dump=on" ; exit 1 ; }
export suffix_list="${canesm_dump_diag_suffix_list}"
export prefix_list=$diag_uxxx
export runid

touch tmp_dump_file_list
make_file_name_list --start=$previous_year:$previous_month --stop=$current_year:$current_month tmp_dump_file_list
[ -s tmp_dump_file_list ] || { echo "ERROR in dumping: could not make tmp_dump_file_list" ; exit 1 ; }

# The list has been created. Now source it and parse the files
. tmp_dump_file_list

# Get hard copies of all these files into a directory which we can dump
for i in `seq $join`; do
  nam=file$i ;
  access ${!nam} ${!nam} nocp=off
done
rm -f .*_Link
rm -f tmp_dump_file_list

cd $WRK_DIR

# dump using hpcarchive
if [ "$shortermdir" = "on" ] ; then
  hpcarchive_project=crd_short_term
else
  hpcarchive_project=crd_cccma
fi

if [ "$hpcarchive_checksum" = "on" ] ; then
  hcparchive_checksum_arg="-k"
else
  hcparchive_checksum_arg=""
fi

# try archiving several times
ntrymax=10
ntry=1
while [ $ntry -le $ntrymax ] ; do
  status=""
  hpcarchive ${hcparchive_checksum_arg} -v -g -p ${hpcarchive_project} -a ${this_archive} -c ${this_archive} || status=$?
  echo status=$status
  if [ "$status" = "" ] ; then
    break
  fi
  # unsuccessful archival - sleep a bit and try again.
  sleep 60
  ntry=`expr $ntry + 1`
done
if [ "$status" != "" ] ; then
  exit 1
fi
