module stats
  ! NCS 2020 this is a very simple set of stats operations,
  ! just to compute the time-average needed for the MWE.
  ! The code is roughly based on what is done in statsav.f,
  ! which is more comprehensive (also does file i/o etc).

  use ccc_data, only: ccc_data_array
  use diag_sizes

  private
  public time_average
  contains
  
  subroutine time_average(cccdata_input, cccdata_output)
    ! Compute a basic time mean. No masking. Equal weights.
    ! Assume time is in 2nd dimension of array, space in first.
    implicit none
    
    character (len=100)   :: output_file   ! CCC file to write result to (will be replace)
    integer               :: il, it       ! loop counter levels, time
    integer               :: ivar         ! loop counter over variables
    integer               :: nl, nt       ! number of levels, timesteps
!    integer, dimension(8) :: IBUF         ! Legacy
    real, allocatable, dimension(:)    :: data_mean ! array to accumulate mean
    real                               :: mean_time
    type(ccc_data_array), dimension(:), intent(in)    :: cccdata_input
    type(ccc_data_array)                              :: cccdata_instance
    type(ccc_data_array)                              :: cccdata_result
    type(ccc_data_array), dimension(:), intent(inout) :: cccdata_output
    
!---------------------------------------------------------------------
    
    write(6,*) 'Doing time_average'
    
    do ivar = 1, size(cccdata_input)
       cccdata_instance = cccdata_input(ivar)      ! The variable we deal with
       allocate(data_mean(cccdata_instance%np))    ! Allocate by size of grid
       nt = cccdata_instance%nt                    ! number of timesteps
       nl = cccdata_instance%nlev                  ! number of levels
       
       call cccdata_result%clear

       ! loop over over time steps and accumulate into simple mean
       do il = 1,nl
          data_mean(:) = 0.e0
          mean_time = 0.e0
          
          do it = 1, nt
             data_mean(:) = data_mean(:) + cccdata_instance%data(:,il,it) / real(nt)
             mean_time = mean_time + real(cccdata_instance%time(it)) / real(nt)
          end do
              ! create a result cccdata object
              cccdata_instance%IBUF(4) = cccdata_instance%level(il)
              cccdata_instance%IBUF(2) = int(mean_time)
              call cccdata_result%add_data(cccdata_instance%name, data_mean, &
                                           cccdata_instance%IBUF)
       end do

       cccdata_output(ivar) = cccdata_result
       deallocate(data_mean)
    end do   
  end subroutine time_average    
end module stats
