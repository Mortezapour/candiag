      subroutine nc_get_char_val(fid,vid,start,count,char_val,
     &                           ndims,xtype)
c***********************************************************************
c Read an array section from a netcdf variable of any type and return the
c data in a real array
c***********************************************************************
      implicit none

      integer fid, vid, start(ndims), count(ndims)
      integer :: ndims, xtype

      !--- Character variable to hold data read from the netcdf file
      character(*) :: char_val

      include "netcdf.inc"

      !--- local
      integer :: ret

      !--- allocate space and read in data from the netcdf file
      if (xtype.eq.NF_CHAR) then
        char_val = " "
        ret = nf_get_vara_text(fid,vid,start,count,char_val)
        call nc_handle_err ("nf_get_vara_text", ret, fid)
      else
        write(6,*)'nc_get_char_val: xtype is out of range.',
     &           ' xtype = ',xtype
        call xit("GET_CHAR_VAL",-1)
      endif

      return
      end
