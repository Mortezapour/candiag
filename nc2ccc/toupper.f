      subroutine toupper(chin,chout)
        implicit none
        character*(*) chin,chout
        integer i
        if (len(chout).lt.len_trim(chin)) then
          write(6,*)'toupper: Output string too short'
          call xit("TOUPPER",-1)
        endif
        chout=' '
        chout=trim(chin)
        do i=1,len_trim(chin)
          select case (chin(i:i))
            case ("a")
              chout(i:i)="A"
            case ("b")
              chout(i:i)="B"
            case ("c")
              chout(i:i)="C"
            case ("d")
              chout(i:i)="D"
            case ("e")
              chout(i:i)="E"
            case ("f")
              chout(i:i)="F"
            case ("g")
              chout(i:i)="G"
            case ("h")
              chout(i:i)="H"
            case ("i")
              chout(i:i)="I"
            case ("j")
              chout(i:i)="J"
            case ("k")
              chout(i:i)="K"
            case ("l")
              chout(i:i)="L"
            case ("m")
              chout(i:i)="M"
            case ("n")
              chout(i:i)="N"
            case ("o")
              chout(i:i)="O"
            case ("p")
              chout(i:i)="P"
            case ("q")
              chout(i:i)="Q"
            case ("r")
              chout(i:i)="R"
            case ("s")
              chout(i:i)="S"
            case ("t")
              chout(i:i)="T"
            case ("u")
              chout(i:i)="U"
            case ("v")
              chout(i:i)="V"
            case ("w")
              chout(i:i)="W"
            case ("x")
              chout(i:i)="X"
            case ("y")
              chout(i:i)="Y"
            case ("z")
              chout(i:i)="Z"
          end select
        enddo
      end

